//
//  AppDelegate.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import AVKit
import GoogleMaps
import GooglePlaces
import Braintree
import Firebase
import FirebaseMessaging
import UserNotifications
import FirebaseInstanceID

@main
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Custom navigation
        BTAppSwitch.setReturnURLScheme("com.appcare.IndiaToWorld.payments")

        FirebaseApp.configure()
        Messaging.messaging()
//
        registerAppForNotifications()
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        UserDefaults.standard.set(deviceID, forKey: "deviceID")

        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey(provideAPIKey.clientProvideAPIKey)
        GMSPlacesClient.provideAPIKey(provideAPIKey.clientProvideAPIKey)
        appearence()
        Thread.sleep(forTimeInterval: 2.0)
        updateRootVC()

//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//        if UserDefaults.standard.bool(forKey: LOGGED_IN){
//            let tabBarVC = storyBoard.instantiateViewController(withIdentifier: Identifiers.TabBarController)
//
//            // then set your root view controller
//            self.window?.rootViewController = tabBarVC
//
//        }
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.scheme?.localizedCaseInsensitiveCompare("com.appcare.IndiaToWorld.payments") == .orderedSame {
        print(url)
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return true
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    //MARK:- Custom functions
    func appearence() {
        //Making navigation bar to transaparent withpout color
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = .clear
        UINavigationBar.appearance().isTranslucent = true
        
        //setting back button to custom image
        let yourBackImage = UIImage(named: "back arrow")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = yourBackImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = yourBackImage
    }
    
    func updateRootVC() {
            let status = UserDefaults.standard.bool(forKey: LOGGED_IN)
            print(status)
            if status == true {
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: Identifiers.TabBarController) as! TabBarController
                if let navigationController = self.window?.rootViewController as? UINavigationController
                {
                    navigationController.pushViewController(initialViewControlleripad, animated: true)
                }
                else
                {
                    print("Navigation Controller not Found")
                }
            }
            else
            {
                print(status)
            }
        }
    
    // MARK: -  FIRFireBase Messaging Delegate
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        // If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        #if DEBUG
        
        print("FCM TOKEN ====> \(String(describing: fcmToken))")
        #endif
        
//        UserDefaults.standard.setValue(fcmToken, forKey: "FCM_TOKEN")
        // UserDefaults.standard.setValue(fcmToken, forKey: FCM_TOKEN)
        UserDefaults.standard.set(fcmToken, forKey: "FCM_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    
    // MARK: - NOTIFICATION DELEGATES(For APNS)
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let strDeviceToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        Messaging.messaging().apnsToken = deviceToken
        
        #if DEBUG
        print("DEVICE TOKEN ====> \(strDeviceToken)\n")
        #endif
        
        // UserDefaults.standard.setValue(strDeviceToken, forKey: "device_token")
        // UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        #if DEBUG
        print("\(error)")
        print("Failed to register for notifications: \(error.localizedDescription)")

        #endif
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "IndiaToWorld")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    //MARK: -  Register / UnRegister User Notifications
    
    func registerAppForNotifications() {
        
        Messaging.messaging().delegate = self
        print("Register For Push Notifications")

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func UnRegisterAppForNotifications() {
        print("UnRegister For Push Notifications")

        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    //MARK: -  UNUserNotificationCenter Delegate
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        let str = "\(userInfo)"
        /*
         let alertController = UIAlertController(title: "News Tested", message: str, preferredStyle: .actionSheet)
         let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
         UIAlertAction in
         NSLog("OK Pressed")
         
         }
         let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
         UIAlertAction in
         NSLog("Cancel Pressed")
         }
         alertController.addAction(okAction)
         alertController.addAction(cancelAction)
         self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
         */
        print("\(str)")
    }
    
    // called when a notification is delivered when app is in foreground.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        _ = center
        _ = notification
        completionHandler([.badge, .alert, .sound])
        
        print("Notifications Will Present=====>:\(notification)")
        
        
        let userInfo = notification.request.content.userInfo
        //  let identity = notification.request.identifier
        print("User Info : \(userInfo)")
        //completionHandler([.sound, .alert, .badge])
        
        let text = userInfo["text"] as? String ?? ""
        print(text)
        

        completionHandler([.badge, .alert, .sound])

    }
    
    // called to let your app know which action was selected by the user for a given notification.
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        _ = center
        _ = response
        completionHandler()
        
        let userInfo = response.notification.request.content.userInfo
        print("User Info : \(userInfo)")
        let text = userInfo["text"] as? String ?? ""
        let title = userInfo["title"] as? String ?? ""
        let type = userInfo["type"] as? String ?? ""
        
        print("Did Receive Notifications Response===> \(response)")
        print("Text:\(text)")
        print("Title:\(title)")
        print("Type:\(type)")
        
        
        if userInfo["type"] as? String == "logout" {
                        
        }
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification notification: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        #if DEBUG
        print("Received push notification: \(notification), identifier: \(identifier ?? "")")
        // iOS 8
        #endif
        completionHandler()
    }
    
}
