//
//  AppHelper.swift
//  IndiaToWorld
//
//  Created by Bhasker on 18/01/21.
//

import Foundation

class AppHelper {
    
    static let appManager = AppHelper()
    
    class func getUserToken() -> String {
        
        if let data = UserDefaults.standard.value(forKey: LOGIN_RESPONSE) {

            let loginData = data as? NSDictionary
            
            if let userToken = loginData?[TOKEN_KEY] as? String {
                
                return userToken
                
            } else {
                
                return INVALID_USER_TOKEN
            }
            
        } else {
            
            return INVALID_USER_TOKEN
        }
    }
    
    class func getUserId() -> String {
        
        if let data = UserDefaults.standard.value(forKey: USER_ID) {
            
            let loginData = data as? String

            if let userId = loginData {
                
                return userId
                
            } else {
                
                return INVALID_USER_ID
            }
            
        } else {
            
            return INVALID_USER_ID
        }
    }
    
    class func getLoginType() -> String {
        
        if let data = UserDefaults.standard.value(forKey: LOGIN_RESPONSE) {
            
            let loginData = data as? NSDictionary
            
            if let loginType = loginData?[LOGIN_TYPE] as? String {
                
                return loginType
                
            } else {
                
                return INVALID_LOGIN_TYPE
            }
            
        } else {
            
            return INVALID_LOGIN_TYPE
        }
    }
    
    class func getGSTNo() -> String {
        
        if let data = UserDefaults.standard.value(forKey: GST_NO) {
            
            let loginData = data as? String

            if let gstNo = loginData {
                
                return gstNo
                
            } else {
                
                return INVALID_GST_NO
            }
            
        } else {
            
            return INVALID_GST_NO
        }
    }
    
    class func getFullName() -> String {
        
        if let data = UserDefaults.standard.value(forKey: FULL_NAME) {
            
            let loginData = data as? String

            if let fullName = loginData {
                
                return fullName
                
            } else {
                
                return INVALID_FUL_NAME
            }
            
        } else {
            
            return INVALID_FUL_NAME
        }
    }
    class func getMobileNo() -> String {
        
        if let data = UserDefaults.standard.value(forKey: MOBILE_NO) {
            
            let loginData = data as? String

            if let gstNo = loginData {
                
                return gstNo
                
            } else {
                
                return INVALID_MOBILE_NO
            }
            
        } else {
            
            return INVALID_MOBILE_NO
        }
    }

    class func getEmailId() -> String {
        
        if let data = UserDefaults.standard.value(forKey: EMAIL_ID) {
            
            let loginData = data as? String

            if let email = loginData {
                
                return email
                
            } else {
                
                return INVALID_EMAIL_ID
            }
            
        } else {
            
            return INVALID_EMAIL_ID
        }
    }

    class func getBundleId() -> String {
        
        let bundleId = Bundle.main.bundleIdentifier!
        
        return bundleId
    }
    
    // Validate Functions
    
    class func isValidEmail(with email: String?) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: email)
    }
    
    class func isValidFirstName() -> Bool {
        
        let firstNameRegEx = "^[a-zA-ZğüşıöçĞÜŞİÖÇ]+$"
        let firstNameTest = NSPredicate(format: "SELF MATCHES %@", firstNameRegEx)
        return firstNameTest.evaluate(with: self)
    }
    
    class func isValidLastName() -> Bool {
        
        let lastNameRegEx = "^[a-zA-ZğüşıöçĞÜŞİÖÇ]+$"
        let lastNameTest = NSPredicate(format: "SELF MATCHES %@", lastNameRegEx)
        return lastNameTest.evaluate(with: self)
    }
    
    class func isValidUsername() -> Bool {
        
        let usernameRegEx = "^[A-Z0-9a-zğüşıöçĞÜŞİÖÇ._%+-]+$"
        let usernameTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        return usernameTest.evaluate(with: self)
    }
    
    class func isValidPhoneNumber(value: String) -> Bool {
        
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    class func getCountryCallingCode(countryRegionCode:String)->String {
            
            let prefixCodes = ["AF": "+93", "AE": "+971", "AL": "+355", "AN": "+599", "AS":"+1", "AD": "+376", "AO": "+244", "AI": "+1", "AG":"+1", "AR": "+54","AM": "+374", "AW": "+297", "AU":"+61", "AT": "+43","AZ": "+994", "BS": "+1", "BH":"+973", "BF": "+226","BI": "+257", "BD": "+880", "BB": "+1", "BY": "+375", "BE":"+32","BZ": "+501", "BJ": "+229", "BM": "+1", "BT":"+975", "BA": "+387", "BW": "+267", "BR": "+55", "BG": "+359", "BO": "+591", "BL": "+590", "BN": "+673", "CC": "+61", "CD":"+243","CI": "+225", "KH":"+855", "CM": "+237", "CA": "+1", "CV": "+238", "KY":"+345", "CF":"+236", "CH": "+41", "CL": "+56", "CN":"+86","CX": "+61", "CO": "+57", "KM": "+269", "CG":"+242", "CK": "+682", "CR": "+506", "CU":"+53", "CY":"+537","CZ": "+420", "DE": "+49", "DK": "+45", "DJ":"+253", "DM": "+1", "DO": "+1", "DZ": "+213", "EC": "+593", "EG":"+20", "ER": "+291", "EE":"+372","ES": "+34", "ET": "+251", "FM": "+691", "FK": "+500", "FO": "+298", "FJ": "+679", "FI":"+358", "FR": "+33", "GB":"+44", "GF": "+594", "GA":"+241", "GS": "+500", "GM":"+220", "GE":"+995","GH":"+233", "GI": "+350", "GQ": "+240", "GR": "+30", "GG": "+44", "GL": "+299", "GD":"+1", "GP": "+590", "GU": "+1", "GT": "+502", "GN":"+224","GW": "+245", "GY": "+595", "HT": "+509", "HR": "+385", "HN":"+504", "HU": "+36", "HK": "+852", "IR": "+98", "IM": "+44", "IL": "+972", "IO":"+246", "IS": "+354", "IN": "+91", "ID":"+62", "IQ":"+964", "IE": "+353","IT":"+39", "JM":"+1", "JP": "+81", "JO": "+962", "JE":"+44", "KP": "+850", "KR": "+82","KZ":"+77", "KE": "+254", "KI": "+686", "KW": "+965", "KG":"+996","KN":"+1", "LC": "+1", "LV": "+371", "LB": "+961", "LK":"+94", "LS": "+266", "LR":"+231", "LI": "+423", "LT": "+370", "LU": "+352", "LA": "+856", "LY":"+218", "MO": "+853", "MK": "+389", "MG":"+261", "MW": "+265", "MY": "+60","MV": "+960", "ML":"+223", "MT": "+356", "MH": "+692", "MQ": "+596", "MR":"+222", "MU": "+230", "MX": "+52","MC": "+377", "MN": "+976", "ME": "+382", "MP": "+1", "MS": "+1", "MA":"+212", "MM": "+95", "MF": "+590", "MD":"+373", "MZ": "+258", "NA":"+264", "NR":"+674", "NP":"+977", "NL": "+31","NC": "+687", "NZ":"+64", "NI": "+505", "NE": "+227", "NG": "+234", "NU":"+683", "NF": "+672", "NO": "+47","OM": "+968", "PK": "+92", "PM": "+508", "PW": "+680", "PF": "+689", "PA": "+507", "PG":"+675", "PY": "+595", "PE": "+51", "PH": "+63", "PL":"+48", "PN": "+872","PT": "+351", "PR": "+1","PS": "+970", "QA": "+974", "RO":"+40", "RE":"+262", "RS": "+381", "RU": "+7", "RW": "+250", "SM": "+378", "SA":"+966", "SN": "+221", "SC": "+248", "SL":"+232","SG": "+65", "SK": "+421", "SI": "+386", "SB":"+677", "SH": "+290", "SD": "+249", "SR": "+597","SZ": "+268", "SE":"+46", "SV": "+503", "ST": "+239","SO": "+252", "SJ": "+47", "SY":"+963", "TW": "+886", "TZ": "+255", "TL": "+670", "TD": "+235", "TJ": "+992", "TH": "+66", "TG":"+228", "TK": "+690", "TO": "+676", "TT": "+1", "TN":"+216","TR": "+90", "TM": "+993", "TC": "+1", "TV":"+688", "UG": "+256", "UA": "+380", "US": "+1", "UY": "+598","UZ": "+998", "VA":"+379", "VE":"+58", "VN": "+84", "VG": "+1", "VI": "+1","VC":"+1", "VU":"+678", "WS": "+685", "WF": "+681", "YE": "+967", "YT": "+262","ZA": "+27" , "ZM": "+260", "ZW":"+263"]
            let countryDialingCode = prefixCodes[countryRegionCode]
            
            return countryDialingCode!
        }

}

struct AppUrls{
    
    //MARK:-  Url's
    //Test Api
    static let BaseUrl = "http://159.65.145.170/indiatoworld/index.php/"
    
    static let RegisterUrl = BaseUrl+"Genaral_Services/register"
    static let LoginUrl = BaseUrl+"Genaral_Services/login"
    static let ForgotPasswordUrl = BaseUrl+"Genaral_Services/Forget_password"
    static let OtpVerifyUrl = BaseUrl+"Genaral_Services/check_otp"
    static let PasswordUpdateUrl = BaseUrl+"Genaral_Services/password_update"
    static let ChangePasswordUrl = BaseUrl+"Genaral_Services/change_password"
    static let AddaddressUrl = BaseUrl+"Genaral_Services/addressAdd"
    static let GetaddressList = BaseUrl+"Genaral_Services/getuserAddress"
    static let Deleteaddress = BaseUrl+"Genaral_Services/userAddressDelete"
    static let getUserProfile = BaseUrl+"Genaral_Services/userProfile"
    static let profileupdate = BaseUrl+"Genaral_Services/profile_update"
    static let profile_photo_update = BaseUrl+"Genaral_Services/profile_photo_update"
    static let TermsAndConditionsUrl = BaseUrl+"Genaral_Services/terms_and_conditions"
    static let AboutUsUrl = BaseUrl+"Genaral_Services/about_us"
    static let FaqsUrl = BaseUrl+"Genaral_Services/faqs"
    static let PrivicyPolicyUrl = BaseUrl+"Genaral_Services/privicypolicy"
    static let RefundPolicyUrl = BaseUrl+"Genaral_Services/refundpolicy"
    static let WareDetailsUrl = BaseUrl+"Genaral_Services/warehouseDetails"
    static let CurrencyCodesUrl = BaseUrl+"Genaral_Services/CurrencyCodes"
    static let CurrencyConverterUrl = BaseUrl+"Genaral_Services/CurrencyConverter"
    static let CostCalculatorUrl = BaseUrl+"Genaral_Services/costcalculator"

    static let CountryListUrl = BaseUrl+"Genaral_Services/get_country_names"
    static let StateListUrl = BaseUrl+"Genaral_Services/get_state_names"
    static let CityListUrl = BaseUrl+"Genaral_Services/get_city_names"

    
    
    
    static let MembershipsPlansUrl = BaseUrl+"Genaral_Services/membershipsPlans"
    static let MembershipsPlansDetailsUrl = BaseUrl+"Genaral_Services/get_plans_benifit"
    static let SaveUserPlansUrl = BaseUrl+"Genaral_Services/save_user_plans"
    
    static let UserPaymentStatusUrl = BaseUrl+"Orders/PaymentStatus"

    static let ShippingAndPackageDetailsUrl =
        BaseUrl+"Genaral_Services/shipping_and_package_details"
    static let ReturnFormUrl = BaseUrl+"Genaral_Services/fetch_return_data"
    static let BuyForMeUrl = BaseUrl+"Orders/buyforme"
    static let BookForMeUrl = BaseUrl+"Orders/bookforme"
    static let productDetailsUrl = BaseUrl+"Genaral_Services/productDetails"

    static let AllPackagesListUrl = BaseUrl+"Orders/getAllorders"
    static let AllOrderHistoryListUrl = BaseUrl+"Orders/getOrdersHistory"
    static let ConsolidateCountriesListUrl = BaseUrl+"Genaral_Services/fetchDeliveredCountry"
    static let ConsolidateProductsListUrl = BaseUrl+"Genaral_Services/fetchOrdersByCountry"
    static let CreateConsolidatePackagesUrl = BaseUrl+"Genaral_Services/createConsolidatePackages"
    static let PaymentDetails = BaseUrl+"Orders/order_payemnt_details"

    static let ConsolidatePackagesListUrl = BaseUrl+"Genaral_Services/fetchConsolidatePackagesDetails"
    static let DeletePackageUrl = BaseUrl+"Genaral_Services/deletePackage"
    static let EditPackageUrl = BaseUrl+"Genaral_Services/editConsolidatePackages"

    static let SavePackagesShippingUrl = BaseUrl+"Genaral_Services/savePackageDetails"
    static let CheckSubscriptionPlanUrl = BaseUrl+"Genaral_Services/checkUserMembershipPlan"
    static let ProceedToShippedDetailsUrl = BaseUrl+"Genaral_Services/proceedTOShippedConPackages"
    static let ProceedToPaymentsForSingleProductUrl = BaseUrl+"Genaral_Services/proceedToPaymentSingleProduct"
    static let ProceedToPaymentsForPackagesUrl = BaseUrl+"Genaral_Services/saveShippingAndPackagingDetails"

    static let ReturnAnOrderRequestUrl = BaseUrl+"Genaral_Services/returnRequest"
    static let ProceedToReadyToSendUrl = BaseUrl+"Genaral_Services/proceedfurther"

    static let GetChatSend = BaseUrl+"Chat/send"
    static let GetNotificationsUrl = BaseUrl+"Genaral_Services/getNotifications"
    static let GetChatHistory = BaseUrl+"Chat/chat_history"
    static let GetChatAdminIdUrl = BaseUrl+"Genaral_Services/adminid"
    static let GetSubscriptionPlanValidationUrl = BaseUrl+"Genaral_Services/subscription_plan_validatation"

    
    
    
}
struct PARAMS{
    //MARK:-  Parameters
    static let username = "username"
    static let password = "password"
    static let deviceid = "deviceid"
    static let devicetype = "devicetype"
    static let devicetoken = "devicetoken"
    static let otp = "otp"
    static let mobile = "mobile"
    static let email = "email"
    static let address = "address"
    static let street = "street"
    static let flotNo = "flot_no"
    static let landMark = "land_mark"
    static let pincode = "pincode"
    static let ltd = "ltd"
    static let lng = "lng"
    static let first_name = "first_name"
    static let last_name = "last_name"
    static let address1 = "address1"
    static let address2 = "address2"
    static let c_code = "c_code"
    static let cpassword = "cpassword"
    static let curentpassword = "curentpassword"
    //MARK:-addAddress(NewAddressVC)
    static let uid = "uid"
    static let address_type = "address_type"
    static let addaddress = "address"
    static let country = "country"
    static let state = "state"
    static let addltd = "ltd"
    static let addlng = "lng"
    static let id = "id"
    static let address_1 = "address_1"
    static let state_id = "state_id"
    static let city_id = "city_id"
    static let postal_code = "postal_code"
    //MARK:- Currency Converter & Cost Calculator
    static let code1 = "code1"
    static let code2 = "code2"
    static let amount = "amount"
    static let height = "height"
    static let width = "width"
    static let depth = "depth"
    static let actual_weight = "actual_weight"
    
    
    //MARK:- BuyForMe
    static let product_name = "product_name"
    static let product_discription = "product_discription"
    static let product_link = "product_link"
    static let product_quantity = "product_quantity"
    static let additional_note = "additional_note"
    static let address_id = "address_id"
    static let booking_date = "booking_date"
    static let referance_images = "referance_images"
    static let type = "type"
    static let product_id = "product_id"
    //MARK:- Book A Package
    static let ecomerce_plotform = "ecomerce_plotform"
    static let product_type = "product_type"
    static let product_delivery = "product_delivery"
    static let expected_date = "expected_date"
    
    //MARK:- Plans
    static let plan_id = "plan_id"
    static let sub_plan_id = "sub_plan_id"
    
    //MARK:- Order History
    static let month = "month"
    static let order_type = "order_type"
    static let pickup_id = "pickup_id"
    static let return_reason_id = "return_reason_id"
    
    //MARK:- Consolidate List
    static let country_id = "country_id"
    static let order_ids = "order_ids"
    static let packages_id = "packages_id"
    static let shipping_option_id = "shipping_option_id"
    static let extra_packaging_id = "extra_packaging_id"
    static let booking_package_id = "booking_package_id"
    static let package_id = "package_id"
    static let package_order_id = "package_order_id"
    
    //Payment
    static let order_id = "order_id"
    static let txn_id = "txn_id"
    static let payment_status = "payment_status"
    static let date_time = "date_time"
}

struct HEADER {
    static let API_KEY = "API-KEY"
    static let API_KEY_VALUE = "827ccb0eea8a706c4c34a16891f84e7b"
}

