//
//  AlertActions.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

/*public func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
    let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
    vc.present(alert, animated: true, completion: nil)
    
    DispatchQueue.main.asyncAfter(deadline: .now()+2) {
        alert.dismiss(animated: true, completion: nil)
    }
}
public func showAlertWithOkAction(_ vc:UIViewController,title:String,message:String,buttonTitle:String,buttonAction:@escaping ()-> Void) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.view.tintColor = .green
    alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: {(alert: UIAlertAction!) in
        buttonAction()
    }))
    
    vc.present(alert, animated: true, completion: nil)
}*/
public func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) {
    
    let alertController = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert)
    alertController.view.tintColor = .systemBlue
    let dismissAction = UIAlertAction(title: "Ok", style: .cancel)
    alertController.addAction(dismissAction)
    vc.present(alertController, animated: true)
}

public func showAlertWithOkAction(_ vc:UIViewController,title:String,message:String,buttonTitle:String,buttonAction:@escaping ()-> Void) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.view.tintColor = .systemBlue
    alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: {(alert: UIAlertAction!) in
        buttonAction()
    }))
    
    vc.present(alert, animated: true, completion: nil)
}

