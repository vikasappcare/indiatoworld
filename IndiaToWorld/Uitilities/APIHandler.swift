//
//  APIHandler.swift
//  IndiaToWorld
//
//  Created by Bhasker on 18/01/21.
//

import Foundation
import Alamofire
//import NVActivityIndicatorView

enum Methods{
    case get,post
}

class ApiHandler{
    
    static func getData(url:String,parameters: Parameters?,method:Methods, headers:HTTPHeaders?,view:UIViewController,completion: @escaping (_ data: Data?,_ value: [String:Any]?, _ error: Error?)->Void){
            if Connectivity.isConnectedToInternet() {
                print("Yes! internet is available.")

            switch method{
            case .get:
                AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
//                    guard let error = response.error else {return}

                    if let data = response.data,let value = response.value{
                        print(value)
                        completion(data,value as? [String:Any],nil)
                    }else{
                        print("Error")
                        completion(nil,nil,response.error)
                    }
                }
            case .post:
                AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
//                    guard let error = response.error else {return}

                    if let data = response.data,let value = response.value{
                        print(value)
                        completion(data,value as? [String:Any],nil)
                    }else{
                        print("Error")
                        completion(nil,nil,response.error)
                    }
                }
            }
        }else{
            //hideActivityIndicator()
           // toastMessage(INTERNET_MSG)
       }
    }
    
    static func getJsonData(url:String,parameters: Parameters?,method:Methods, headers:HTTPHeaders?,view:UIViewController,completion: @escaping (_ data: [String:Any]?,_ error: Error?)->Void){
            if Connectivity.isConnectedToInternet() {
                print("Yes! internet is available.")

            switch method{
            case .get:
                print("Get")
            case .post:
                AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
//                    guard let error = response.error else {return}

                    if let _ = response.data,let value = response.value{
                        print(value)
                        completion(value as? [String : Any],nil)
                    }else{
                        print("Error")
                        completion(nil,response.error)
                    }
                }
            }
        }else{
            //hideActivityIndicator()
           // toastMessage(INTERNET_MSG)
       }
    }
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

