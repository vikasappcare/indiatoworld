//
//  OTPVerifyVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class OTPVerifyVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var firstTF: UITextField!
    @IBOutlet weak var secondTF: UITextField!
    @IBOutlet weak var thirdTF: UITextField!
    @IBOutlet weak var fourthTF: UITextField!

    var comingFrom = String()
    var getMobileNumberStr = String()
    var getEmailStr = String()
    var defaults = UserDefaults.standard
    var logInDataDictModel: LoginModelDict?
    var otpStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.OTP_Verification)
        configureView()
    }
    
    func configureView() {
        
        firstTF.textAlignment = .center
        secondTF.textAlignment = .center
        thirdTF.textAlignment = .center
        fourthTF.textAlignment = .center
        
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        
        
        firstTF.becomeFirstResponder()
        
        firstTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        secondTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        thirdTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        fourthTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)

    }
    
    //MARK: -  IB ACTIONS
    //verify
    @IBAction func verifyAction(_ sender: Any) {
        
            self.firstTF.text = self.firstTF.text?.trimmingCharacters(in: .whitespaces)
            self.secondTF.text = self.secondTF.text?.trimmingCharacters(in: .whitespaces)
            self.thirdTF.text = self.thirdTF.text?.trimmingCharacters(in: .whitespaces)
            self.fourthTF.text = self.fourthTF.text?.trimmingCharacters(in: .whitespaces)
            
            if otpStr.count == 4 {
                
                userOtpVerifyApi()
                
            }else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterOtp)
            }
            
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.NewPasswordVC) as! NewPasswordVC
//
//        vc.comingFrom = comingFrom
//
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if  text?.count == 1 {
            
            switch textField {
            
            case firstTF:
                secondTF.becomeFirstResponder()
                
            case secondTF:
                thirdTF.becomeFirstResponder()
                
            case thirdTF:
                fourthTF.becomeFirstResponder()
                
            case fourthTF:
                fourthTF.becomeFirstResponder()
                
                otpStr = String(format: "%@%@%@%@", firstTF.text ?? "", secondTF.text ?? "", thirdTF.text ?? "", fourthTF.text ?? "")
                print(" Entered OTP textfieleds ========\(otpStr)")
                
            default:
                
                break
            }
        }
        
        if  text?.count == 0 {
            
            switch textField {
            
            case firstTF:
                firstTF.becomeFirstResponder()
            case secondTF:
                firstTF.becomeFirstResponder()
            case thirdTF:
                secondTF.becomeFirstResponder()
            case fourthTF:
                thirdTF.becomeFirstResponder()
                
            default:
                
                break
            }
            
        } else {
        }
    }
}
//MARK:-  API Services

extension OTPVerifyVC {
    
    func userOtpVerifyApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [PARAMS.otp:otpStr,
                                            PARAMS.email:getEmailStr
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.OtpVerifyUrl)")

            ApiHandler.getData(url: AppUrls.OtpVerifyUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(LoginModel.self, from: data){
                            if loginData.status == 200{
                                
                                let dataArr = loginData.data
                                self.logInDataDictModel = dataArr[0]
                                print("Dict Data: \(String(describing: self.logInDataDictModel))")
//                                let userId = self.logInDataDictModel?.uid
//
//                                // UserDefaults.standard.set(self.logInDataDict, forKey: LOGIN_RESPONSE)
//                                self.defaults.set(userId, forKey: USER_ID)
////                                self.defaults.set(self.logInDataDictModel?.gstNo, forKey: GST_NO)
//                                self.defaults.set(self.logInDataDictModel?.email, forKey: EMAIL_ID)
//                                self.defaults.set(self.logInDataDictModel?.mobile, forKey: MOBILE_NO)
//                                self.defaults.set(self.logInDataDictModel?.firstName, forKey: FULL_NAME)
//
//                                self.defaults.set(true, forKey: LOGGED_IN)
//                                self.defaults.synchronize()
                                
                                showAlertWithOkAction(self, title: STATUS, message: loginData.message , buttonTitle: "OK") {
                                    DispatchQueue.main.async {
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.NewPasswordVC) as! NewPasswordVC
                                        vc.emailStr = self.getEmailStr
                                        self.navigationController?.pushViewController(vc, animated: true)

                                    }
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
  /*
    func resendOtpApi(){
        
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.contact:getMobileNumberStr,
                PARAMS.deviceid:"1234567",
                PARAMS.devicetype:"ios",
                PARAMS.devicetoken:"121213"
            ]
            
            ApiHandler.getJsonData(url: AppUrls.reSendOtpUrl, parameters: parameters, method: .post, headers: nil, view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {
                        }
                        
                    }else {
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }*/
}
extension OTPVerifyVC: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 1
   }
}
