//
//  ForgotPasswordVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var emailIDTF: UITextField!
    
    var comingFrom = String()
    var logInDataDictModel: LoginModelDict?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Forgot_Password)
        
    }
    
    //MARK:- Selector
    @IBAction func sendOTPAction(_ sender: Any) {
        
        self.emailIDTF.text = self.emailIDTF.text?.trimmingCharacters(in: .whitespaces)
        if emailIDTF.hasText {
            
            if AppHelper.isValidEmail(with: emailIDTF.text){
                
                forgotPasswordApi()
                
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailFormat)
            }
            
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailID)
        }

//        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.OTPVerifyVC) as! OTPVerifyVC
//        vc.comingFrom = comingFrom
//        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
//MARK:-  API Services

extension ForgotPasswordVC {
    
    func forgotPasswordApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.email:emailIDTF.text ?? "",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ForgotPasswordUrl)")

            ApiHandler.getData(url: AppUrls.ForgotPasswordUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(LoginModel.self, from: data){
                            if loginData.status == 200{
                                let dataArr = loginData.data
                                self.logInDataDictModel = dataArr[0]
                                print("Dict Data: \(String(describing: self.logInDataDictModel))")

                                showAlertWithOkAction(self, title: STATUS, message: loginData.message , buttonTitle: "OK") {
                                    DispatchQueue.main.async {
                                        
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.OTPVerifyVC) as! OTPVerifyVC
                                        vc.getMobileNumberStr = self.logInDataDictModel?.mobile ?? ""
                                        vc.getEmailStr = self.logInDataDictModel?.email ?? ""
                                        vc.comingFrom = self.comingFrom
                                        self.navigationController?.pushViewController(vc, animated: true)

                                    }
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
