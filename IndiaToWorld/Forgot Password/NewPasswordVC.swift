//
//  NewPasswordVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class NewPasswordVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var newPswdTF: UITextField!
    @IBOutlet weak var confirmNewPswdTF: UITextField!
    @IBOutlet weak var newPswdBtn: UIButton!
    @IBOutlet weak var confirmPswdBtn: UIButton!
    
    var comingFrom = String()
    var emailStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.New_Password)
    }
    
    //MARK:- selector
    @IBAction func passwordProtectAction(_ sender: UIButton) {
        
        if sender.tag == 99 {
            //new pswd
            newPswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            newPswdTF.isSecureTextEntry = false
            
        }else if sender.tag == 98{
            //confirm pswd
            confirmPswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            confirmNewPswdTF.isSecureTextEntry = false
        }
        
        
    }
    @IBAction func changePswdAction(_ sender: Any) {
        
        self.newPswdTF.text = self.newPswdTF.text?.trimmingCharacters(in: .whitespaces)
        self.confirmNewPswdTF.text = self.confirmNewPswdTF.text?.trimmingCharacters(in: .whitespaces)

        if newPswdTF.hasText {
            
            if newPswdTF.hasText{
                
                passwordUpdateApi()
                
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.confirmNewPswd)
            }
            
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.newPswd)
        }
    }
    
}
//MARK:-  API Services

extension NewPasswordVC {
    
    func passwordUpdateApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                                           PARAMS.password:newPswdTF.text ?? "",
                                            PARAMS.email: emailStr,
                                            PARAMS.cpassword:confirmNewPswdTF.text ?? "",
            ]
            
            ApiHandler.getJsonData(url: AppUrls.PasswordUpdateUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {
                            if self.comingFrom == "Settings" {
                                self.navigationController?.backToViewController(vc: SettingsVC.self)
                            }else{
                                self.navigationController?.backToViewController(vc: LoginViewController.self)
                            }
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
