//
//  HistoryVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import DropDown

class HistoryVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var monthNameLbl: UILabel!
    @IBOutlet weak var dropDownBtn: CustomButton!
   
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    var orderHistoryArr = [OrderHistoryArr]()
    let monthCodeDropDown = DropDown()
    var currentYear = String()
    var selectedMonth = String()
    var selectedMonthArr = [String]()
    var orderType = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.register(ConsolidateTableViewCell.nib(), forCellReuseIdentifier: ConsolidateTableViewCell.identifier)
        selectedMonthArr = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        
        self.monthCodeDropDown.anchorView = self.monthNameLbl
        self.monthCodeDropDown.bottomOffset = CGPoint(x: 0, y: self.monthNameLbl.bounds.height+15)
        self.monthCodeDropDown.dataSource = ["Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"]
        self.monthCodeDropDown.selectionAction = { [weak self] (index, item) in
            self?.monthNameLbl.text = item
            self?.selectedMonth = self?.selectedMonthArr[index] ?? "0"
            
            self?.getOrderHistoryApi()
        }
        selectedMonth = "01"

        tableview.tableFooterView = UIView()
        tableview.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        orderType = "all"
        getCurrentYear()
        getOrderHistoryApi()

    }
    
    func getCurrentYear() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        currentYear = formatter.string(from: Date())
        print("Current Date : \(currentYear)")

    }

    //MARK:-  IB Actions
    
    @objc func onClickViewMoreBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.ViewMoreVC) as! ViewMoreVC
            vc.productId = self.orderHistoryArr[sender.tag].productID
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    @IBAction func onClickMonthDropDownBtn(_ sender: UIButton) {
        
        monthCodeDropDown.show()
    }
    
    @IBAction func onClickSegmentController(_ sender: UISegmentedControl) {
        
       if segmentControl.selectedSegmentIndex == 0 {
            
        orderType = "all"
       }else {
        orderType = "return"
       }
        
        getOrderHistoryApi()
    }
    
}
//MARK:- Extensions
extension HistoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistoryArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableview.dequeueReusableCell(withIdentifier: ConsolidateTableViewCell.identifier, for: indexPath) as? ConsolidateTableViewCell else{return UITableViewCell()}
        
        if orderHistoryArr.count > 0 {
        cell.loadOrderHistoryData(historyItem: orderHistoryArr[indexPath.row])
        }
        cell.viewMoreBtn.tag = indexPath.row
         cell.viewMoreBtn.addTarget(self, action: #selector(onClickViewMoreBtn), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 240
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}
//MARK:-  API Services

extension HistoryVC {
    
    func getOrderHistoryApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            let month = String(format: "%@-%@",currentYear,selectedMonth)
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.order_type:orderType,
                PARAMS.month: month
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AllOrderHistoryListUrl)")

            ApiHandler.getData(url: AppUrls.AllOrderHistoryListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(OrderHistoryModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.orderHistoryArr = []
                                self.orderHistoryArr = loginData.data
                                print("Dict Data: \(String(describing: self.orderHistoryArr))")
                                if self.orderHistoryArr.count > 0 {
                                    self.tableview.isHidden = false
                                    DispatchQueue.main.async {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.reloadData()

                                    }
                                }else {
                                    
                                    self.tableview.isHidden = true
                                }

                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                                self.tableview.isHidden = true

                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.tableview.isHidden = true

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
