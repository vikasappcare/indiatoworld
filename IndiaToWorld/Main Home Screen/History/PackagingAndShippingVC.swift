//
//  PackagingAndShippingVC.swift
//  IndiaToWorld
//
//  Created by Bhasker on 06/01/21.
//

import UIKit
import Braintree
import Razorpay


class PackagingAndShippingVC: UIViewController {
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var shippingOptionsTblVw: UITableView!
    @IBOutlet weak var packagingOptionsTblVw: UITableView!
    @IBOutlet weak var paymentContinueBtn: CustomButton!
    
    
    var packagingAndShippingArr : PackagingAndShippingArr?
    var shippingOptionsArray = [ShippingOption]()
    var packagingArray = [PackagingOption]()
    var packageDetailsArr = [ShippedPackagesDetailsArr]()
    var isCheckBoxSelected = Bool()
    var countryId = String()
    var packagesIdsArr = [String]()
    var packageId = String()
    var shippingId = String()
    var uniquePackageOrderId = String()
    var isFromProceed = Bool()
    var bookingPackageId = String()
    var braintreeClient: BTAPIClient!
    var razorpay: RazorpayCheckout!
    var transcationId = String()

    
    var contryId = String()

    //MARK:-  View Life Cycle
    
    
    
    //MARK:-
    var singleSelectedAddress = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Packaging_Shipping)
        
        isCheckBoxSelected = false
        shippingOptionsTblVw.tableFooterView = UIView()
        shippingOptionsTblVw.separatorStyle = .none
        packagingOptionsTblVw.tableFooterView = UIView()
        packagingOptionsTblVw.separatorStyle = .none
        razorpay = RazorpayCheckout.initWithKey("rzp_test_2dpgkg40hOPhTo", andDelegate: self)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getPackagingAndShippingListApi()
        
        if isFromProceed == true {
            
            
        }else {
            getProceedTOShippedConPackagesApi()

        }

    }
    
    @IBAction func currencyBtnAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.CurrencyConvertorVC) as! CurrencyConvertorVC
        navigationController?.pushViewController(vc, animated: true)
    }
    internal func showPaymentForm(){
        
        let options: [String:Any] = [
                    "amount": "1000", //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "purchase description",
//                    "order_id": "order_DBJOWzybf0sJbb",
                    "image": "",
                    "name": "Razor Pay Payment",
                    "prefill": [
                        "contact": "123456789",
                        "email": "test@gmail.com"
                    ],
                    "theme": [
                        "color": "#012A93"
                      ]
                ]
        razorpay.open(options)
    }
    
    func startCheckout() {
        // Example: Initialize BTAPIClient, if you haven't already
        braintreeClient = BTAPIClient(authorization: "sandbox_d5p6q2pz_2f949w5zx27gyk9q")
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self // Optional

        // Specify the transaction amount here. "2.32" is used in this example.
        let request = BTPayPalRequest(amount: "2.32")
        request.currencyCode = "USD" // Optional; see BTPayPalRequest.h for more options

        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")

                // Access additional information
                _ = tokenizedPayPalAccount.email
                _ = tokenizedPayPalAccount.firstName
                _ = tokenizedPayPalAccount.lastName
                _ = tokenizedPayPalAccount.phone

                // See BTPostalAddress.h for details
                _ = tokenizedPayPalAccount.billingAddress
                _ = tokenizedPayPalAccount.shippingAddress
            } else if error != nil {
                // Handle error here...
            } else {
                // Buyer canceled payment approval
            }
        }
    }
    //MARK:-  IB Actions
    
    @IBAction func onClickPaymentContinueBtn(_ sender: Any) {
        
        if isFromProceed == true{
            if shippingId != "" {
                
                if packageId != "" {
                    
                    proceedPaymentForSingleProduct()

                }else {
                    showAlertMessage(vc: self, titleStr: "", messageStr: "Please select any package option for proceed to shipment")
                }
            }else {
                
                showAlertMessage(vc: self, titleStr: "", messageStr: "Please select any shipping option for proceed to shipment")

            }
        }else {
            
            proceedToPaymentForPackagesApi()
        }
        
//        startCheckout()

        
//        DispatchQueue.main.async {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
                
    }
}

extension PackagingAndShippingVC: RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        print(str)
         
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.TabBarController) as! TabBarController
            isSelectedMenu = 1
            self.navigationController?.pushViewController(vc, animated: true)

            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
//            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        print(payment_id)
        transcationId = payment_id
//        userPaymentStatusAPI()
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }

    }
}
//MARK:-  UITableViewDelegate, UITableViewDataSource
extension PackagingAndShippingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == shippingOptionsTblVw {
            
            return shippingOptionsArray.count
            
        }else {
            
            return packagingArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == shippingOptionsTblVw {
            
            let cell = shippingOptionsTblVw.dequeueReusableCell(withIdentifier: ShippingOptionsTableViewCell.identifier, for: indexPath) as! ShippingOptionsTableViewCell
            
            cell.shippingOptionLbl.text = shippingOptionsArray[indexPath.row].title
            return cell
            
            
        }else {
            
            let cell = packagingOptionsTblVw.dequeueReusableCell(withIdentifier: PackagingOptionsTableViewCell.identifier, for: indexPath) as! PackagingOptionsTableViewCell
            
            cell.packageOptionLbl.text = String(format: "%@ - %@", packagingArray[indexPath.row].title,packagingArray[indexPath.row].price)
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == shippingOptionsTblVw {
          
            let cell = shippingOptionsTblVw.cellForRow(at: indexPath) as! ShippingOptionsTableViewCell
       cell.shippingImgVw.image = UIImage(named: "CheckRadioButton")
         //   cell.shippingOptionImgVw.image = UIImage(named: "SelcetedIcon")
          //  SelcetedIcon
            shippingId = shippingOptionsArray[indexPath.row].id
        }else {
            
            let cell = packagingOptionsTblVw.cellForRow(at: indexPath) as! PackagingOptionsTableViewCell
            cell.packageImgVw.image = UIImage(named: "SelcetedIcon")
            packageId = packagingArray[indexPath.row].id

            packagesIdsArr.append(packagingArray[indexPath.row].id ?? "")
            print("Packages Id : \(packagesIdsArr)")

        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == shippingOptionsTblVw {
          
            let cell = shippingOptionsTblVw.cellForRow(at: indexPath) as! ShippingOptionsTableViewCell
//            cell.shippingOptionImgVw.image = UIImage(named: "UnCheckRadioButton")
            cell.shippingImgVw.image = UIImage(named: "UnCheckRadioButton")
           // Checkbox
            shippingId = ""
        }else {
            
            let cell = packagingOptionsTblVw.cellForRow(at: indexPath) as! PackagingOptionsTableViewCell
            cell.packageImgVw.image = UIImage(named: "Checkbox")
            packageId = ""
            
            if packagesIdsArr.count == 1 {
                
                packagesIdsArr.removeAll()
                
            }else {
                
                for i in 0..<packagesIdsArr.count {
                    
                    let catId = packagesIdsArr[i]
                    
                  if catId == packagingArray[indexPath.row].id {
                        
                    packagesIdsArr.remove(at: i)
                    print(packagesIdsArr)
                    break
                       
                    
                    }else{

                    }
                   
                }
            }
        }
    }
    
}
extension PackagingAndShippingVC: BTViewControllerPresentingDelegate,BTAppSwitchDelegate{
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        
    }
    
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }

}
//MARK:-  API Services

extension PackagingAndShippingVC {
    
    func getPackagingAndShippingListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                             
            print("UrlString:  \(AppUrls.ShippingAndPackageDetailsUrl)")

            ApiHandler.getData(url: AppUrls.ShippingAndPackageDetailsUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let plansDetails = try? JSONDecoder().decode(PackagingAndShippingModel.self, from: data){
                            if plansDetails.status == 200{
                                self.packagingAndShippingArr = plansDetails.data
                                self.shippingOptionsArray = self.packagingAndShippingArr?.shippingOptions ?? []
                                self.packagingArray = self.packagingAndShippingArr?.packagingOptions ?? []

                                if self.shippingOptionsArray.count > 0 {
                                    
//                                    self.noDataImgVw.isHidden = true
                                    self.shippingOptionsTblVw.isHidden = false
                                    DispatchQueue.main.async {
                                                                                    
                                            self.shippingOptionsTblVw.delegate = self
                                            self.shippingOptionsTblVw.dataSource = self
                                            self.shippingOptionsTblVw.reloadData()

                                    }
                                }else {
                                    self.shippingOptionsTblVw.isHidden = true
                                }
                                
                                if self.packagingArray.count > 0 {
                                    
                                    //                                    self.noDataImgVw.isHidden = true
                                    self.shippingOptionsTblVw.isHidden = false
                                    DispatchQueue.main.async {
                                        
                                        self.packagingOptionsTblVw.delegate = self
                                        self.packagingOptionsTblVw.dataSource = self
                                        self.packagingOptionsTblVw.reloadData()
                                        
                                    }
                                }else {
                                    self.packagingOptionsTblVw.isHidden = true
                                }

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: plansDetails.message)
                                self.shippingOptionsTblVw.isHidden = true
                                self.packagingOptionsTblVw.isHidden = true

                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                            self.shippingOptionsTblVw.isHidden = true
                            self.packagingOptionsTblVw.isHidden = true

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    //MARK:- For unique order id for proceed to payment
    func getProceedTOShippedConPackagesApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                    
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.country_id : countryId
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ProceedToShippedDetailsUrl)")

            ApiHandler.getData(url: AppUrls.ProceedToShippedDetailsUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let packageDetails = try? JSONDecoder().decode(ShippedPackagesDetailsModel.self, from: data){
                            if packageDetails.status == 200{
                                self.packageDetailsArr = packageDetails.data
                                print(self.packageDetailsArr)

                                self.uniquePackageOrderId = self.packageDetailsArr[0].id
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: packageDetails.message)
                            }
                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func proceedToPaymentForPackagesApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.shipping_option_id: shippingId,
                PARAMS.extra_packaging_id : packageId,
                PARAMS.package_order_id: uniquePackageOrderId,
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ProceedToPaymentsForPackagesUrl)")

            ApiHandler.getJsonData(url: AppUrls.ProceedToPaymentsForPackagesUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            
//                            self.showPaymentForm()
                            DispatchQueue.main.async {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                                vc.isFromOrderPayment = "Order payment"
                                vc.bookingId = data["order_id"] as? String ?? ""
                                self.navigationController?.pushViewController(vc, animated: true)

                            }
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func proceedPaymentForSingleProduct() {
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.shipping_option_id: shippingId,
                PARAMS.extra_packaging_id : packageId,
                PARAMS.booking_package_id: bookingPackageId,
                PARAMS.address_id:singleSelectedAddress,
            ]
            
            print("UrlString: \(AppUrls.ProceedToPaymentsForSingleProductUrl)")
            print("Parameters: \(parameters)")

            ApiHandler.getJsonData(url: AppUrls.ProceedToPaymentsForSingleProductUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

//                            self.showPaymentForm()
                            DispatchQueue.main.async {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                                self.navigationController?.pushViewController(vc, animated: true)

                            }
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func userPaymentStatusAPI(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.order_id: "",
                PARAMS.txn_id: transcationId,
                PARAMS.payment_status: "",
                PARAMS.date_time:""
            ]
            
            ApiHandler.getJsonData(url: AppUrls.UserPaymentStatusUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            self.navigationController?.popViewController(animated: true)
//                            self.navigationController?.backToViewController(vc: ReadyToSendVC.self)
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessPaymentVC) as! SuccessPaymentVC
//                            self.navigationController?.pushViewController(vc, animated: true)

//                            self.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
