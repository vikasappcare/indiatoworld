//
//  ReturnFormVC.swift
//  IndiaToWorld
//
//  Created by Bhasker on 06/01/21.
//

import UIKit
import Razorpay


class ReturnFormVC: UIViewController {

    @IBOutlet weak var returnTblVw: UITableView!
    @IBOutlet weak var pickUpTimeTblVw: UITableView!
    @IBOutlet weak var wareHouseAddressLbl: UILabel!
    @IBOutlet weak var submitBtn: CustomButton!
    
    var returnTypeArray = [String]()
    var pickUpTimeArray = [PickupTime]()
    var isCheckBoxSelected = Bool()
    var orderId = String()
    var returnTypeId = String()
    var pickUpId = String()
    var razorpay: RazorpayCheckout!
    var transcationId = String()
    var amount = String()
    var paymentStatus = String()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navTitle(heading: NavTitle.Return_Form)
        isCheckBoxSelected = false
        returnTblVw.tableFooterView = UIView()
        returnTblVw.separatorStyle = .none
        pickUpTimeTblVw.tableFooterView = UIView()
        pickUpTimeTblVw.separatorStyle = .none
        razorpay = RazorpayCheckout.initWithKey("rzp_test_2dpgkg40hOPhTo", andDelegate: self)

        getReturnFormListApi()
        // Do any additional setup after loading the view.
    }
    
    
    internal func showPaymentForm(){
        
        let options: [String:Any] = [
                    "amount": amount, //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "purchase description",
//                    "order_id": "order_DBJOWzybf0sJbb",
                    "image": "",
                    "name": AppHelper.getFullName(),
                    "prefill": [
                        "contact": AppHelper.getMobileNo(),
                        "email": AppHelper.getEmailId()
                    ],
                    "theme": [
                        "color": "#012A93"
                      ]
                ]
        razorpay.open(options)
    }

    //MARK:-  IB Actions
    
    @objc func onClickPlanSelectedCheckBoxBtn(_ sender: UIButton){
        
        let cell = returnTblVw.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? ReturnFormTableViewCell
//        if isCheckBoxSelected == false {
//
//            cell?.planSelectedCheckBoxImgVw.image = UIImage(named: "SelcetedIcon")
//            isCheckBoxSelected = true
//
//        }else {
//
//            cell?.planSelectedCheckBoxImgVw?.image = UIImage(named: "uncheck_box")
//            isCheckBoxSelected = false
//
//        }
//        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        
        if returnTypeId != "" {
            if pickUpId != "" {
                
                returnAnOrderApi()

            }else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: "Please select pick up time")
            }
            
        }else {
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: "Please select reason for return")

        }
//        DispatchQueue.main.async {
//
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.PackagingAndShippingVC) as! PackagingAndShippingVC
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }

    }
    
}
//MARK:-  RazorpayPaymentCompletionProtocol

extension ReturnFormVC: RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        print(str)
        paymentStatus = "Failed"
         showAlertMessage(vc: self, titleStr: "", messageStr: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        print(payment_id)
        paymentStatus = "Success"
        transcationId = payment_id
        userPaymentStatusAPI()

    }
}
//MARK:-  UITableViewDelegate, UITableViewDataSource
extension ReturnFormVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == returnTblVw {
            
            return returnTypeArray.count

        }else {
            
            return pickUpTimeArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == returnTblVw {
            
            let cell = returnTblVw.dequeueReusableCell(withIdentifier: ReturnFormTableViewCell.identifier, for: indexPath) as! ReturnFormTableViewCell
            
            cell.returnTypeLbl.text = returnTypeArray[indexPath.row]
            return cell

            
        }else {
            
            let cell = pickUpTimeTblVw.dequeueReusableCell(withIdentifier: PickUpTimeTableViewCell.identifier, for: indexPath) as! PickUpTimeTableViewCell
            
            cell.pickUpTimeLbl.text = String(format: "%@ - %@", pickUpTimeArray[indexPath.row].period,pickUpTimeArray[indexPath.row].timeSlot)
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == returnTblVw {
            
            let cell = returnTblVw.cellForRow(at: indexPath) as! ReturnFormTableViewCell
       cell.returnOptionImgVw.image = UIImage(named: "CheckRadioButton")

            returnTypeId = returnTypeArray[indexPath.row]

            
        }else {
            
            let cell = pickUpTimeTblVw.cellForRow(at: indexPath) as! PickUpTimeTableViewCell
            cell.pickUpTimeImgVw.image = UIImage(named: "CheckRadioButton")

            pickUpId = String(format: "%@ - %@", pickUpTimeArray[indexPath.row].period,pickUpTimeArray[indexPath.row].timeSlot)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == returnTblVw {
          
            let cell = returnTblVw.cellForRow(at: indexPath) as! ReturnFormTableViewCell
            cell.returnOptionImgVw.image = UIImage(named: "UnCheckRadioButton")
            returnTypeId = ""
        }else {
            
            let cell = pickUpTimeTblVw.cellForRow(at: indexPath) as! PickUpTimeTableViewCell
            cell.pickUpTimeImgVw.image = UIImage(named: "UnCheckRadioButton")
            pickUpId = ""
            
        }
    }
    
}
//MARK:-  API Services

extension ReturnFormVC {
    
    func getReturnFormListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                    
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId()
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ReturnFormUrl)")

            ApiHandler.getData(url: AppUrls.ReturnFormUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let plansDetails = try? JSONDecoder().decode(ReturnFormModel.self, from: data){
                            if plansDetails.status == 200{
                                let returnDict = plansDetails.data
                                print(returnDict)
                                self.returnTypeArray = returnDict.returnReason
                                self.pickUpTimeArray = returnDict.pickupTime

                                if self.returnTypeArray.count > 0 {
                                    
//                                    self.noDataImgVw.isHidden = true
                                    self.returnTblVw.isHidden = false
                                    DispatchQueue.main.async {
                                                                                    
                                            self.returnTblVw.delegate = self
                                            self.returnTblVw.dataSource = self
                                            self.returnTblVw.reloadData()

                                    }
                                }else {
                                    self.returnTblVw.isHidden = true
                                }
                                
                                if self.pickUpTimeArray.count > 0 {
                                    
                                    //                                    self.noDataImgVw.isHidden = true
                                    self.pickUpTimeTblVw.isHidden = false
                                    DispatchQueue.main.async {
                                        
                                        self.pickUpTimeTblVw.delegate = self
                                        self.pickUpTimeTblVw.dataSource = self
                                        self.pickUpTimeTblVw.reloadData()
                                        
                                    }
                                }else {
                                    self.pickUpTimeTblVw.isHidden = true
                                }

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: plansDetails.message)
                                self.returnTblVw.isHidden = true
                                self.pickUpTimeTblVw.isHidden = true

                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                            self.returnTblVw.isHidden = true
                            self.pickUpTimeTblVw.isHidden = true

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func returnAnOrderApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.order_id:orderId,
                PARAMS.uid: AppHelper.getUserId(),
                PARAMS.return_reason_id: returnTypeId,
                PARAMS.pickup_id:pickUpId,
                 
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ReturnAnOrderRequestUrl)")

            ApiHandler.getJsonData(url: AppUrls.ReturnAnOrderRequestUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        

                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "", buttonTitle: "Ok") {
                            
                            self.amount = data["Amount"] as? String ?? ""
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.ReturnConformationViewController) as! ReturnConformationViewController
                            vc.amount = self.amount
                            vc.orderId = self.orderId
                            vc.planInfo = data["plan_info"] as? String ?? ""
                            vc.modalPresentationStyle = .overCurrentContext
                            self.navigationController?.present(vc, animated: false, completion: nil)

//                            self.showPaymentForm()

//                            self.tabBarController?.selectedIndex = 1
                        }
                        
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func userPaymentStatusAPI(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.order_id: orderId,
                PARAMS.txn_id: transcationId,
                PARAMS.payment_status: paymentStatus,
                PARAMS.date_time:""
            ]
            
            ApiHandler.getJsonData(url: AppUrls.UserPaymentStatusUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            self.tabBarController?.selectedIndex = 1

//                            self.navigationController?.popViewController(animated: true)
//                            self.navigationController?.backToViewController(vc: ReadyToSendVC.self)
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessPaymentVC) as! SuccessPaymentVC
//                            self.navigationController?.pushViewController(vc, animated: true)

//                            self.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
