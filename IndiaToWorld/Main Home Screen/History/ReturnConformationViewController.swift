//
//  ReturnConformationViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 17/05/21.
//

import UIKit
import Razorpay

class ReturnConformationViewController: UIViewController {

    @IBOutlet weak var returnAmountLbl: UILabel!
    
    @IBOutlet weak var planDescriptionLbl: UILabel!
    
    @IBOutlet weak var upgradePlanBtn: UIButton!
    
    @IBOutlet weak var payReturnAmountBtn: UIButton!
    
    @IBOutlet weak var dismissBtn: UIButton!
    
    
    var razorpay: RazorpayCheckout!
    var transcationId = String()
    var amount = String()
    var paymentStatus = String()
    var orderId = String()
    var planInfo = String()
    var toDayDate = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        returnAmountLbl.text = String(format: "₹%@", amount)
        planDescriptionLbl.text = planInfo
        let str = "Pay ₹" + amount
        payReturnAmountBtn.setTitle(str, for: .normal)
        razorpay = RazorpayCheckout.initWithKey("rzp_test_2dpgkg40hOPhTo", andDelegate: self)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        toDayDate = formatter.string(from: Date())
        print("Current Date : \(toDayDate)")

        // Do any additional setup after loading the view.
    }
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        upgradePlanBtn.layer.cornerRadius = 10
        upgradePlanBtn.layer.borderWidth = 1
        upgradePlanBtn.layer.borderColor = #colorLiteral(red: 0.0403117165, green: 0.1795804501, blue: 0.4042866826, alpha: 1)
    }
    internal func showPaymentForm(){
        
        if amount != "" {
        let amount1 = Float(self.amount)! * 100
               let amount2 = Int(amount1)
        let options: [String:Any] = [
                    "amount": amount2, //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "Charges for return an order",
//                    "order_id": "order_DBJOWzybf0sJbb",
                    "image": "",
            "name": AppHelper.getFullName(),
                    "prefill": [
                        "contact": AppHelper.getMobileNo(),
                        "email": AppHelper.getEmailId()
                    ],
                    "theme": [
                        "color": "#012A93"
                      ]
                ]
        razorpay.open(options)
        }
    }
    //MARK:-  IB Actions
    
    
    @IBAction func onClickDismissBtn(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickUpgradePlanBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            
            let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.SubscriptionVC) as! SubscriptionVC
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            vc.isFrom = "Consolidate"

            self.present(nav, animated: true, completion: nil)

//            let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.SubscriptionVC) as!  SubscriptionVC
//            vc.isFrom = "Consolidate"
//            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    
    @IBAction func onClickPayReturnAmountBtn(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        showPaymentForm()
    }
    
    
}
//MARK:-  RazorpayPaymentCompletionProtocol

extension ReturnConformationViewController: RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        print(str)
        paymentStatus = "Failed"
         showAlertMessage(vc: self, titleStr: "", messageStr: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        print(payment_id)
        paymentStatus = "Success"
        transcationId = payment_id
        userPaymentStatusAPI()

    }
}
extension ReturnConformationViewController {
    
    func userPaymentStatusAPI(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.order_id: orderId,
                PARAMS.txn_id: transcationId,
                PARAMS.payment_status: paymentStatus,
                PARAMS.date_time:toDayDate
            ]
            
            ApiHandler.getJsonData(url: AppUrls.UserPaymentStatusUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            self.tabBarController?.selectedIndex = 1

                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
