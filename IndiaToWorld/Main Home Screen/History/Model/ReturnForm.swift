//
//  ReturnForm.swift
//  IndiaToWorld
//
//  Created by Bhasker on 02/02/21.
//

import Foundation
// MARK: - Welcome
struct ReturnFormModel: Codable {
    let status: Int
    let message: String
    let data: ReturnFormDict
}

// MARK: - DataClass
struct ReturnFormDict: Codable {
    let returnReason: [String]
    let pickupTime: [PickupTime]
    let pickupAddress: [PickupAddress]

    enum CodingKeys: String, CodingKey {
        case returnReason = "return_reason"
        case pickupTime = "pickup_time"
        case pickupAddress = "pickup_address"
    }
}

// MARK: - PickupAddress
struct PickupAddress: Codable {
    let warehouseAddress: String

    enum CodingKeys: String, CodingKey {
        case warehouseAddress = "warehouse_address"
    }
}

// MARK: - PickupTime
struct PickupTime: Codable {
    let period, timeSlot: String

    enum CodingKeys: String, CodingKey {
        case period
        case timeSlot = "time_slot"
    }
}
