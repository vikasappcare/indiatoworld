//
//  ShippedPackagesDetails.swift
//  IndiaToWorld
//
//  Created by Bhasker on 12/02/21.
//

import Foundation
// MARK: - Welcome
struct ShippedPackagesDetailsModel: Codable {
    let status: Int
    let message: String
    let data: [ShippedPackagesDetailsArr]
}

// MARK: - Datum
struct ShippedPackagesDetailsArr: Codable {
    let id, uid, orderID, conPackageID: String
    let countryID, shippingOptionID, extraPackagingID: String?
    let addressID, packageCount: String
    let status: String
    let cd: String

    enum CodingKeys: String, CodingKey {
        case id, uid
        case orderID = "order_id"
        case conPackageID = "con_package_id"
        case countryID = "country_id"
        case shippingOptionID = "shipping_option_id"
        case extraPackagingID = "extra_packaging_id"
        case addressID = "address_id"
        case packageCount = "package_count"
        case status, cd
    }
}

