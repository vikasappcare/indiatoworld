//
//  OrderHistory.swift
//  IndiaToWorld
//
//  Created by Bhasker on 02/02/21.
//

import Foundation

//MARK: - Welcome
struct OrderHistoryModel: Codable {
    let status: Int
    let message: String
    let data: [OrderHistoryArr]
}

// MARK: - Datum
struct OrderHistoryArr: Codable {
    let id: String
    let type: String
    let ecomercePlotform, productType, productDelivery, productName: String
    let productDiscription: String
    let productLink: String
    let productQuantity: String
    let orderTypeID: String?
    let additionalNote: String
    let referanceImages: String
    let expectedDate, bookingDate: String
    let invoice: String
    let status, bookingid, orderID, packagingSize: String
    let packagingWeight, packagingWeightCost, shortStorageDays, shortStorageCost: String
    let uid: String
    let cd: String
    let addressID, paymentDetailsStatus: String
    let paymentDetailsDescription: PaymentDetailsDescription
    let productID: String
    let addressType: String
    let address: String
    let state: String
    let country: String
    let ltd: String
    let lng: String
    let adsID: String

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case packagingSize = "packaging_size"
        case packagingWeight = "packaging_weight"
        case packagingWeightCost = "packaging_weight_cost"
        case shortStorageDays = "short_storage_days"
        case shortStorageCost = "short_storage_cost"
        case uid, cd
        case addressID = "address_id"
        case paymentDetailsStatus = "payment_details_status"
        case paymentDetailsDescription = "payment_details_description"
        case productID = "product_id"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
    }
}
