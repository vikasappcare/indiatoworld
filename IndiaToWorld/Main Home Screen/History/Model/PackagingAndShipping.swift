//
//  PackagingAndShipping.swift
//  IndiaToWorld
//
//  Created by Bhasker on 02/02/21.
//

import Foundation
// MARK: - Welcome
struct PackagingAndShippingModel: Codable {
    let status: Int
    let message: String
    let data: PackagingAndShippingArr
}

// MARK: - DataClass
struct PackagingAndShippingArr: Codable {
    let shippingOptions: [ShippingOption]
    let packagingOptions: [PackagingOption]

    enum CodingKeys: String, CodingKey {
        case shippingOptions = "shipping_options"
        case packagingOptions = "packaging_options"
    }
}

// MARK: - PackagingOption
struct PackagingOption: Codable {
    let id, title, price: String
}

// MARK: - ShippingOption
struct ShippingOption: Codable {
    let id, title: String
}
