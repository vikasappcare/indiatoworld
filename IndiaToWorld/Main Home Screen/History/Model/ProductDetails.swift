//
//  ProductDetails.swift
//  IndiaToWorld
//
//  Created by Bhasker on 02/03/21.
//

import Foundation
/*// MARK: - Welcome
struct ProductDetailsModel: Codable {
    let status: Int
    let message: String
    let data: [ProductDetailsArr]
}

// MARK: - Datum
struct ProductDetailsArr: Codable {
    let id, type: String?
    let ecomercePlotform, productType, productDelivery: String?
    let productName, productDiscription, productLink, productQuantity: String?
    let orderTypeID, additionalNote, referanceImages: String?
    let expectedDate: String?
    let bookingDate: String
    let invoice: String?
    let status: String?
    let bookingid: String?
    let orderID, uid, cd, addressID: String?
    let addressType, address, state, ltd: String?
    let lng, country: String?

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case uid, cd
        case addressID = "address_id"
        case addressType = "address_type"
        case address, state, ltd, lng, country
    }
}*/
// MARK: - Welcome
struct ProductDetailsModel: Codable {
    let status: Int
    let message: String
    let data: [ProductDetailsArr]
}

// MARK: - Datum
struct ProductDetailsArr: Codable {
    let id, type, ecomercePlotform, productType: String?
    let productDelivery, productName: String?
    let productDiscription, productLink, productQuantity: String?
    let orderTypeID: String?
    let additionalNote, referanceImages: String?
    let expectedDate: String?
    let bookingDate: String?
    let invoice, status, bookingid: String?
    let orderID: String?
    let uid, cd, addressID, addressType: String?
    let address, state, ltd, lng: String?
    let country: String?

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case uid, cd
        case addressID = "address_id"
        case addressType = "address_type"
        case address, state, ltd, lng, country
    }
}
