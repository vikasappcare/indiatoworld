//
//  ViewMoreVC.swift
//  IndiaToWorld
//
//  Created by Bhasker on 06/01/21.
//

import UIKit

class ViewMoreVC: UIViewController {

    //MARK:-  IB Outlets
    
    @IBOutlet weak var orderIdLbl: UILabel!
    
    @IBOutlet weak var orderIdBgVw: UIView!
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var dashedVw: UIView!
    @IBOutlet weak var productDeliveryLbl: UILabel!
    @IBOutlet weak var ecommercePlatformLbl: UILabel!
    @IBOutlet weak var productTypeLbl: UILabel!
    @IBOutlet weak var expectedDeliveryDateLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var invoiceLbl: UILabel!
    @IBOutlet weak var cancelBtn: CustomButton!
    @IBOutlet weak var contactWareHouseBtn: CustomButton!
    
    @IBOutlet weak var productDeliveryTitleLbl: UILabel!
    @IBOutlet weak var eCommerceTitleLbl: UILabel!
    @IBOutlet weak var productTypeTitleLbl: UILabel!
    @IBOutlet weak var productNameTitleLbl: UILabel!
    @IBOutlet weak var expDateTitleLbl: UILabel!
    @IBOutlet weak var invoiceTitleLbl: UILabel!
    @IBOutlet weak var noDataImgVw: UIImageView!
    
    
    var productDetailsArr = [ProductDetailsArr]()
    var productId = String()
    var productDetailsDict : AllPackagesArr!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navTitle(heading: NavTitle.Product_Details)
        self.noDataImgVw.isHidden = true
        self.getProductDetailsFromDict(productDetail: self.productDetailsDict)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.noDataImgVw.isHidden = false
//        getProductDetailsApi()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        orderIdBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
         drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)

    }
    
    func loadProductDetailsFromDict(productDetail:ProductDetailsArr){
        
        productNameLbl.text = productDetail.productName
        addressLbl.text = ""

        if productDetail.orderID == "" {
            
            orderIdLbl.text = String(format: "OrderId:%@", productDetail.bookingid ?? "")

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", productDetail.orderID ?? "")

        }

        if productDetail.productDiscription == "" || productDetail.productDiscription == nil{
            
            productDeliveryTitleLbl.text = "Product Delivery"
            productDeliveryLbl.text = productDetail.productDelivery

        }else {
            productDeliveryLbl.text = productDetail.productDiscription
            productDeliveryTitleLbl.text = "Product Details"

        }
        
        if productDetail.productLink == "" || productDetail.productLink == nil{
            
            ecommercePlatformLbl.text = productDetail.ecomercePlotform
            eCommerceTitleLbl.text = "E-commerce Platform"

        }else {
            
            ecommercePlatformLbl.text = productDetail.productLink
            eCommerceTitleLbl.text = "Product Link"

        }
        
        if productDetail.productQuantity == "" || productDetail.productQuantity == nil{
            productTypeTitleLbl.text = "Product Type"
            productTypeLbl.text = productDetail.productType

        }else {
            
            productTypeLbl.text = productDetail.productQuantity
            productTypeTitleLbl.text = "Product Quantity"

        }
        
        if productDetail.bookingDate == "" || productDetail.bookingDate == nil{
            expDateTitleLbl.text = "Expected Delivery Date"
            expectedDeliveryDateLbl.text = productDetail.expectedDate

        }else {
            
            expectedDeliveryDateLbl.text = productDetail.bookingDate
            expDateTitleLbl.text = "Booking Date"

        }
        
        if productDetail.referanceImages == "" || productDetail.referanceImages == nil{
            invoiceTitleLbl.text = "Invoice Upload"
            invoiceLbl.text = productDetail.invoice

        }else {
            
            invoiceLbl.text = productDetail.referanceImages
            invoiceTitleLbl.text = "Reference Image"

        }
        
    }
    
    func getProductDetailsFromDict(productDetail:AllPackagesArr){
        
        productNameLbl.text = productDetail.productName
        addressLbl.text = ""

        if productDetail.orderID == "" {
            
            orderIdLbl.text = String(format: "OrderId:%@", productDetail.bookingid )

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", productDetail.orderID )

        }

        if productDetail.productDiscription == "" {
            
            productDeliveryTitleLbl.text = "Product Delivery"
            productDeliveryLbl.text = productDetail.productDelivery

        }else {
            productDeliveryLbl.text = productDetail.productDiscription
            productDeliveryTitleLbl.text = "Product Details"

        }
        
        if productDetail.productLink == "" {
            
            ecommercePlatformLbl.text = productDetail.ecomercePlotform
            eCommerceTitleLbl.text = "E-commerce Platform"

        }else {
            
            ecommercePlatformLbl.text = productDetail.productLink
            eCommerceTitleLbl.text = "Product Link"

        }
        
        if productDetail.productQuantity == "" {
            productTypeTitleLbl.text = "Product Type"
            productTypeLbl.text = productDetail.productType

        }else {
            
            productTypeLbl.text = productDetail.productQuantity
            productTypeTitleLbl.text = "Product Quantity"

        }
        
        if productDetail.bookingDate == "" {
            expDateTitleLbl.text = "Expected Delivery Date"
            expectedDeliveryDateLbl.text = productDetail.expectedDate

        }else {
            
            expectedDeliveryDateLbl.text = productDetail.bookingDate
            expDateTitleLbl.text = "Booking Date"

        }
        
        if productDetail.referanceImages == "" {
            invoiceTitleLbl.text = "Invoice Upload"
            invoiceLbl.text = productDetail.invoice

        }else {
            
            invoiceLbl.text = productDetail.referanceImages
            invoiceTitleLbl.text = "Reference Image"

        }
        
    }
    func loadProductDetails(productDetail:ProductDetailsArr){
        
        if productDetail.type == "buyforme" {
            productDeliveryTitleLbl.text = "Product Details"
            eCommerceTitleLbl.text = "Product Link"
            productTypeTitleLbl.text = "Product Quantity"
            expDateTitleLbl.text = "Booking Date"
            invoiceTitleLbl.text = "Reference Image"
           
            productDeliveryTitleLbl.text = productDetail.productDiscription
            eCommerceTitleLbl.text = productDetail.productLink
            productTypeTitleLbl.text = productDetail.productQuantity
            expDateTitleLbl.text = productDetail.bookingDate
            invoiceTitleLbl.text = productDetail.referanceImages

        }else {
            productDeliveryTitleLbl.text = "Product Delivery"
            eCommerceTitleLbl.text = "E-commerce Platform"
            productTypeTitleLbl.text = "Product Type"
            expDateTitleLbl.text = "Expected Delivery Date"
            invoiceTitleLbl.text = "Invoice Upload"

            productDeliveryTitleLbl.text = productDetail.productDelivery
            eCommerceTitleLbl.text = productDetail.ecomercePlotform
            productTypeTitleLbl.text = productDetail.productType
            expDateTitleLbl.text = productDetail.expectedDate
            invoiceTitleLbl.text = productDetail.invoice

        }
        
        
    }
    //MARK:-  IB Actions
    
    @IBAction func onClickIvoiceBtn(_ sender: UIButton) {
        
       
        let vc = UserProfilePictureViewController.getInstance()
        let nav = UINavigationController(rootViewController: vc)
        vc.profilePicStr = invoiceLbl.text ?? ""
        nav.modalPresentationStyle = .formSheet
        self.present(nav, animated: true, completion: nil)

    }
    
    
    @IBAction func onClickCancelBtn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickContactWareHouseBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {

            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.WareHouseDetailsVC) as! WareHouseDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)

        }

    }
    

}
extension UIViewController{
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.

        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
}
//MARK:-  API Services

extension ViewMoreVC {
    
    func getProductDetailsApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.product_id: productId,
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.productDetailsUrl)")

            ApiHandler.getData(url: AppUrls.productDetailsUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(ProductDetailsModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.productDetailsArr = []
                                self.productDetailsArr = loginData.data
                                print("Dict Data: \(String(describing: self.productDetailsArr))")
                                if self.productDetailsArr.count > 0 {
                                    self.noDataImgVw.isHidden = true
                                    self.loadProductDetailsFromDict(productDetail: self.productDetailsArr[0])
//                                    self.loadProductDetails(productDetail: self.productDetailsArr[0])
                                }else {
                                    
                                    self.noDataImgVw.isHidden = false
                                    
                                }

                            }else {
                                self.hideActivityIndicator()
                                self.noDataImgVw.isHidden = false

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.noDataImgVw.isHidden = false
                            self.hideActivityIndicator()
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
