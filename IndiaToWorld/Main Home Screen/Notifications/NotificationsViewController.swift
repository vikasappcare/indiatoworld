//
//  NotificationsViewController.swift
//  IndiaToWorld
//
//  Created by Appcare on 08/01/21.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var notificationsTb: UITableView!
    
    
    var notificationsArr = [NotificationsArr]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNotificationsAPI()

//        notificationsTb.reloadData()
        notificationsTb.tableFooterView = UIView()
        notificationsTb.rowHeight = UITableView.automaticDimension
        navTitle(heading: NavTitle.Notifications)
    }
    
}
//MARK:-  UITableViewDelegate, UITableViewDataSource
extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationsTb.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as! NotificationsTableViewCell
        cell.selectionStyle = .none
        
        if notificationsArr.count > 0 {
            
            cell.titleLbl.text = notificationsArr[indexPath.row].title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension NotificationsViewController {
    
    func getNotificationsAPI(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.GetNotificationsUrl)")
            
            ApiHandler.getData(url: AppUrls.GetNotificationsUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { [self] (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let notificationsData = try? JSONDecoder().decode(NotificationsModel.self, from: data){
                            if notificationsData.status == 200{
                                let dataArr = notificationsData.data
                                self.notificationsArr = dataArr
                                if notificationsArr.count > 0 {
                                    notificationsTb.isHidden = false

                                    DispatchQueue.main.async {
                                        notificationsTb.reloadData()
                                    }

                                }else{
                                    
                                    notificationsTb.isHidden = true
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: notificationsData.message )
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
