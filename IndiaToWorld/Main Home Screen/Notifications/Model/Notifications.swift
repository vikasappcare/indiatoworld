//
//  Notifications.swift
//  IndiaToWorld
//
//  Created by Bhasker on 01/04/21.
//

import Foundation
// MARK: - Welcome
struct NotificationsModel: Codable {
    let status: Int
    let message: String
    let data: [NotificationsArr]
}

// MARK: - Datum
struct NotificationsArr: Codable {
    let id, uid, title, cd: String
}
