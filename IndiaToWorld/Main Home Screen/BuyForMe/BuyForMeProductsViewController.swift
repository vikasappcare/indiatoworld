//
//  BuyForMeProductsViewController.swift
//  IndiaToWorld
//
//  Created by Appcare on 31/12/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class BuyForMeProductsViewController: UIViewController {
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var productNameTF: CustomTF!
    @IBOutlet weak var productDetailsTF: CustomTextView!
    @IBOutlet weak var productLinkTF: CustomTextView!
    @IBOutlet weak var productQuantityTF: CustomTF!
    @IBOutlet weak var referenceImgTF: UITextField!
    @IBOutlet weak var linkView: UIView!
    @IBOutlet weak var uplaodView: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var referenceImgBtn: UIButton!
    @IBOutlet weak var additionalNoteTF: CustomTextView!
    @IBOutlet weak var referenceImgChangeBtn: CustomButton!
    @IBOutlet weak var productLinkChangeBtn: CustomButton!
    
    var isFrom = ""
    var imageData1 = [Data]()
    let quantityDropDown = DropDown()
    var currentDate = String()
    var imgdata = Data()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        designEx(basicview:linkView)
        designEx(basicview:uplaodView)
//        navTitle(heading: NavTitle.Buy_For_Me)
        referenceImgChangeBtn.setTitle("Upload", for: .normal)
       getCurrentDate()
        self.quantityDropDown.anchorView = self.productQuantityTF
        self.quantityDropDown.bottomOffset = CGPoint(x: 0, y: self.productQuantityTF.bounds.height+15)
        self.quantityDropDown.dataSource = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
        self.quantityDropDown.selectionAction = { [weak self] (index, item) in
            self?.productQuantityTF.text = item
        }

        if isFrom == ""{
            
        }else {
            viewHeight.constant = 0
        }
        // Do any additional setup after loading the view.
    }
    
    func getCurrentDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        currentDate = formatter.string(from: Date())
        print("Current Date : \(currentDate)")

    }

    //MARK:-  IB Actions
    
    @IBAction func nextBtnAction(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
//        self.navigationController?.pushViewController(vc, animated: true)
            
        
            self.productNameTF.text = self.productNameTF.text?.trimmingCharacters(in: .whitespaces)
            self.productDetailsTF.text = self.productDetailsTF.text?.trimmingCharacters(in: .whitespaces)
        self.productDetailsTF.text = self.productDetailsTF.text?.trimmingCharacters(in: .whitespaces)
        self.productLinkTF.text = self.productLinkTF.text?.trimmingCharacters(in: .whitespaces)
        self.productQuantityTF.text = self.productQuantityTF.text?.trimmingCharacters(in: .whitespaces)
        self.referenceImgTF.text = self.referenceImgTF.text?.trimmingCharacters(in: .whitespaces)

            if productNameTF.hasText {
                
                if productDetailsTF.hasText {
                    
                    if productLinkTF.hasText {
                        
                        if productQuantityTF.hasText {
                            if imgdata.count > 0 {
                                
                                /*      NotificationCenter.default.post(name: Notification.Name(rawValue: "BuyForMePageChanged"), object: 1)
                                ISFROMBUYFORME = true
                                BUY_FOR_ME_DATA_DICT = ["product_name":productNameTF.text ?? "","product_details":productDetailsTF.text ?? "","product_link":productLinkTF.text ?? "","product_quantity":productQuantityTF.text ?? "","booking_date":currentDate,"ref_image":imgdata,"additional_note":additionalNoteTF.text ?? ""]
                                */
                                if Reachability.isConnectedToNetwork(){
                                    self.uploadPicDocumentforBuyForMe(data:imgdata)
                                }else{
                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
                                    
                                }

                            } else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.selectReferenceImg)
                            }

                        } else {
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.selectQuantiy)
                        }

                    } else {
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterProductLink)
                    }
                    
                } else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterProductDetails)
                }
                
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterProductName)
            }
        

    }
    
    @IBAction func onClickReferenceImgBtn(_ sender: UIButton) {
        
      /*  ImagePickerManager().pickImage(self) { (img) in
            let Profileimage = img
                        let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
            self.referenceImgTF.text = "\(Date().timeIntervalSince1970).jpeg"
            self.referenceImgChangeBtn.setTitle("Change", for: .normal)
            self.imgdata = imgData
            print(img, "image print")
        }*/
        
    }
    
    //MARK:-  IB Actions
    
    @IBAction func onClickProductQuantityBtn(_ sender: UIButton) {
        
        quantityDropDown.show()
    }
    
    @IBAction func onClickReferenceImgChangeBtn(_ sender: UIButton) {
        
        ImagePickerManager().pickImage(self) { (img) in
            let Profileimage = img
                        let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
            self.referenceImgTF.text = "\(Date().timeIntervalSince1970).jpeg"
            self.referenceImgChangeBtn.setTitle("Change", for: .normal)
            self.imgdata = imgData
            print(img, "image print")
        }
        
    }
    
    @IBAction func onClickProductLinkChangeBtn(_ sender: UIButton) {
        
        productLinkTF.text = ""
    }
    
    @IBAction func onClickRequestOrderBtn(_ sender: UIButton) {
        
//        if Reachability.isConnectedToNetwork(){
//            self.uploadPicDocumentforUSer(data:imgdata)
//        }else{
//            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
//
//        }

    }
    
}
/*
//MARK:- Image Picker
extension BuyForMeProductsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    //Show alert to selected the media source type.
    private func showAlert() {
        
        let alert = UIAlertController(title: "Alert", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //get image from source type
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true) { [weak self] in
            
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            //Setting image to your image view
            self?.photoImageBtn.setImage(image, for: .normal)
            let Profileimage = image
            let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
            self?.imageData.append(imgData)
            if Reachability.isConnectedToNetwork(){
                self?.uploadPicDocumentforUSer(data:imgData)
            }else{
                self?.showAlert(title: Network.title, message: Network.message)

            }
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
*/
extension BuyForMeProductsViewController {
    
    func uploadPicDocumentforBuyForMe(data:Data)  {
        
        self.imageData1.append(data)
        let url = AppUrls.BuyForMeUrl
        let parameters: [String:Any] = [PARAMS.uid:AppHelper.getUserId(),
                                        PARAMS.product_name:productNameTF.text ?? "",
                                        PARAMS.product_discription:productDetailsTF.text ?? "",
                                        PARAMS.product_link:productLinkTF.text ?? "",
                                        PARAMS.product_quantity:productQuantityTF.text ?? "",
                                        PARAMS.additional_note:additionalNoteTF.text ?? "",
                                        PARAMS.address_id:"",
                                        PARAMS.booking_date:currentDate
        ]
        let headers: HTTPHeaders = [
            HEADER.API_KEY:HEADER.API_KEY_VALUE
        ]
        UploadDocumentImageForBuyingProduct(isUser: true, endUrl: url, imageData: imageData1, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
        print("url",url)
        print("image",imageData1)
        print(parameters,"parameters")
        
    }
    func UploadDocumentImageForBuyingProduct(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        if isLoader {
            Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
        }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            //               if let data = [imageData]{
            //                   multipartFormData.append(data, withName: "images[]", fileName: "\(Date.init().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            //                   // multipartFormData.append(data, withName: "profilepic[]", fileName: "imageNew.jpeg", mimeType: "image/jpeg")
            //               }
            for imagesData in self.imageData1 {
                multipartFormData.append(imagesData, withName: "referance_images", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
        },
        to: endUrl, method: .post,headers: headers )
        .responseJSON(completionHandler: { (response) in
            Indicator.shared().hideIndicator(vc: vc)
            if let err = response.error{
                Indicator.shared().hideIndicator(vc: vc)
                print(err)
                onError?(err)
                return
            }
            let json = response.data
            if (json != nil)
            {
                let jsonObject = JSON(json!)
                print(jsonObject)
                if jsonObject["status"].intValue == 200 {
                    
                    DispatchQueue.main.async {
                        showAlertWithOkAction(self, title: ALERT_TITLE, message: jsonObject["message"].stringValue, buttonTitle: "OK") {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                            vc.bookingId = jsonObject["data"].stringValue
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                        
                    }
                }else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                    
                }

            }
        })
    }
}
