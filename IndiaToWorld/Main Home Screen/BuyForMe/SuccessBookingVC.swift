//
//  SuccessBookingVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 07/01/21.
//

import UIKit

class SuccessBookingVC: UIViewController {

    @IBOutlet weak var CheckPackageBtnAction: UIButton!
    @IBOutlet weak var bookingIdlbl: UILabel!
    @IBOutlet weak var successDeslbl: UILabel!
  
    var bookingId = String()
    var isFromOrderPayment = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: NavTitle.Success)
        
        let bookingStr = bookingId.components(separatedBy: "Your order id is ")
        
//        if bookingStr.count > 2 {
        bookingIdlbl.text = bookingId//String(format: "Booking ID : %@", "I2W4532")
//        }
    }
    
    
 //MARK:_Actions
    
    @IBAction func NextBtnAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.TabBarController) as! TabBarController
        if isFromOrderPayment == "Order payment" {
            
            isSelectedMenu = 4

        }else if isFromOrderPayment == "Intransit"{
            
            isSelectedMenu = 5

        }else {
            
            isSelectedMenu = 0

        }
        navigationController?.pushViewController(vc, animated: true)
    }
}
