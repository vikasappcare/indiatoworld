//
//  BuyForMeVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

//protocol SegmentDelegate: class {
//    func SegmentChange(index:Int)
//}

class BuyForMeVC: UIViewController,SegmentDelegate {
    
    @IBOutlet weak var BasicView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        ISFROMBUYFORME = false
        NotificationCenter.default.addObserver(self, selector: #selector(BookNewpackagesVC.currentPageChanged(notification:)), name: Notification.Name(rawValue: "BuyForMePageChanged"), object: nil)

//        navTitle(heading: NavTitle.Buy_For_Me)
        ViewEmbedder.embed(withIdentifier: Identifiers.BuyForMeProductsViewController,parent: self,container: self.BasicView){ vc in
            // do things when embed complete
            vc.isEditing = true
           // ReadyToBuyVC
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func currentPageChanged(notification: Notification) {

        SegmentChange(index: 1)
    }

    func SegmentChange(index:Int){
        print("ok")
        segmentControl.selectedSegmentIndex = index
        if segmentControl.selectedSegmentIndex == 0 {
            print("Select 0")
            ViewEmbedder.embed(withIdentifier: Identifiers.BuyForMeProductsViewController,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
            
        }else{
            print("Select 1")
            ViewEmbedder.embed(withIdentifier: Identifiers.SelectedAddressViewController,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
            //            segmentControl.setEnabled(true, forSegmentAt: 1)
            //            segmentControl.setEnabled(false, forSegmentAt: 0)
        }
        
    }
    
    //MARK:-  IB Actions
    
    @IBAction func segmentControlAction(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0 {
            ViewEmbedder.embed(withIdentifier: Identifiers.BuyForMeProductsViewController,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
        }else{
            ViewEmbedder.embed(withIdentifier: Identifiers.SelectedAddressViewController,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
        }
    }
    

}
