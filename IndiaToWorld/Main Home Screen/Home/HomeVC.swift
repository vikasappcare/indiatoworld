//
//  HomeVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

//
//  HomeVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
class HomeVC: UIViewController {
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var listCollectionVw: UICollectionView!
    @IBOutlet weak var BasicView: UIView!
    @IBOutlet weak var notficitionsBtn: UIButton!
  
    @IBOutlet weak var userNameLbl: UILabel!
    
    
    var nameArr = ["All","Buy for me","Incoming","In Review","Ready to send","Order Payment","In Transit"]
    var SlotsSelectedCell = "All"

    //MARK:-  View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
//        isSelectedMenu = 0 
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        listCollectionVw.scrollToItem(at:IndexPath(row: isSelectedMenu, section: 0), at: .centeredHorizontally, animated: true)

        listCollectionVw.delegate = self
        listCollectionVw.dataSource = self
        listCollectionVw.reloadData()
        guard let name = UserDefaults.standard.value(forKey: FULL_NAME) else {return}
        userNameLbl.text = String(format: "Hello,%@", name as? String ?? "")
        embed(Identifier: Identifiers.AllPackagesVC, Basicview: self.BasicView)
        passingVC()
        
        checkSubscriptionPlanValidationApi()
    }
    //MARK:-  IB Actions

    @IBAction func onTapNotifications(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.NotificationsViewController) as! NotificationsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:-  UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = listCollectionVw.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
        cell.namelbl.backgroundColor = UIColor.white
        cell.namelbl.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.1647058824, blue: 0.5764705882, alpha: 1)
        cell.namelbl.layer.borderWidth = 0.8
        cell.namelbl.padding = UIEdgeInsets(top: 8, left: 15, bottom: 8, right: 15)
        cell.namelbl.text = nameArr[indexPath.row]
        cell.namelbl.sizeToFit()
        cell.namelbl.layer.cornerRadius = cell.namelbl.frame.height/2
        cell.namelbl.layer.masksToBounds = true
        cell.isSelected = false
        if indexPath.item == isSelectedMenu {
            
            cell.namelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.namelbl.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.1647058824, blue: 0.5764705882, alpha: 1)
        }

//        if SlotsSelectedCell == nameArr[indexPath.row]
//        {
//            cell.namelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            cell.namelbl.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
//        }
//        else
//        {
//            cell.namelbl.textColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
//            cell.namelbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        }
//
        return cell
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //      //  let width: CGFloat = self.listCollectionVw.frame.size.width
    //
    //             let label = UILabel(frame: CGRect.zero)
    //               label.text = nameArr[indexPath.item]
    //               label.sizeToFit()
    //               return CGSize(width: label.frame.size.width, height: 80)
    //
    //    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.SlotsSelectedCell = nameArr[indexPath.row]
//        listCollectionVw.reloadData()
        
        isSelectedMenu = indexPath.row
        
        let selectedCell = collectionView.cellForItem(at: indexPath)
        selectedCell?.isSelected = true
        if indexPath.row == 0 {
            embed(Identifier: Identifiers.AllPackagesVC, Basicview: self.BasicView)
        }
        if indexPath.row == 1{
            embed(Identifier: Identifiers.ReadyToBuyVC, Basicview: self.BasicView)
        }
        if indexPath.row == 2{
            embed(Identifier: Identifiers.IncomingViewController, Basicview: self.BasicView)
        }
        if indexPath.row == 3{
            embed(Identifier: Identifiers.InReviewViewController, Basicview: self.BasicView)
        }
        if indexPath.row == 4{
            embed(Identifier: Identifiers.ReadyToSendVC, Basicview: self.BasicView)
        }
        
        if indexPath.row == 5{
            embed(Identifier: Identifiers.OrderPaymentViewController, Basicview: self.BasicView)
        }
        
        if indexPath.row == 6{
            embed(Identifier: Identifiers.INTransitViewController, Basicview: self.BasicView)
        }
        collectionView.scrollToItem(at:indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
}
extension HomeVC {
    
    func checkSubscriptionPlanValidationApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.GetSubscriptionPlanValidationUrl)")

            ApiHandler.getJsonData(url: AppUrls.GetSubscriptionPlanValidationUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }else {
                        
                        self.hideActivityIndicator()
//                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
