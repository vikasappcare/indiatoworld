//
//  CheckSubscriptionPlan.swift
//  IndiaToWorld
//
//  Created by Bhasker on 12/02/21.
//

import Foundation
// MARK: - Welcome
struct CheckSubscriptionPlanModel: Codable {
    let status: Int
    let message: String
    let data: [CheckSubscriptionPlanArr]
}

// MARK: - Datum
struct CheckSubscriptionPlanArr: Codable {
    let id, uid, planID, subPlanID: String
    let status, cd: String

    enum CodingKeys: String, CodingKey {
        case id, uid
        case planID = "plan_id"
        case subPlanID = "sub_plan_id"
        case status, cd
    }
}
