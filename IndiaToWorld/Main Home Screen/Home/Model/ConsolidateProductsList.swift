//
//  ConsolidateProductsList.swift
//  IndiaToWorld
//
//  Created by Bhasker on 09/02/21.
//

import Foundation
// MARK: - Welcome
struct ConsolidateProductsListModel: Codable {
    let status: Int
    let message: String
    let data: [ConsolidateProductsListArr]
}

// MARK: - Datum
struct ConsolidateProductsListArr: Codable {
    let id, type: String?
    let ecomercePlotform, productType, productDelivery: String?
    let productName, productDiscription, productLink, productQuantity: String?
    let orderTypeID, additionalNote, referanceImages: String?
    let expectedDate: String?
    let bookingDate: String?
    let invoice: String?
    let status: String?
    let bookingid: String?
    let orderID, uid, cd, addressID: String?
    let addressType, address, state, ltd: String?
    let lng, country: String?

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case uid, cd
        case addressID = "address_id"
        case addressType = "address_type"
        case address, state, ltd, lng, country
    }
}
