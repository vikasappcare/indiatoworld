//
//  OrderPaymentDetails.swift
//  IndiaToWorld
//
//  Created by Bhasker on 19/05/21.
//

import Foundation
/*// MARK: - Welcome
struct OrderPaymentDetailsModel: Codable {
    let status: Int
    let message: String
    let data: OrderPaymentDetailsDict
}

// MARK: - DataClass
struct OrderPaymentDetailsDict: Codable {
    let packageDetails: [PackageDetail]
    let planName, packageTitle, packagePrice, shippingTitle: String
    let shippingPrice: String
    let grandTotal: Int

    enum CodingKeys: String, CodingKey {
        case packageDetails = "package_details"
        case planName = "plan_name"
        case packageTitle = "package_title"
        case packagePrice = "package_price"
        case shippingTitle = "shipping_title"
        case shippingPrice = "shipping_price"
        case grandTotal = "grand_total"
    }
}

// MARK: - PackageDetail
struct PackageDetail: Codable {
    let id, type, ecomercePlotform, productType: String
    let productDelivery, productName, productDiscription, productLink: String
    let productQuantity, orderTypeID, additionalNote: String
    let referanceImages: String?
    let expectedDate: String
    let bookingDate: String?
    let invoice, status, bookingid, orderID: String
    let packagingSize, packagingWeight, packagingWeightCost, shortStorageDays: String
    let uid, cd, addressID, paymentDetailsStatus: String
    let paymentDetailsDescription, productID, addressType, address: String
    let state, country, ltd, lng: String
    let adsID: String

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case packagingSize = "packaging_size"
        case packagingWeight = "packaging_weight"
        case packagingWeightCost = "packaging_weight_cost"
        case shortStorageDays = "short_storage_days"
        case uid, cd
        case addressID = "address_id"
        case paymentDetailsStatus = "payment_details_status"
        case paymentDetailsDescription = "payment_details_description"
        case productID = "product_id"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
    }
}
*/

// MARK: - Welcome
struct PackageOrderPaymentDetailsModel: Codable {
    let status: Int
    let message: String
    let data: PackageOrderPaymentDetailsDict
}

// MARK: - DataClass
struct PackageOrderPaymentDetailsDict: Codable {
    let packageDetails: PackageDetails
    let planName, packageTitle, packagePrice, shippingTitle: String
    let shippingPrice: String

    enum CodingKeys: String, CodingKey {
        case packageDetails = "package_details"
        case planName = "plan_name"
        case packageTitle = "package_title"
        case packagePrice = "package_price"
        case shippingTitle = "shipping_title"
        case shippingPrice = "shipping_price"
    }
}

// MARK: - PackageDetails
struct PackageDetails: Codable {
    let uid, packageOrderID, packageID, packageTitle: String
    let totalItems, addressType, address, state: String
    let ltd, lng, country: String
    let orders: [PackageDetailsOrders]?
    let grandTotal: Int

    enum CodingKeys: String, CodingKey {
        case uid
        case packageOrderID = "package_order_id"
        case packageID = "package_id"
        case packageTitle = "package_title"
        case totalItems = "total_items"
        case addressType = "address_type"
        case address, state, ltd, lng, country, orders
        case grandTotal = "grand_total"
    }
}

// MARK: - Order
struct PackageDetailsOrders: Codable {
    let id, productName, productType: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case productType = "product_type"
    }
}

// MARK: - Welcome
struct SingleOrderPaymentDetailsModel: Codable {
    let status: Int
    let message: String
    let data: SingleOrderPaymentDetailsDict
}

// MARK: - DataClass
struct SingleOrderPaymentDetailsDict: Codable {
    let packageDetails: [SingleOrderPackageDetail]
    let planName, packageTitle, packagePrice, shippingTitle: String
    let shippingPrice: String
    let grandTotal: Int

    enum CodingKeys: String, CodingKey {
        case packageDetails = "package_details"
        case planName = "plan_name"
        case packageTitle = "package_title"
        case packagePrice = "package_price"
        case shippingTitle = "shipping_title"
        case shippingPrice = "shipping_price"
        case grandTotal = "grand_total"
    }
}

// MARK: - PackageDetail
struct SingleOrderPackageDetail: Codable {
    let id, type: String?
    let ecomercePlotform, productType, productDelivery: String?
    let productName, productDiscription, productLink, productQuantity: String
    let orderTypeID: String?
    let additionalNote: String
    let expectedDate,referanceImages: String?
    let bookingDate: String
    let invoice: String?
    let status,uid, cd: String?
    let bookingid: String?
    let orderID, packagingSize, packagingWeight, packagingWeightCost: String
    let shortStorageDays, shortStorageCost: String
    let addressID, paymentDetailsStatus, paymentDetailsDescription, productID: String
    let addressType, address, state, country: String?
    let ltd, lng, adsID: String?

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case packagingSize = "packaging_size"
        case packagingWeight = "packaging_weight"
        case packagingWeightCost = "packaging_weight_cost"
        case shortStorageDays = "short_storage_days"
        case shortStorageCost = "short_storage_cost"
        case uid, cd
        case addressID = "address_id"
        case paymentDetailsStatus = "payment_details_status"
        case paymentDetailsDescription = "payment_details_description"
        case productID = "product_id"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
    }
}

