//
//  ChatData.swift
//  IndiaToWorld
//
//  Created by Bhasker on 22/04/21.
//

import Foundation
// MARK: - Welcome
struct AdminDataModel: Codable {
    let status: Int
    let message, data: String
}
