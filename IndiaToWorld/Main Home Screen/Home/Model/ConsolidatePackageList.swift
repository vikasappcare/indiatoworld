//
//  ConsolidatePackageList.swift
//  IndiaToWorld
//
//  Created by Bhasker on 10/02/21.
//
/*
import Foundation
// MARK: - Welcome
struct ConsolidatePackageListModel: Codable {
    let status: Int
    let message: String
    let data: [ConsolidatePackageListArr]
}

// MARK: - Datum
struct ConsolidatePackageListArr: Codable {
    let uid, packageOrderID, packageID,packageTitle, totalItems: String
    let orders: [Order]

    enum CodingKeys: String, CodingKey {
        case uid
        case packageOrderID = "package_order_id"
        case packageID = "package_id"
        case totalItems = "total_items"
        case orders
        case packageTitle = "package_title"
    }
}

// MARK: - Order
struct Order: Codable {
    let id, productName, productType: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case productType = "product_type"
    }
}*/

/* // MARK: - Welcome
 struct ConsolidatePackageListModel: Codable {
     let status: Int
     let message: String
     let data: [ConsolidatePackageListArr]
 }

 // MARK: - Datum
 struct ConsolidatePackageListArr: Codable {
     let uid, packageOrderID, packageID, packageTitle: String
     let totalItems, addressType, address, state: String
     let ltd, lng, country: String
     let orders: [Order]

     enum CodingKeys: String, CodingKey {
         case uid
         case packageOrderID = "package_order_id"
         case packageID = "package_id"
         case packageTitle = "package_title"
         case totalItems = "total_items"
         case addressType = "address_type"
         case address, state, ltd, lng, country, orders
     }
 }

 // MARK: - Order
 struct Order: Codable {
     let id, productName, productType: String

     enum CodingKeys: String, CodingKey {
         case id
         case productName = "product_name"
         case productType = "product_type"
     }
 }
 
*/
// MARK: - Welcome
/*struct ConsolidatePackageListModel: Codable {
    let status: Int
    let message: String
    let data: [ConsolidatePackageListArr]
}

// MARK: - Datum
struct ConsolidatePackageListArr: Codable {
    let uid, packageOrderID, packageID, packageTitle: String
    let totalItems: String
    let addressType, address, state, ltd: String?
    let lng, country: String?
    let orders: [Order]

    enum CodingKeys: String, CodingKey {
        case uid
        case packageOrderID = "package_order_id"
        case packageID = "package_id"
        case packageTitle = "package_title"
        case totalItems = "total_items"
        case addressType = "address_type"
        case address, state, ltd, lng, country, orders
    }
}

// MARK: - Order
struct Order: Codable {
    let id, productName, productType: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case productType = "product_type"
    }
}
*/

// MARK: - Welcome
struct ConsolidatePackageListModel: Codable {
    let status: Int
    let message: String
    let data: [ConsolidatePackageListArr]
}

// MARK: - Datum
struct ConsolidatePackageListArr: Codable {
    let uid, packageOrderID, packageID, packageTitle: String
    let totalItems: String
    let addressType, address, state, ltd: String?
    let lng, country: String?
    let orders: [Order]

    enum CodingKeys: String, CodingKey {
        case uid
        case packageOrderID = "package_order_id"
        case packageID = "package_id"
        case packageTitle = "package_title"
        case totalItems = "total_items"
        case addressType = "address_type"
        case address, state, ltd, lng, country, orders
    }
}

// MARK: - Order
struct Order: Codable {
    let id, productName, productType, packageID: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case productType = "product_type"
        case packageID = "package_id"
    }
}
