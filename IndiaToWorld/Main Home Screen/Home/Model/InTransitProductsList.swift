//
//  InTransitProductsList.swift
//  IndiaToWorld
//
//  Created by Bhasker on 17/02/21.
//

import Foundation
// MARK: - Welcome
struct InTransitProductsListModel: Codable {
    let status: Int
    let message: String
    let data: [InTransitProductsListArr]
}

// MARK: - Datum
struct InTransitProductsListArr: Codable {
    let id, type, ecomercePlotform, productType: String?
    let productDelivery, productName, productDiscription, productLink: String?
    let productQuantity, orderTypeID, additionalNote, referanceImages: String?
    let expectedDate, bookingDate: String?
    let invoice: String?
    let status, bookingid, orderID: String?
    let uid: String
    let cd, addressID, productID: String?
    let addressType, address, state, country: String?
    let ltd, lng: String
    let adsID, packageOrderID, packageID, packageTitle: String?
    let totalItems: String?
    let orders: [InTransitOrders]?

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case uid, cd
        case addressID = "address_id"
        case productID = "product_id"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
        case packageOrderID = "package_order_id"
        case packageID = "package_id"
        case packageTitle = "package_title"
        case totalItems = "total_items"
        case orders
    }
}

// MARK: - Order
struct InTransitOrders: Codable {
    let id, productName, productType: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case productType = "product_type"
    }
}

// MARK: - Welcome
struct chatConversion: Codable {
    let status: Bool?
    let message: String?
    let data: [ChatData]?
}

// MARK: - Datum
struct ChatData: Codable {
    let id, fromID, toID, datumDescription: String?
    let createAt, image, readCount, chat: String?
    let nCount, userID: String?
    let profilePic: String?
    let fullname: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case fromID = "from_id"
        case toID = "to_id"
        case datumDescription = "description"
        case createAt = "create_at"
        case image
        case readCount = "read_count"
        case chat
        case nCount = "n_count"
        case userID = "user_id"
        case profilePic = "profile_pic"
        case fullname
    }
}
