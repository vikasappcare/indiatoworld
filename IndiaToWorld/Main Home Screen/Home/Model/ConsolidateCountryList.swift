//
//  ConsolidateCountryList.swift
//  IndiaToWorld
//
//  Created by Bhasker on 08/02/21.
//

import Foundation
// MARK: - Welcome
struct ConsolidateCountryListModel: Codable {
    let status: Int
    let message: String
    let data: [ConsolidateCountryListArr]
}

// MARK: - Datum
struct ConsolidateCountryListArr: Codable {
    let countryID, name, cd: String

    enum CodingKeys: String, CodingKey {
        case countryID = "country_id"
        case name, cd
    }
}
