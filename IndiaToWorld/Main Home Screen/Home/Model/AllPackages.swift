//
//  AllPackages.swift
//  IndiaToWorld
//
//  Created by Bhasker on 21/01/21.
//

import Foundation
struct AllPackagesModel: Codable {
    let status: Int
    let message: String
    let data: [AllPackagesArr]
}

// MARK: - Datum
struct AllPackagesArr: Codable {
    let id, type, ecomercePlotform, productType: String
    let productDelivery, productName, productDiscription, productLink: String
    let productQuantity, additionalNote: String
    let referanceImages: String
    let expectedDate, bookingDate: String
    let invoice: String
    let status, bookingid, orderID, uid: String
    let cd, addressID, productID, addressType: String
    let address, state, country, ltd: String
    let lng, adsID: String

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case uid, cd
        case addressID = "address_id"
        case productID = "product_id"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
    }
}
