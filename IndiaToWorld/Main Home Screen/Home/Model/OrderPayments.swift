//
//  OrderPayments.swift
//  IndiaToWorld
//
//  Created by Bhasker on 19/05/21.
//

import Foundation
/*// MARK: - Welcome
struct OrderPaymentsModel: Codable {
    let status: Int
    let message: String
    let data: [OrderPaymentsArr]
}

// MARK: - Datum
struct OrderPaymentsArr: Codable {
    let id, type, ecomercePlotform, productType: String
    let productDelivery, productName, productDiscription, productLink: String
    let productQuantity: String
    let orderTypeID: String?
    let additionalNote, referanceImages, expectedDate, bookingDate: String
    let invoice: String
    let status, bookingid, orderID, packagingSize: String
    let packagingWeight, packagingWeightCost, shortStorageDays, uid: String
    let cd, addressID, productID, ordDate: String
    let addressType, address, state, country: String
    let ltd, lng, adsID: String

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case packagingSize = "packaging_size"
        case packagingWeight = "packaging_weight"
        case packagingWeightCost = "packaging_weight_cost"
        case shortStorageDays = "short_storage_days"
        case uid, cd
        case addressID = "address_id"
        case productID = "product_id"
        case ordDate = "ord_date"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
    }
}
*/

/* // MARK: - Welcome
 struct OrderPaymentsModel: Codable {
     let status: Int
     let message: String
     let data: [OrderPaymentsArr]
 }

 // MARK: - Datum
 struct OrderPaymentsArr: Codable {
     let id, type, ecomercePlotform, productType: String?
     let productDelivery, productName, productDiscription, productLink: String?
     let productQuantity, orderTypeID, additionalNote, referanceImages: String?
     let expectedDate, bookingDate: String?
     let invoice: String?
     let status, bookingid, orderID, packagingSize: String?
     let packagingWeight, packagingWeightCost, shortStorageDays: String?
     let uid: String
     let cd, addressID, paymentDetailsStatus, paymentDetailsDescription: String?
     let productID, ordDate: String?
     let addressType, address, state, country: String?
     let ltd, lng: String?
     let adsID, packageOrderID, packageID, packageTitle: String?
     let totalItems: String?
     let orders: [PaymentsOrder]?

     enum CodingKeys: String, CodingKey {
         case id, type
         case ecomercePlotform = "ecomerce_plotform"
         case productType = "product_type"
         case productDelivery = "product_delivery"
         case productName = "product_name"
         case productDiscription = "product_discription"
         case productLink = "product_link"
         case productQuantity = "product_quantity"
         case orderTypeID = "order_type_id"
         case additionalNote = "additional_note"
         case referanceImages = "referance_images"
         case expectedDate = "expected_date"
         case bookingDate = "booking_date"
         case invoice, status, bookingid
         case orderID = "order_id"
         case packagingSize = "packaging_size"
         case packagingWeight = "packaging_weight"
         case packagingWeightCost = "packaging_weight_cost"
         case shortStorageDays = "short_storage_days"
         case uid, cd
         case addressID = "address_id"
         case paymentDetailsStatus = "payment_details_status"
         case paymentDetailsDescription = "payment_details_description"
         case productID = "product_id"
         case ordDate = "ord_date"
         case addressType = "address_type"
         case address, state, country, ltd, lng
         case adsID = "ads_id"
         case packageOrderID = "package_order_id"
         case packageID = "package_id"
         case packageTitle = "package_title"
         case totalItems = "total_items"
         case orders
     }
 }

 // MARK: - Order
 struct PaymentsOrder: Codable {
     let id, productName, productType, paymentDetailsDescription: String
     let paymentDetailsStatus: String

     enum CodingKeys: String, CodingKey {
         case id
         case productName = "product_name"
         case productType = "product_type"
         case paymentDetailsDescription = "payment_details_description"
         case paymentDetailsStatus = "payment_details_status"
     }
 }
*/


// MARK: - Welcome
struct OrderPaymentsModel: Codable {
    let status: Int
    let message: String
    let data: [OrderPaymentsArr]
}

// MARK: - Datum
struct OrderPaymentsArr: Codable {
    let id, type, ecomercePlotform, productType: String?
    let productDelivery, productName, productDiscription, productLink: String?
    let productQuantity: String?
    let orderTypeID: String?
    let additionalNote: String?
    let referanceImages: String?
    let expectedDate, bookingDate, invoice, status: String?
    let bookingid, orderID, packagingSize, packagingWeight: String?
    let packagingWeightCost, shortStorageDays, shortStorageCost: String?
    let uid: String
    let cd, addressID: String?
    let paymentDetailsStatus, paymentDetailsDescription: String
    let productID, ordDate: String?
    let addressType, address, state, country: String
    let ltd, lng: String
    let adsID, packageOrderID, packageID, packageTitle: String?
    let totalItems: String?
    let orders: [PaymentsOrder]?

    enum CodingKeys: String, CodingKey {
        case id, type
        case ecomercePlotform = "ecomerce_plotform"
        case productType = "product_type"
        case productDelivery = "product_delivery"
        case productName = "product_name"
        case productDiscription = "product_discription"
        case productLink = "product_link"
        case productQuantity = "product_quantity"
        case orderTypeID = "order_type_id"
        case additionalNote = "additional_note"
        case referanceImages = "referance_images"
        case expectedDate = "expected_date"
        case bookingDate = "booking_date"
        case invoice, status, bookingid
        case orderID = "order_id"
        case packagingSize = "packaging_size"
        case packagingWeight = "packaging_weight"
        case packagingWeightCost = "packaging_weight_cost"
        case shortStorageDays = "short_storage_days"
        case shortStorageCost = "short_storage_cost"
        case uid, cd
        case addressID = "address_id"
        case paymentDetailsStatus = "payment_details_status"
        case paymentDetailsDescription = "payment_details_description"
        case productID = "product_id"
        case ordDate = "ord_date"
        case addressType = "address_type"
        case address, state, country, ltd, lng
        case adsID = "ads_id"
        case packageOrderID = "package_order_id"
        case packageID = "package_id"
        case packageTitle = "package_title"
        case totalItems = "total_items"
        case orders
    }
}

// MARK: - Orders
struct PaymentsOrder: Codable {
    let id, productName, productType, paymentDetailsDescription: String
    let paymentDetailsStatus: String?

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "product_name"
        case productType = "product_type"
        case paymentDetailsDescription = "payment_details_description"
        case paymentDetailsStatus = "payment_details_status"
    }
}
enum PaymentDetailsDescription: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(PaymentDetailsDescription.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for PaymentDetailsDescription"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}
