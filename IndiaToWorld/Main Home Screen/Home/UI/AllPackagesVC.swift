//
//  AllPackagesVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 30/12/20.
//

import UIKit

class AllPackagesVC: UIViewController {

    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var allPackagestTblVw: UITableView!
    
    
    var actionButton : ActionButton!
    var allPackagesListArr = [AllPackagesArr]()
    var refreshControl = UIRefreshControl()
    var adminId = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        allPackagestTblVw.register(AllTableViewCell.nib(), forCellReuseIdentifier: AllTableViewCell.identifier)
        allPackagestTblVw.register(OrderHistoryTableViewCell.nib(), forCellReuseIdentifier: OrderHistoryTableViewCell.identifier)
        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.allPackagestTblVw.addSubview(self.refreshControl)
            self.allPackagestTblVw.refreshControl = self.refreshControl
        }
        allPackagestTblVw.estimatedRowHeight = allPackagestTblVw.rowHeight
        allPackagestTblVw.rowHeight = UITableView.automaticDimension
        allPackagestTblVw.separatorStyle = .none
//        headinglbl.highlight(searchedText: "incoming packages")
        floatButton()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        checkSubscriptionPlanValidationApi()
        getAllPackagesListApi()
        getAdminIdApi()
//        tableview.delegate = self
//        tableview.dataSource = self
//        tableview.separatorStyle = .none
//        tableview.reloadData()

    }

    
    @objc func refresh(sender:AnyObject){
        
        getAllPackagesListApi()
        allPackagestTblVw.reloadData()
        self.refreshControl.endRefreshing()
    }

    func floatButton(){
        let one = ActionButtonItem(title: "Book A New Package", image: #imageLiteral(resourceName: "FEb"))
        one.action = { item in
            
            self.floatBtnAction() }
        let two = ActionButtonItem(title: "Buy For Me", image: #imageLiteral(resourceName: "FEb"))
        two.action = { item in
            
            self.floatBtnAction1() }
        actionButton = ActionButton(attachedToView: self.view, items: [one,two,])
        actionButton.setTitle("+", forState: UIControl.State())
      //  actionButton.backgroundColor = #colorLiteral(red: 0.08235294118, green: 0.5725490196, blue: 0.9019607843, alpha: 1)
        actionButton.backgroundColor = UIColor.red
        actionButton.action = { button in button.toggleMenu()}
    }
    
    @objc func floatBtnAction()
       {
          print("1")
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
       
       }
    @objc func floatBtnAction1()
       {
          print("2")
        self.tabBarController?.selectedIndex = 2
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookNewpackagesVC) as! BookNewpackagesVC
//        navigationController?.pushViewController(vc, animated: true)
       
       }

    //MARK:-  IB Actions

    @IBAction func bookNewPackagesAction(_ sender: Any) {
       // BookNewpackagesVC
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buyFormeAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BuyForMeProductsViewController) as!  BuyForMeProductsViewController
//        vc.isFrom = "Buy"
//        navigationController?.pushViewController(vc, animated: true)
                        self.tabBarController?.selectedIndex = 2

    }
        
    @IBAction func onClickPlusBtn(_ sender: Any) {
        
        self.floatBtnAction()

    }
    
    @objc func onClickViewMoreBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.ViewMoreVC) as! ViewMoreVC
            vc.productId = self.allPackagesListArr[sender.tag].productID
            vc.productDetailsDict = self.allPackagesListArr[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }

    @objc func onChatNowBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.UserChatViewController) as! UserChatViewController
            vc.toId = self.adminId
            vc.fromId = AppHelper.getUserId()
            
            if self.allPackagesListArr[sender.tag].orderID == "" {
                vc.orderID = self.allPackagesListArr[sender.tag].bookingid
            }else {
                vc.orderID = self.allPackagesListArr[sender.tag].orderID
            }
            self.navigationController?.pushViewController(vc, animated: true)

        }
//        showAlertMessage(vc: self, titleStr: "Inprogress", messageStr: "Chat implementation is in inprogress")
    }
}
//MARK:- Extensions
extension AllPackagesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPackagesListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.allPackagesListArr[indexPath.row].type == "buyforme" {
            
            guard let cell = allPackagestTblVw.dequeueReusableCell(withIdentifier: AllTableViewCell.identifier, for: indexPath) as? AllTableViewCell else{return UITableViewCell()}
            
            cell.viewMoreBtn.tag = indexPath.row
            cell.chatNowBtn.tag = indexPath.row
            cell.viewMoreBtn.addTarget(self, action: #selector(onClickViewMoreBtn), for: .touchUpInside)
            cell.chatNowBtn.addTarget(self, action: #selector(onChatNowBtn), for: .touchUpInside)

           cell.selectionStyle = .none
            cell.selectionStyle = .none
            if self.allPackagesListArr.count > 0 {
                
                cell.loadPackageList(packageData: self.allPackagesListArr[indexPath.row])
            }
            return cell

        }else {
            guard let cell = allPackagestTblVw.dequeueReusableCell(withIdentifier: OrderHistoryTableViewCell.identifier, for: indexPath) as? OrderHistoryTableViewCell else{return UITableViewCell()}
            if self.allPackagesListArr.count > 0 {
                
                cell.loadPackageList(packageData: self.allPackagesListArr[indexPath.row])
            }
            
            cell.viewMoreBtn.tag = indexPath.row
            cell.viewMoreBtn.addTarget(self, action: #selector(onClickViewMoreBtn), for: .touchUpInside)
           cell.selectionStyle = .none
            cell.selectionStyle = .none

            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}
//MARK:-  API Services

extension AllPackagesVC {
    
    func getAdminIdApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            print("UrlString:  \(AppUrls.GetChatAdminIdUrl)")

            ApiHandler.getData(url: AppUrls.GetChatAdminIdUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(AdminDataModel.self, from: data){
                            if loginData.status == 200{

                                self.adminId = loginData.data
                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    func checkSubscriptionPlanValidationApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.GetSubscriptionPlanValidationUrl)")

            ApiHandler.getJsonData(url: AppUrls.GetSubscriptionPlanValidationUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }else {
                        
                        self.hideActivityIndicator()
//                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    func getAllPackagesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:"all",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AllPackagesListUrl)")

            ApiHandler.getData(url: AppUrls.AllPackagesListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(AllPackagesModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.allPackagesListArr = loginData.data
                                print("Dict Data: \(String(describing: self.allPackagesListArr))")
                                if self.allPackagesListArr.count > 0 {
                                    self.allPackagestTblVw.isHidden = false
                                    DispatchQueue.main.async {
                                        self.allPackagestTblVw.delegate = self
                                        self.allPackagestTblVw.dataSource = self
                                        self.allPackagestTblVw.reloadData()

                                    }
                                }else {
                                    
                                    self.allPackagestTblVw.isHidden = true
                                }

                            }else {
                                self.hideActivityIndicator()
                                self.allPackagestTblVw.isHidden = true

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.allPackagestTblVw.isHidden = true
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
