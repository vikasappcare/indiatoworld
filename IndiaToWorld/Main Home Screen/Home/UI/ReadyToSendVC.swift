//
//  ReadyToSendVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 05/01/21.
//


import UIKit
class ReadyToSendVC: UIViewController {
    
    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var basicTableView: UIView!
    @IBOutlet weak var emptyBasicView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bookNewPackageBtn: UIButton!
    @IBOutlet weak var buyFormeBtn: UIButton!
    
    @IBOutlet weak var proceedBtn: UIButton!
    @IBOutlet weak var consolidateBtn: UIButton!
    
    var selectedPackagesIdsArr = [String]()
    var arr = [String]()
    var actionButton : ActionButton!
    var allPackagesListArr = [AllPackagesArr]()
    var isProceedSelected = Bool()
    var productId = String()
    var refreshControl = UIRefreshControl()
    var selectedCountry = Int()
    var countryId = String()
    var isProductSelected = Bool()
    var orderIdArr = [String]()
    var dummyArr = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.register(ReadyToSendTableViewCell.nib(), forCellReuseIdentifier: ReadyToSendTableViewCell.identifier)
        tableview.estimatedRowHeight = tableview.rowHeight
        tableview.rowHeight = UITableView.automaticDimension
        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.tableview.addSubview(self.refreshControl)
            self.tableview.refreshControl = self.refreshControl
        }
        isProceedSelected = false
        productId = ""
        headinglbl.highlight(searchedText: "consolidate")

//        tableview.delegate = self
//        tableview.dataSource = self
        tableview.separatorStyle = .none
//        tableview.reloadData()
        basicTableView.isHidden = false
        emptyBasicView.isHidden = true
        floatButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        tableview.delegate = self
//        tableview.dataSource = self
        tableview.separatorStyle = .none
//        tableview.reloadData()
        getAllPackagesListApi()
        buttonRequest()
    }
    func buttonRequest(){
         if orderIdArr.count == 0{
            self.consolidateBtn.isHidden = true
            self.proceedBtn.isHidden = true
            
        }else if orderIdArr.count == 1{
            self.proceedBtn.isHidden = false
            self.consolidateBtn.isHidden = false
            self.proceedBtn.isEnabled = true
            self.consolidateBtn.isEnabled = false
            btnColor(color:#colorLiteral(red: 0.2039215686, green: 0.3098039216, blue: 0.6431372549, alpha: 1),Button:proceedBtn)
            btnColor(color:#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1),Button:consolidateBtn)
        }else if orderIdArr.count == 2{
            self.proceedBtn.isEnabled = false
            self.consolidateBtn.isEnabled = true
            self.proceedBtn.isHidden = false
            self.consolidateBtn.isHidden = false
            btnColor(color:#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1),Button:proceedBtn)
            btnColor(color:#colorLiteral(red: 0.2039215686, green: 0.3098039216, blue: 0.6431372549, alpha: 1),Button:consolidateBtn)
        }else if orderIdArr.count >= 3{
            self.proceedBtn.isEnabled = false
            self.consolidateBtn.isEnabled = true
            self.proceedBtn.isHidden = false
            self.consolidateBtn.isHidden = false
            btnColor(color:#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1),Button:proceedBtn)
            btnColor(color:#colorLiteral(red: 0.2039215686, green: 0.3098039216, blue: 0.6431372549, alpha: 1),Button:consolidateBtn)
        }
    }
    func btnColor(color:UIColor,Button:UIButton){
        Button.layer.cornerRadius = 5
        Button.layer.borderWidth = 0.8
        Button.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        Button.backgroundColor = color
        
    }
    func consolidateProcess(){
        let alertController = UIAlertController(
            title: ALERT_TITLE,
            message: "You don't have enough products for consolidate process! Either you can remove already created packages or book more products!!",
            preferredStyle: UIAlertController.Style.alert
        )
        
        let cancelAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            // ...
            
            let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.ConsolidateListProductsVC) as!  ConsolidateListProductsVC
            self.navigationController?.pushViewController(vc, animated: true)

//            self.deleteAddressRequest()
        }
        
        let okayAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // ...
        }
        
        alertController.addAction(okayAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
    }
    @objc func refresh(sender:AnyObject){
        
        getAllPackagesListApi()
        tableview.reloadData()
        self.refreshControl.endRefreshing()
    }

    func floatButton(){
        let one = ActionButtonItem(title: "Book A New Package", image: #imageLiteral(resourceName: "FEb"))
        one.action = { item in
            
            self.floatBtnAction() }
        let two = ActionButtonItem(title: "Buy For Me", image: #imageLiteral(resourceName: "FEb"))
        two.action = { item in
            
            self.floatBtnAction1() }
        actionButton = ActionButton(attachedToView: self.view, items: [one,two,])
        actionButton.setTitle("+", forState: UIControl.State())
       // actionButton.backgroundColor = UIColor.red
        actionButton.backgroundColor = UIColor.red
        
        actionButton.action = { button in button.toggleMenu()}
    }
    @objc func floatBtnAction()
    {
        print("1")
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func floatBtnAction1()
    {
       print("2")
     self.tabBarController?.selectedIndex = 2
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookNewpackagesVC) as! BookNewpackagesVC
//        navigationController?.pushViewController(vc, animated: true)
    
    }
    //MARK:-  IB Actions
    
    @IBAction func bookNewPackagesAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buyFormeAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BuyForMeProductsViewController) as!  BuyForMeProductsViewController
//        vc.isFrom = "Buy"
//        navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.selectedIndex = 2

        
    }
    @IBAction func ProceedButtonAction(_ sender: Any) {
        
//        if isProceedSelected == false {
//            productId = ""
//            isProceedSelected = true
//            tableview.reloadData()

//        }else {
//
//        if productId == "" {
//            if isProceedSelected == true {
//
//                showAlertMessage(vc: self, titleStr: "", messageStr: "Please select one product for proceed")
//
//            }
//
//            isProceedSelected = true
//            tableview.reloadData()
//
//        }else {
//
//            let vc = storyboard?.instantiateViewController(identifier: Identifiers.PackagingAndShippingVC) as!  PackagingAndShippingVC
//            vc.isFromProceed = true
//            vc.bookingPackageId = productId
//            navigationController?.pushViewController(vc, animated: true)
//
//        }
////        let vc = storyboard?.instantiateViewController(identifier: Identifiers.SubscriptionVC) as!  SubscriptionVC
////        navigationController?.pushViewController(vc, animated: true)
//    }
        isProceedSelected = true
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.SelectedAddressViewController) as!  SelectedAddressViewController
        vc.isFromProceed = true
        if orderIdArr.count > 0{
            vc.bookingPackageId = orderIdArr[0]
        }else{
            print("no index")
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func ConsolidateButtonAction(_ sender: Any) {
        
//        if allPackagesListArr.count < 2 {
//
//            consolidateProcess()
//        }else {
//
//            checkSubscriptionPlanApi()
//        }
        
        checkSubscriptionPlanApi()
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        self.floatBtnAction()
        
    }
    

    @objc func onClickViewMoreBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.ViewMoreVC) as! ViewMoreVC
            vc.productId = self.allPackagesListArr[sender.tag].productID
            vc.productDetailsDict = self.allPackagesListArr[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}
//MARK:- Extensions
extension ReadyToSendVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPackagesListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      //  orderIdArr = []
        guard let cell = tableview.dequeueReusableCell(withIdentifier: ReadyToSendTableViewCell.identifier, for: indexPath) as? ReadyToSendTableViewCell else{return UITableViewCell()}
        
//        if isProceedSelected == true {
//            cell.checkBoxImgVw.isHidden = false
//            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
//        }else {
//
//            cell.checkBoxImgVw.isHidden = true
//        }
//
        if allPackagesListArr.count > 0 {
            cell.loadPackageList(packageData: allPackagesListArr[indexPath.row])

        }
        if dummyArr.count > 0 {
            
            if dummyArr[indexPath.row] == 1 {
                cell.checkBoxImgVw.isHidden = false
                cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
//                orderIdArr.append(allPackagesListArr[indexPath.row].productID)
//                print("totalVaules",orderIdArr)
            }else {
                cell.checkBoxImgVw.isHidden = true
                cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
            }

        }
        cell.viewMoreBtn.tag = indexPath.row

        cell.viewMoreBtn.addTarget(self, action: #selector(onClickViewMoreBtn), for: .touchUpInside)
        cell.selectionStyle = .none
        cell.selectionStyle = .none
        buttonRequest()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
////        let cell = tableview.cellForRow(at: indexPath) as! ReadyToSendTableViewCell
////       // cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
////        if cell.checkBoxImgVw.image == #imageLiteral(resourceName: "SelcetedIcon"){
////            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
////        }else{
////            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
////        }
//        productId = allPackagesListArr[indexPath.row].productID
//        print(productId)
//        print("ok",selectedPackagesIdsArr.append("0"))
//        print("arr",arr.append(productId))
//    }
////    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
////
//////        let cell = tableview.cellForRow(at: indexPath) as! ReadyToSendTableViewCell
//////        if cell.checkBoxImgVw.image == #imageLiteral(resourceName: "SelcetedIcon"){
//////            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
//////        }else{
//////            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
//////        }
////        print(productId)
////             print("ok",selectedPackagesIdsArr.append("0"))
////            print("arr",arr.append(productId))
////       // productId = ""
////    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let cell = tableview.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! ReadyToSendTableViewCell
//
//        cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
//        orderIdArr.append(allPackagesListArr[indexPath.row].productID)
//        dummyArr[indexPath.row] = 1
//        print("Order Id : \(orderIdArr)")
//        if orderIdArr.count == 1 {
//
//            orderIdArr.removeAll()
//
//        }else {
//
//            for i in 0..<orderIdArr.count {
//
//                let catId = orderIdArr[i]
//
//              if catId == allPackagesListArr[indexPath.row].productID {
//
//                orderIdArr.remove(at: i)
//                print(orderIdArr)
//                dummyArr[indexPath.row] = 0
//                   // categoryCell.categoriesCellBgVw.backgroundColor = .clear
//                break
//
//
//                }else{
//
//                  //  cell.categoriesCellBgVw.backgroundColor = .selectionLightBlue
//
//                    //categoryIdArr.append(categoryIdStr)
//                }
//
//            }
//        }
        if  dummyArr[indexPath.row] == 0{
            dummyArr[indexPath.row] = 1
           orderIdArr.append(allPackagesListArr[indexPath.row].productID)
        }else if dummyArr[indexPath.row] == 1{
           // dummyArr[indexPath.row] = 0
           // orderIdArr.remove(at:indexPath.row)
            
            for i in 0..<orderIdArr.count {
           
                           let catId = orderIdArr[i]
           
                         if catId == allPackagesListArr[indexPath.row].productID {
           
                           orderIdArr.remove(at: i)
                           print(orderIdArr)
                           dummyArr[indexPath.row] = 0
                              // categoryCell.categoriesCellBgVw.backgroundColor = .clear
                           break
           
           
                           }else{
           
                             //  cell.categoriesCellBgVw.backgroundColor = .selectionLightBlue
           
                               //categoryIdArr.append(categoryIdStr)
                           }
           
                       }
        }
        
        
        print("Order IdTotal : \(orderIdArr)")
        tableview.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//
//        let cell = tableview.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! ReadyToSendTableViewCell
//
//        cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
////        cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "Checkbox"), for: .normal)
////        orderIdArr.append(consolidateProductsListArr[indexPath.row].id ?? "")
//
//        if orderIdArr.count == 1 {
//
//            orderIdArr.removeAll()
//
//        }else {
//
//            for i in 0..<orderIdArr.count {
//
//                let catId = orderIdArr[i]
//
//              if catId == allPackagesListArr[indexPath.row].productID {
//
//                orderIdArr.remove(at: i)
//                print(orderIdArr)
//                dummyArr[indexPath.row] = 0
//                   // categoryCell.categoriesCellBgVw.backgroundColor = .clear
//                break
//
//
//                }else{
//
//                  //  cell.categoriesCellBgVw.backgroundColor = .selectionLightBlue
//
//                    //categoryIdArr.append(categoryIdStr)
//                }
//
//            }
//        }
//      /*  for (key,value) in orderIdArr.enumerated() {
//
//            if consolidateProductsListArr[indexPath.row].id == value {
//
//                orderIdArr.remove(at: key)
//                break
//            }
//
//        }*/
//
//        print("Order Id 123: \(orderIdArr)")
//    }
}

//MARK:-  API Services

extension ReadyToSendVC {
    
    func getAllPackagesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:"ReadyToSend",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AllPackagesListUrl)")

            ApiHandler.getData(url: AppUrls.AllPackagesListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(AllPackagesModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.allPackagesListArr = loginData.data
                                print("Dict Data: \(String(describing: self.allPackagesListArr))")

                                if self.allPackagesListArr.count > 0 {
                                  
                                        self.dummyArr = []
                                        for i in 0..<self.allPackagesListArr.count{
                                            
                                            self.dummyArr.append(0)
                                        }
                                    if self.dummyArr.count > 0{
                                        self.dummyArr[0] = 1
                                        self.orderIdArr.append(self.allPackagesListArr[0].productID)
                                    }
                                    self.tableview.isHidden = false
                                    self.basicTableView.isHidden = false
                                    self.emptyBasicView.isHidden = true

                                    DispatchQueue.main.async {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.reloadData()

                                    }
                                }else {
                                    self.basicTableView.isHidden = true
                                    self.emptyBasicView.isHidden = false

                                    self.tableview.isHidden = true
                                }
                                
                            }else {
                                self.hideActivityIndicator()
                                self.basicTableView.isHidden = true
                                self.emptyBasicView.isHidden = false

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.basicTableView.isHidden = true
                            self.emptyBasicView.isHidden = false

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func checkSubscriptionPlanApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId()
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.CheckSubscriptionPlanUrl)")

            ApiHandler.getData(url: AppUrls.CheckSubscriptionPlanUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(CheckSubscriptionPlanModel.self, from: data){
                            if loginData.status == 200{
                                DispatchQueue.main.async {
                                    
//                                    let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.SubscriptionVC) as!  SubscriptionVC
//                                    vc.isFrom = "Consolidate"
//                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
//                                    let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.ConsolidateVC) as! ConsolidateVC
//                                    self.navigationController?.pushViewController(vc, animated: true)
                                    DispatchQueue.main.async {

                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SelectedAddressViewController) as! SelectedAddressViewController
                                        vc.isFromConsolidate = true
                                        vc.contryId = self.countryId
                                        vc.orderedItemsId = self.orderIdArr
                                        vc.iscontryId = self.countryId
                                        self.navigationController?.pushViewController(vc, animated: true)

                                    }
//                                    let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.ConsolidateListProductsVC) as!  ConsolidateListProductsVC
//                                    self.navigationController?.pushViewController(vc, animated: true)

                                }
                                
                            }else {
                                self.hideActivityIndicator()

//                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                                
                                showAlertWithOkAction(self, title: ALERT_TITLE, message: loginData.message, buttonTitle: "OK") {
                                    
                                    DispatchQueue.main.async {
                                        
                                        let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.SubscriptionVC) as!  SubscriptionVC
                                        vc.isFrom = "Consolidate"
                                        self.navigationController?.pushViewController(vc, animated: true)

                                    }
                                }
                            }
                            
                        }else{
                            self.hideActivityIndicator()

//                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                            
                            showAlertWithOkAction(self, title: ALERT_TITLE, message: value?["message"] as? String ?? "", buttonTitle: "OK") {
                                
                                DispatchQueue.main.async {
                                    
                                    let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.SubscriptionVC) as!  SubscriptionVC
                                    vc.isFrom = "Consolidate"
                                    self.navigationController?.pushViewController(vc, animated: true)

                                }
                            }
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
   
}

//let vc = storyboard?.instantiateViewController(identifier: Identifiers.PackagingAndShippingVC) as!  PackagingAndShippingVC
//vc.isFromProceed = true
//if orderIdArr.count > 0{
//    vc.bookingPackageId = orderIdArr[0]
//}else{
//    print("no index")
//}
//
//navigationController?.pushViewController(vc, animated: true)
