//
//  InReviewViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 25/01/21.
//

import UIKit

class InReviewViewController: UIViewController {
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var basicTableView: UIView!
    @IBOutlet weak var emptyBasicView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bookNewPackageBtn: UIButton!
    @IBOutlet weak var buyFormeBtn: UIButton!
    @IBOutlet weak var headinglbl: UILabel!
   
    @IBOutlet weak var plusBtn: UIButton!
    
    
    var delgt:SegmentDelegate?
    var allPackagesListArr = [AllPackagesArr]()
    var actionButton : ActionButton!
    var refreshControl = UIRefreshControl()
    var adminId = String()

    
    //MARK:-  View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.register(AllTableViewCell.nib(), forCellReuseIdentifier: AllTableViewCell.identifier)
        tableview.estimatedRowHeight = tableview.rowHeight
        tableview.rowHeight = UITableView.automaticDimension
        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.tableview.addSubview(self.refreshControl)
            self.tableview.refreshControl = self.refreshControl
        }
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.reloadData()
        basicTableView.isHidden = false
        emptyBasicView.isHidden = true
        headinglbl.highlight(searchedText: "pictures")
        floatButton()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getAllPackagesListApi()
        getAdminIdApi()

//        tableview.delegate = self
//        tableview.dataSource = self
//        tableview.separatorStyle = .none
//        tableview.reloadData()

    }
    
    @objc func refresh(sender:AnyObject){
        
        getAllPackagesListApi()
        tableview.reloadData()
        self.refreshControl.endRefreshing()
    }

    func floatButton(){
        let one = ActionButtonItem(title: "Book A New Package", image: #imageLiteral(resourceName: "FEb"))
        one.action = { item in
            
            self.floatBtnAction() }
        let two = ActionButtonItem(title: "Buy For Me", image: #imageLiteral(resourceName: "FEb"))
        two.action = { item in
            
            self.floatBtnAction1() }
        actionButton = ActionButton(attachedToView: self.view, items: [one,two,])
        actionButton.setTitle("+", forState: UIControl.State())
        //actionButton.backgroundColor = #colorLiteral(red: 0.08235294118, green: 0.5725490196, blue: 0.9019607843, alpha: 1)
        actionButton.backgroundColor = UIColor.red
        actionButton.action = { button in button.toggleMenu()}
    }
    
    @objc func floatBtnAction()
       {
          print("1")
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
       
       }
    @objc func floatBtnAction1()
    {
       print("2")
     self.tabBarController?.selectedIndex = 2
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookNewpackagesVC) as! BookNewpackagesVC
//        navigationController?.pushViewController(vc, animated: true)
    
    }

    //MARK:-  IB Actions
    
    @IBAction func bookNewPackagesAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buyFormeAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(identifier: "BuyForMeProductsViewController") as!  BuyForMeProductsViewController
//        vc.isFrom = "Buy"
//        navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.selectedIndex = 2

    }
    
    @IBAction func onClickPlusBtn(_ sender: UIButton) {
        self.floatBtnAction()

    }
    @objc func onClickViewMoreBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.ViewMoreVC) as! ViewMoreVC
            vc.productId = self.allPackagesListArr[sender.tag].productID
            vc.productDetailsDict = self.allPackagesListArr[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    
    @objc func onChatNowBtn(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.UserChatViewController) as! UserChatViewController
            vc.toId = self.adminId
            vc.fromId = AppHelper.getUserId()
            
            if self.allPackagesListArr[sender.tag].orderID == "" {
                vc.orderID = self.allPackagesListArr[sender.tag].bookingid
            }else {
                vc.orderID = self.allPackagesListArr[sender.tag].orderID
            }
            self.navigationController?.pushViewController(vc, animated: true)

        }
//        showAlertMessage(vc: self, titleStr: "Inprogress", messageStr: "Chat implementation is in inprogress")
    }

}
//MARK:-  Extensions
extension InReviewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPackagesListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableview.dequeueReusableCell(withIdentifier: AllTableViewCell.identifier, for: indexPath) as? AllTableViewCell else{return UITableViewCell()}
        
        cell.chatNowBtn.tag = indexPath.row
        cell.viewMoreBtn.tag = indexPath.row
        cell.chatNowBtn.addTarget(self, action: #selector(onChatNowBtn), for: .touchUpInside)
        cell.viewMoreBtn.addTarget(self, action: #selector(onClickViewMoreBtn), for: .touchUpInside)
       cell.selectionStyle = .none
        cell.selectionStyle = .none
        if self.allPackagesListArr.count > 0 {
            
            cell.loadPackageList(packageData: self.allPackagesListArr[indexPath.row])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}
//MARK:-  API Services

extension InReviewViewController {
    
    func getAllPackagesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:"InReview",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AllPackagesListUrl)")

            ApiHandler.getData(url: AppUrls.AllPackagesListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(AllPackagesModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.allPackagesListArr = loginData.data
                                print("Dict Data: \(String(describing: self.allPackagesListArr))")

                                if self.allPackagesListArr.count > 0 {
                                    
                                    self.basicTableView.isHidden = false
                                    self.emptyBasicView.isHidden = true

                                    self.tableview.isHidden = false

                                    DispatchQueue.main.async {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.reloadData()

                                    }
                                }else {
                                    
                                    self.basicTableView.isHidden = true
                                    self.emptyBasicView.isHidden = false

                                    self.tableview.isHidden = true
                                    
                                }
                                
                            }else {
                                self.hideActivityIndicator()
                                self.basicTableView.isHidden = true
                                self.emptyBasicView.isHidden = false

                                self.tableview.isHidden = true

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.tableview.isHidden = true
                            self.basicTableView.isHidden = true
                            self.emptyBasicView.isHidden = false

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func getAdminIdApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            print("UrlString:  \(AppUrls.GetChatAdminIdUrl)")

            ApiHandler.getData(url: AppUrls.GetChatAdminIdUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(AdminDataModel.self, from: data){
                            if loginData.status == 200{

                                self.adminId = loginData.data
                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
