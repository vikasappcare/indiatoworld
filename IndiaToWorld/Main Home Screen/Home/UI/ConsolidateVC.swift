//
//  ConsolidateVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 06/01/21.
//

import UIKit

class ConsolidateVC: UIViewController {
    
    //MARK:-  IB Outlets

    @IBOutlet weak var consolidateCollectionVW: UICollectionView!
    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var listTableview: UITableView!
  
    var selectedCell = ""
    var consolidateCountryArr = [ConsolidateCountryListArr]()
    var consolidateProductsListArr = [ConsolidateProductsListArr]()
    var selectedCountry = Int()
    var countryId = String()
    var isProductSelected = Bool()
    var orderIdArr = [String]()
    var dummyArr = [Int]()
    var refreshControl = UIRefreshControl()

    
    //MARK:-  View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listTableview.estimatedRowHeight = listTableview.rowHeight
        listTableview.rowHeight = UITableView.automaticDimension
        tableVwNibs()
        selectedCountry = 0
        
        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.listTableview.addSubview(self.refreshControl)
            self.listTableview.refreshControl = self.refreshControl
        }
//        consolidateCollectionVW.delegate = self
//        consolidateCollectionVW.dataSource = self
//        consolidateCollectionVW.reloadData()
        listTableview.separatorStyle = .none
        navTitle(heading: NavTitle.Consolidate)
//        listTableview.reloadData()
        headinglbl.highlight(searchedText: "single")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isProductSelected = false
        selectedCountry = 0
        orderIdArr = []
        getConsolidateCountriesListApi()
    }
    
    @objc func refresh(sender:AnyObject){
        orderIdArr = []
        getConsolidateCountriesListApi()
        listTableview.reloadData()
        self.refreshControl.endRefreshing()
    }

    func tableVwNibs() {
        
        listTableview.register(ConsolidateTableViewCell.nib(), forCellReuseIdentifier: ConsolidateTableViewCell.identifier)

    }
    //MARK:-  IB Actions
    
    @objc func onClickCheckBoxBtn(_ sender: UIButton) {
        
//        let cell = listTableview.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! ConsolidateTableViewCell
//        if isProductSelected == false {
//
//            cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "SelcetedIcon"), for: .normal)
//            isProductSelected = true
//            orderIdArr.append(consolidateProductsListArr[sender.tag].id ?? "")
//        }else {
//
//            cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "Checkbox"), for: .normal)
//            isProductSelected = false
//
//            for (key,value) in orderIdArr.enumerated() {
//
//                if consolidateProductsListArr[sender.tag].id == value {
//
//                    orderIdArr.remove(at: key)
//                }else {
//
////                    orderIdArr.append(consolidateProductsListArr[sender.tag].id ?? "")
//                }
//
////                if orderIdArr.count == 1 {
////                    orderIdArr.removeAll()
////
////                }else {
////                    if consolidateProductsListArr[sender.tag].id == value {
////
////                        orderIdArr.remove(at: key)
////                    }
////
////                }
//            }
//        }
//
//        print("Order Id : \(orderIdArr)")
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        

        if orderIdArr.count < 2 {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: "Please select atleast two products to consolidate as one package")


        }else {
            DispatchQueue.main.async {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SelectedAddressViewController) as! SelectedAddressViewController
                vc.isFromConsolidate = true
                vc.contryId = self.countryId
                vc.orderedItemsId = self.orderIdArr
                self.navigationController?.pushViewController(vc, animated: true)

            }
        }
    }
    
}
//MARK:- UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension ConsolidateVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return consolidateCountryArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = consolidateCollectionVW.dequeueReusableCell(withReuseIdentifier: "ConsolidateCollectionViewCell", for: indexPath) as! ConsolidateCollectionViewCell
        cell.headinglbl.backgroundColor = UIColor.white
        cell.headinglbl.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.1647058824, blue: 0.5764705882, alpha: 1)
        cell.headinglbl.layer.borderWidth = 0.8
        cell.headinglbl.padding = UIEdgeInsets(top: 8, left: 15, bottom: 8, right: 15)
        cell.headinglbl.text = consolidateCountryArr[indexPath.row].name
        cell.headinglbl.sizeToFit()
        cell.headinglbl.layer.cornerRadius = cell.headinglbl.frame.height/2
        cell.headinglbl.layer.masksToBounds = true
        cell.isSelected = false
        
        if selectedCountry == indexPath.item {
            
            cell.headinglbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.headinglbl.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.1647058824, blue: 0.5764705882, alpha: 1)

        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedCountry = indexPath.row
        let selectedCell = collectionView.cellForItem(at: indexPath)
        selectedCell?.isSelected = true
        collectionView.scrollToItem(at:indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
        countryId = consolidateCountryArr[indexPath.row].countryID
        getConsolidateProductsListApi()
//        self.SlotsSelectedCell = consolidateCountryArr[indexPath.row].name
    }
}
//MARK:-  UITableViewDelegate, UITableViewDataSource
extension ConsolidateVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consolidateProductsListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = listTableview.dequeueReusableCell(withIdentifier: ConsolidateTableViewCell.identifier, for: indexPath) as? ConsolidateTableViewCell else{return UITableViewCell()}
        cell.selectionStyle = .none
        
        if consolidateProductsListArr.count > 0 {
            
            cell.loadConsolidateData(consolidateItem: consolidateProductsListArr[indexPath.row])
        }
        if dummyArr.count > 0 {
            
            if dummyArr[indexPath.row] == 1 {
                
                cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
            }else {
                
                cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
            }

        }
        
        cell.checkBoxBtn.tag = indexPath.row
        cell.checkBoxBtn.addTarget(self, action: #selector(onClickCheckBoxBtn), for: .touchUpInside)//Checkbox
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = listTableview.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! ConsolidateTableViewCell

        cell.checkBoxImgVw.image = #imageLiteral(resourceName: "SelcetedIcon")
        orderIdArr.append(consolidateProductsListArr[indexPath.row].id ?? "")
        dummyArr[indexPath.row] = 1
        print("Order Id : \(orderIdArr)")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = listTableview.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! ConsolidateTableViewCell

        cell.checkBoxImgVw.image = #imageLiteral(resourceName: "Checkbox")
//        cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "Checkbox"), for: .normal)
//        orderIdArr.append(consolidateProductsListArr[indexPath.row].id ?? "")

        if orderIdArr.count == 1 {
            
            orderIdArr.removeAll()
            
        }else {
            
            for i in 0..<orderIdArr.count {
                
                let catId = orderIdArr[i]
                
              if catId == consolidateProductsListArr[indexPath.row].id {
                    
                orderIdArr.remove(at: i)
                print(orderIdArr)
                dummyArr[indexPath.row] = 0
                   // categoryCell.categoriesCellBgVw.backgroundColor = .clear
                break
                   
                
                }else{

                  //  cell.categoriesCellBgVw.backgroundColor = .selectionLightBlue

                    //categoryIdArr.append(categoryIdStr)
                }
               
            }
        }
      /*  for (key,value) in orderIdArr.enumerated() {
            
            if consolidateProductsListArr[indexPath.row].id == value {
                
                orderIdArr.remove(at: key)
                break
            }
            
        }*/
        
        print("Order Id 123: \(orderIdArr)")
    }
}
//MARK:-  API Services

extension ConsolidateVC {
    
    func getConsolidateCountriesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.ConsolidateCountriesListUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(ConsolidateCountryListModel.self, from: data){
                            if loginData.status == 200{
                                self.consolidateCountryArr = loginData.data
                                print(self.consolidateCountryArr)
                                if self.consolidateCountryArr.count > 0 {
                                    
                                    self.countryId = self.consolidateCountryArr[0].countryID
//                                    self.noDataImgVw.isHidden = true
                                    self.consolidateCollectionVW.isHidden = false
                                    DispatchQueue.main.async {
                                        
                                        self.consolidateCollectionVW.delegate = self
                                        self.consolidateCollectionVW.dataSource = self
                                        self.consolidateCollectionVW.reloadData()

                                    }

                                    self.getConsolidateProductsListApi()
                                }else {
                                    
//                                    self.noDataImgVw.isHidden = false
                                    self.consolidateCollectionVW.isHidden = true
                                }

                            }else {
                                
//                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
//                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func getConsolidateProductsListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.country_id:countryId,
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ConsolidateProductsListUrl)")

            ApiHandler.getData(url: AppUrls.ConsolidateProductsListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(ConsolidateProductsListModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.consolidateProductsListArr = loginData.data
                                print("Dict Data: \(String(describing: self.consolidateProductsListArr))")
                                if self.consolidateProductsListArr.count > 0 {
                                    self.dummyArr = []
                                    for i in 0..<self.consolidateProductsListArr.count{
                                        
                                        self.dummyArr.append(0)
                                    }
                                    self.listTableview.isHidden = false
                                    DispatchQueue.main.async {
                                        self.listTableview.delegate = self
                                        self.listTableview.dataSource = self
                                        self.listTableview.reloadData()

                                    }
                                }else {
                                    
                                    self.listTableview.isHidden = true
                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)

                                }
                            }else {
                                self.hideActivityIndicator()
                                self.listTableview.isHidden = true

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.listTableview.isHidden = true
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
}
