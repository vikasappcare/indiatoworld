//
//  UserChatViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 01/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import JSQMessagesViewController
import Photos


class UserChatViewController: JSQMessagesViewController,UINavigationControllerDelegate {
    var fromId : String!
    var toId : String!
    var chatConversionList: chatConversion? = nil
    var selectChatDataArray : NSMutableArray = []
    var messageCount : Int!
    var messages = [JSQMessage]()
    var messageData : String!
    var name : String!
    var mailId : String!
    var timer : Timer!
    var orderID = String()
    var myImgstr = String()
    var incomingimgstr = String()
    let datePicker = UIDatePicker()
    lazy var incomingAvatarImageView: JSQMessagesAvatarImage = self.setupIncomingAvartar()
    lazy var outgoingAvatarImageView: JSQMessagesAvatarImage = self.setupoutgoingingAvartar()
    var arrCount : Int!
    
    var returnBtn : UIButton?
    var proceedBtn: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          self.inputToolbar.contentView.leftBarButtonItem = nil
        self.senderId = fromId
        print("print",fromId,toId)
       // self.senderId = "210107091749"
        self.senderDisplayName = ""
        // selectChatWebServiceCall1()
        
        print("imagessss",myImgstr ?? "in",incomingimgstr ?? "out")
        if Reachability.isConnectedToNetwork() {
            ChatuserHistory()
        }else {
            
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
//            showAlert(title: Network.title, message: Network.message)
        }
        navbarItems()
        setUpButtons()
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        
           timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(ChatuserHistory), userInfo: nil, repeats: true)
        

        print("in",setupIncomingAvartar)
        print("out",setupoutgoingingAvartar)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        addSocialPagesButtonsToView()

    }
    override func viewWillAppear(_ animated: Bool) {
      //  ChatuserHistory()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil{
            timer.invalidate()
        }
        timer = nil
        // locationManager.stopUpdatingLocation()
    }
    
    func setUpButtons(){
        
        let returnBtn = self.makeButton(title: "Return the order", titleColor: UIColor(red: 112.0/255.0, green: 112.0/255.0, blue: 112.0/255.0, alpha: 1.0), font: UIFont.init(name: "HKGrotesk-Medium", size: 14.0), background: UIColor(red: 112.0/255.0, green: 112.0/255.0, blue: 112.0/255.0, alpha: 0.10), cornerRadius: 20.0, borderWidth: 0, borderColor: .clear)
        view.addSubview(returnBtn)
        // Adding Constraints
        returnBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        returnBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
//        button.widthAnchor.constraint(equalToConstant: 150).isActive = true
//        button.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        returnBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        returnBtn.addTarget(self, action: #selector(returnBtnPressed(_ :)), for: .touchUpInside)

        let proceedBtn = self.makeButton(title: "Proceed further", titleColor: UIColor(red: 1.0/255.0, green: 147.0/255.0, blue: 33.0/255.0, alpha: 1.0), font: UIFont.init(name: "HKGrotesk-Medium", size: 14.0), background: UIColor(red: 1.0/255.0, green: 147.0/255.0, blue: 33.0/255.0, alpha: 0.10), cornerRadius: 20.0, borderWidth: 0, borderColor: .clear)
        view.addSubview(proceedBtn)
        // Adding Constraints
        proceedBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        proceedBtn.leftAnchor.constraint(equalTo: returnBtn.rightAnchor, constant: 20).isActive = true
        proceedBtn.widthAnchor.constraint(equalTo: returnBtn.widthAnchor, multiplier: 1).isActive = true

//        returnBtn.widthAnchor.constraint(equalToConstant: 150).isActive = true
        proceedBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        proceedBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        proceedBtn.addTarget(self, action: #selector(proceedBtnPressed(_ :)), for: .touchUpInside)

    }
    private func setupIncomingAvartar() -> JSQMessagesAvatarImage {
        if incomingimgstr != nil && incomingimgstr != ""{
            if URL(string: incomingimgstr) != nil{
                let url = URL(string: incomingimgstr)
                let data = try? Data(contentsOf: url!)
                
                if let imageData = data {
                    // let image = UIImage(data: imageData)
                    return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(data: imageData), diameter: 40)
                }
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "new_logo"), diameter: 40)
            }
            else
            {
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "new_logo"), diameter: 40)
                
            }
        }
            
        else
        {
            return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "new_logo"), diameter: 40)
        }
    }
    private func setupoutgoingingAvartar() -> JSQMessagesAvatarImage {
        
        
        if myImgstr != nil && myImgstr != "" {
            if URL(string: myImgstr) != nil{
                let url = URL(string: myImgstr)
                let data = try? Data(contentsOf: url!)
                
                if let imageData = data {
                    // let image = UIImage(data: imageData)
                    return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(data: imageData), diameter: 40)
                }
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "emptyIcon"), diameter: 40)
            }
            else
            {
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "emptyIcon"), diameter: 40)
                
            }
        }
        else{
            return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "emptyIcon"), diameter: 40)
        }
    }
    func navTitleWithImageAndText(titleText: String, imageName: String) -> UIView {
        
        // Creates a new UIView
        let titleView = UIView()
        
        // Creates a new text label
        let label = UILabel()
        label.text = titleText
        label.sizeToFit()
        label.center = titleView.center
        label.textAlignment = NSTextAlignment.center
        
        // Creates the image view
        let image = UIImageView()
        // image.image = UIImage(url: URL(string: myImgstr))
        
        let url = URL(string: myImgstr)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            image.image  = UIImage(data: imageData)
        }
        
        
        // Maintains the image's aspect ratio:
        let imageAspect = image.image!.size.width / image.image!.size.height
        
        // Sets the image frame so that it's immediately before the text:
        let imageX = label.frame.origin.x - label.frame.size.height * imageAspect
        let imageY = label.frame.origin.y
        
        let imageWidth = label.frame.size.height * imageAspect
        let imageHeight = label.frame.size.height
        
        image.frame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
        
        image.contentMode = UIView.ContentMode.scaleAspectFit
        
        // Adds both the label and image view to the titleView
        titleView.addSubview(label)
        titleView.addSubview(image)
        
        // Sets the titleView frame to fit within the UINavigation Title
        titleView.sizeToFit()
        
        return titleView
        
    }
    func navbarItems()
    {
        let closeImage: UIImage? = UIImage.init(named: "back arrow")!.withRenderingMode(.alwaysOriginal)
        let close =  UIBarButtonItem(image: closeImage,
                                     landscapeImagePhone: nil, style: .done, target: self,
                                     action: #selector(btn_clicked(_:)))
        let userImage = UIImageView()
                   // print(incomingimgstr)
                    if incomingimgstr == nil{
                        userImage.image = UIImage(named: "emptyIcon")
        
                    }
                    else if incomingimgstr == ""{
                        userImage.image = UIImage(named: "emptyIcon")
        
                    }
                    else if incomingimgstr == "http://35.176.88.39/uploads/"{
                        userImage.image = UIImage(named: "emptyIcon")
        
                    }
                    else if incomingimgstr == "uploads/"{
                        userImage.image = UIImage(named: "emptyIcon")
        
                    }
                    else{
                        
                        let url = URL(string: incomingimgstr)
                        let data = try? Data(contentsOf: url!)
                        if let imageData = data{
                            userImage.image = UIImage(data: imageData)
                        }
                        
        
                    }
      //  userImage.image = UIImage(named: "photo-1")
       userImage.contentMode = .scaleToFill
       userImage.layer.cornerRadius = userImage.frame.width/2
       // userImage.layer.masksToBounds = true
        //        if incomingimgstr != nil && incomingimgstr != "" && incomingimgstr != "http://13.234.112.106/uploads/" {
        //            userImage.image = UIImage(url: URL(string: incomingimgstr))
        //
        //        }
        //        else{
        //            userImage.image = UIImage(named: "profile")
        //        }
        
        let titleLable = "\("India To World")\n\("justnow")"
        print(titleLable)
        let title = UIBarButtonItem.init(title: titleLable, style: .done, target: self, action: nil)
        let button2 = UIButton(type: .custom)
        button2.frame = CGRect(x: 0.0, y: 0.0, width: 60, height: 60)
        button2.setImage(userImage.image, for: .normal)
      //  button2.layer.cornerRadius = button2.frame.width/2
        button2.imageView?.contentMode = .scaleToFill
        //button2.addTarget(target, action: #selector(profileImageClicked), for: .touchUpInside)
        
        let currWidth = button2.widthAnchor.constraint(equalToConstant: 24)
        currWidth.isActive = true
        let currHeight = button2.heightAnchor.constraint(equalToConstant: 24)
        currHeight.isActive = true
        button2.layer.cornerRadius = button2.frame.width/2
       // button2.layer.masksToBounds = true
        navigationController?.navigationBar.tintColor = .black
        let barButtonItem2 = UIBarButtonItem(customView: button2)
        // barButtonItem2.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileImageClicked)))
        //  title.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileImageClicked)))
        self.navigationItem.leftBarButtonItems = [close,barButtonItem2,title]
        
//        let title1 = "Proceed Further"
//
//        let proceedBtn = UIBarButtonItem.init(title: title1, style: .done, target: self, action: #selector(proceddBtntn_clicked(_:)))
//        self.navigationItem.rightBarButtonItems = [proceedBtn]

    }
    

   // Define commmon method
    func makeButton(title: String? = nil,
                       titleColor: UIColor = .black,
                       font: UIFont? = nil,
                       background: UIColor = .clear,
                       cornerRadius: CGFloat = 0,
                       borderWidth: CGFloat = 0,
                       borderColor: UIColor = .clear) -> UIButton {
    let button = UIButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle(title, for: .normal)
    button.backgroundColor = background
    button.setTitleColor(titleColor, for: .normal)
    button.titleLabel?.font = font
    button.layer.cornerRadius = 6.0
    button.layer.borderWidth = 2
    button.layer.borderColor = UIColor.clear.cgColor
    return button
  }
    
    func addSocialPagesButtonsToView()
        {
            var scrollView : UIScrollView!

            let pageNames = NSArray(objects: "Open My Facebook Page","Open My TripAdvisor Page")
            scrollView = UIScrollView(frame:CGRect(x:0, y:10, width:Int(collectionView.frame.width+100), height:50))
            scrollView.sizeToFit()
          //  scrollView.setTranslatesAutoresizingMaskIntoConstraints(false)
            for i in 0..<2 {

               // let catogry = categoryList[i] as! CategoryModel
//                defaultSocialMediaButtonsScrollContentSize = defaultSocialMediaButtonsScrollContentSize + 200
                scrollView.addSubview(self.createViewForSocialPagesButtons(name: pageNames[i] as! String, heightOfParent: Int(scrollView.frame.size.height), tag: i))
                scrollView.contentSize = CGSize(width: 200, height:40)

            }
//            defaultSocialMediaButtonsScrollContentSize = defaultSocialMediaButtonsScrollContentSize + 40
            scrollView.contentSize = CGSize(width: 200, height:40)
            scrollView.showsHorizontalScrollIndicator = true
            scrollView.showsVerticalScrollIndicator = false

            view.addSubview(scrollView)
        }
        func createViewForSocialPagesButtons(name: String, heightOfParent: Int, tag: Int) -> UIButton {

            let button = UIButton(frame: CGRect(x:250 , y: 0, width: 220, height: 40))
            button.backgroundColor = UIColor.clear
            button.setTitle(name, for: .normal)
            button.tag = tag
            button.setTitleColor(UIColor.blue, for: .normal)
            button.layer.cornerRadius = 2;
            button.layer.borderWidth = 1;
            button.layer.borderColor = UIColor.blue.cgColor
            button.titleLabel?.textAlignment = NSTextAlignment.center
            let font = UIFont(name: "Rubik", size: 17)
            button.titleLabel?.font = font
            button.addTarget(self, action: #selector(openPageButtonTapped(_:)), for: .touchUpInside)

//            defaultXValueforView = defaultXValueforView + 225;
            return button
        }

    @objc func openPageButtonTapped(_ sender : UIButton){
            let buttonTapped = sender.tag
//            if(Commons.connectedToNetwork())
//            {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ReturnFormVC") as! ReturnFormVC
                if(buttonTapped==0)
                {
//                    nextViewController.pageUrlAdress = "https://www.facebook.com/TripAdvisor/"
                }
                else if(buttonTapped==1)
                {
//                  nextViewController.pageUrlAdress = "https://www.tripadvisor.com.ph/Restaurant_Review-g298460-d10229090-Reviews-Vikings_SM_City_Cebu_Restaurant-Cebu_City_Cebu_Island_Visayas.html"
                }

                self.navigationController?.pushViewController(nextViewController, animated:true)

//            }
//            else
//            {
//                Commons.showAlert("Please check your connection", VC: self)
//            }
        }
    // Button Action
     @objc func returnBtnPressed(_ sender: UIButton) {
            print("Pressed")
        
        DispatchQueue.main.async {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReturnFormVC") as! ReturnFormVC
            vc.orderId = self.orderID
            self.navigationController?.pushViewController(vc, animated: true)

        }
      }
    
    // Button Action
     @objc func proceedBtnPressed(_ sender: UIButton) {
            print("Pressed")
        
        DispatchQueue.main.async {
//            self.navigationController?.popViewController(animated: true)

            self.proceedToReadyToSendApi()
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
////            self.navigationController?.pushViewController(vc, animated: true)
//            self.navigationController?.backToViewController(vc: vc)
        }
      }
    @objc func btn_clicked(_ sender: UIBarButtonItem)
    {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @objc func proceddBtntn_clicked(_ sender: UIBarButtonItem)
    {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }


}

extension UserChatViewController{
    //Apis Method
    @objc func ChatuserHistory(){
        let url = AppUrls.GetChatHistory
        let userid =  AppHelper.getUserId()
        
        let paramters: [String:Any] = ["from_id":fromId!,"to_id" : toId!,"order_id":orderID]
       // let paramters: [String:Any] = ["from_id":"210107091749","to_id" : "210105093340"]
        
        print(url)
        print(paramters)
        
        if Connectivity.isConnectedToInternet() {
            
//            showActivityIndicator()
                        
            ApiHandler.getData(url: url, parameters: paramters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        let usernameInfoDetail = try JSONDecoder().decode(chatConversion.self, from: data)
                        self.chatConversionList = usernameInfoDetail
                        if self.chatConversionList?.data?.count == 0{
                            print("no data")
                            showAlertMessage(vc: self, titleStr: Network.title, messageStr: self.chatConversionList?.message ?? "")

                        }else{
                            self.messages = []
                            for i in 0..<(self.chatConversionList?.data?.count)!
                            {
                                self.messageCount = i
                                self.getMessages()
                            }
                            if self.arrCount != (self.chatConversionList?.data?.count)!{
                                
                                self.collectionView.reloadData()
                                self.scrollToBottom()
                                self.arrCount = (self.chatConversionList?.data?.count)!
                            }
                           // print("pllll",self?.chatConversionList?.data[0]?.chatHistory?.chatHistoryDescription)
                        }
                    }catch{
                        print(error.localizedDescription)
                        //self!.showAlert(title: Alerts.Alert, message: error.localizedDescription)
                    }

                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }

     /*   RestService.RondomUsername(url: url, method: .post, params: paramters, isLoader: false, title: "", description: "Loading...", vc: self, success: {[weak self] (usernameInfo) in
            do{
                let usernameInfoDetail = try JSONDecoder().decode(chatConversion.self, from: usernameInfo)
                self?.chatConversionList = usernameInfoDetail
                if self?.chatConversionList?.data?.count == 0{
                    print("no data")
                }else{
                    self?.messages = []
                    for i in 0..<(self?.chatConversionList?.data?.count)!
                    {
                        self!.messageCount = i
                        self!.getMessages()
                    }
                    if self!.arrCount != (self?.chatConversionList?.data?.count)!{
                        
                        self?.collectionView.reloadData()
                        self!.scrollToBottom()
                        self!.arrCount = (self?.chatConversionList?.data?.count)!
                    }
                   // print("pllll",self?.chatConversionList?.data[0]?.chatHistory?.chatHistoryDescription)
                }
            }catch{
                print(error.localizedDescription)
                //self!.showAlert(title: Alerts.Alert, message: error.localizedDescription)
            }
        }) { (error) in
            //madhu uncommit
         //self.showAlert(title: Alerts.Alert, message: error.localizedDescription)
        }*/
    }
   func ChatuserHistory1(){
    
        let url = AppUrls.GetChatHistory
        let userid =  UserDefaults.standard.string(forKey:"userId")
        
        let paramters: [String:Any] = ["from_id":fromId!,"to_id" : toId!,"order_id":orderID]
   // let paramters: [String:Any] = ["from_id":"210107091749","to_id" : "210105093340"]
    
        print(paramters)
        print(url)
        print(paramters)
    
    if Connectivity.isConnectedToInternet() {
        
        showActivityIndicator()
                    
        ApiHandler.getData(url: url, parameters: paramters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
           self.hideActivityIndicator()

            if let data = data{
                print(data)
                do{
                    let usernameInfoDetail = try JSONDecoder().decode(chatConversion.self, from: data)
                    self.chatConversionList = usernameInfoDetail
                    if self.chatConversionList?.data?.count == 0{
                        print("no data")
                    }else{
                        for i in 0..<(self.chatConversionList?.data?.count)!
                        {
                            self.messageCount = i
                            self.getMessages()
                        }
                        if self.arrCount != (self.chatConversionList?.data?.count)!{
                            
                            self.collectionView.reloadData()
                            self.scrollToBottom()
                            self.arrCount = (self.chatConversionList?.data?.count)!
                        }
                   //     print("pllll",self?.chatConversionList?.data[0]?.chatHistory?.chatHistoryDescription)
                    }
                }catch{
                    print(error.localizedDescription)
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error.localizedDescription )
                }

            }else{
                print("Error\(String(describing: error))")
                self.hideActivityIndicator()
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

            }
        }

    }else {
        
        self.hideActivityIndicator()
        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
    }

    /*    RestService.RondomUsername(url: url, method: .post, params: paramters, isLoader: true, title: "", description: "Loading...", vc: self, success: {[weak self] (usernameInfo) in
            do{
                let usernameInfoDetail = try JSONDecoder().decode(chatConversion.self, from: usernameInfo)
                self?.chatConversionList = usernameInfoDetail
                if self?.chatConversionList?.data?.count == 0{
                    print("no data")
                }else{
                    for i in 0..<(self?.chatConversionList?.data?.count)!
                    {
                        self!.messageCount = i
                        self!.getMessages()
                    }
                    if self!.arrCount != (self?.chatConversionList?.data?.count)!{
                        
                        self?.collectionView.reloadData()
                        self!.scrollToBottom()
                        self!.arrCount = (self?.chatConversionList?.data?.count)!
                    }
               //     print("pllll",self?.chatConversionList?.data[0]?.chatHistory?.chatHistoryDescription)
                }
            }catch{
                print(error.localizedDescription)
                self!.showAlert(title: Alerts.Alert, message: error.localizedDescription)
            }
        }) { (error) in
            //madhu uncommit
       //  self.showAlert(title: Alerts.Alert, message: error.localizedDescription)
        }*/
    }
    
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        
        let date = date
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date!)
        let minutes = calendar.component(.minute, from: date!)
        let seconds = calendar.component(.second, from: date!)
        print("hours = \(hour):\(minutes):\(seconds)")
        
        if minutes < 10 {
            var minutes = String(minutes)
            minutes = String(format: "%02d", minutes)
        }
        //  var newText = text + "\n\n \(hour):\(minutes)"
        
        //  (message?.text)! + "\n\n \(hour):\(minutes)"
        messages.append(message!)
        messageData  = message?.text
        sendChatWebServiceCall()
        finishSendingMessage()
    }
    func scrollToBottom() {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: self.messages.count-1, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        }
    }
    func sendChatWebServiceCall() {
        let url = AppUrls.GetChatSend
        let userid =  UserDefaults.standard.string(forKey:"userId")
        let paramters: [String:Any] = ["from_id": fromId!,"to_id" : toId!,"message" : messageData!,"order_id":orderID]
      //  let paramters: [String:Any] = ["from_id":"210107091749","to_id" : "210105093340"]
        print(paramters)
        print(url,paramters,"chat")
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getJsonData(url: url, parameters: paramters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        let message = data["message"] as? String

                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }

      /*  RestService.RondomUsername(url: url, method: .post, params: paramters, isLoader: true, title: "", description: "Loading...", vc: self, success: { [weak self] (SettingsInfo : Data?) in
            do{
                if let jsonData = SettingsInfo
                {
                    let json = try? JSON(data: jsonData)
                    let message = json!["message"].stringValue
                    //  self!.self.showAlert(title: "Message", message: message)
                }
            }catch{
                self!.showAlert(title: Alerts.Alert, message: error.localizedDescription)
            }
            
        }) { (error) in
            //madhu uncommit
        // self.showAlert(title: Alerts.Alert, message: error.localizedDescription)
        }*/
        
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        return NSAttributedString(string: messageUsername!)
    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    //    {
    //        return 15
    //    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        // messages to show
        let msg = messages[indexPath.row]
        if !msg.isMediaMessage {
            
            if msg.senderId! == senderId {
                cell.textView.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//                cell.textView.backgroundColor = #colorLiteral(red: 0.0403117165, green: 0.1795804501, blue: 0.4042866826, alpha: 1)
            }else{
                cell.textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            cell.textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: cell.textView.textColor ?? UIColor.white]
        }
        return cell
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        let message = messages[indexPath.row]
        // I want to show incomingAvatarImageView only
        if message.senderId != self.senderId {
            return incomingAvatarImageView
            
        } else {
            return outgoingAvatarImageView
        }
        return nil
    }
    //    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource? {
    //        return nil
    //    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        let message = messages[indexPath.row]
        
        // return bubbleFactory?.outgoingMessagesBubbleImage(with: .lightGray)
        if message.senderId != self.senderId {
            return bubbleFactory?.incomingMessagesBubbleImage(with: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        } else {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1))
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!){
        ImagePickerManager().pickImage(self) { (img) in
            //                  self.userImgView.image = img
            //                  self.updatedprofile(img:img)
            let photoItem = JSQPhotoMediaItem(image: img)!
            let mediaItem = JSQPhotoMediaItem(image: nil)
            mediaItem!.appliesMediaViewMaskAsOutgoing = true
            mediaItem!.image = UIImage(data: img.jpegData(compressionQuality: 0.5)!)
            let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: mediaItem)
            self.messages.append(message!)
            self.finishSendingMessage()
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
    func getMessages() -> [JSQMessage] {
        let getFriends = chatConversionList?.data?[messageCount]
        var message1 : JSQMessage!
//        message1 = JSQMessage(senderId: getFriends?.from_id, displayName: "", text: getFriends?.description)
        
       // message1 = JSQMessage(senderId: "210107091749", displayName: "", text: "hiii")
        message1 = JSQMessage(senderId: getFriends?.fromID ?? "0", displayName: "", text: getFriends?.datumDescription ?? "hello")
        messages.append(message1!)
        //   messages.append(message2!)
        return messages
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        let message = self.messages[indexPath.item]
        if indexPath.item == 0 {
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        
        if indexPath.item -  1 > 0{
            let previousMessage = self.messages[indexPath.item - 1 ]
            
            if  ( ( message.date.timeIntervalSince(previousMessage.date) / 60 ) > 1){
                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
            }
        }
        
        
        return nil
    }
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)!
            self.messages.append(message)
        
        //Optional: play sent sound
        
           self.finishSendingMessage()
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        if indexPath.item -  1 > 0{
            let message = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1 ]
            
            if  ( ( message.date.timeIntervalSince(previousMessage.date) / 60 ) > 1){
                return kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        return 0.0
    }
    
}
extension JSQMessagesInputToolbar {
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        if #available(iOS 11.0, *), let window = self.window {
            let anchor = window.safeAreaLayoutGuide.bottomAnchor
            bottomAnchor.constraint(lessThanOrEqualToSystemSpacingBelow: anchor, multiplier: 1.0).isActive = true
        }
    }
}
extension UserChatViewController {
    
    func proceedToReadyToSendApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.order_id:orderID,
                PARAMS.uid: AppHelper.getUserId(),
                 
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ProceedToReadyToSendUrl)")

            ApiHandler.getJsonData(url: AppUrls.ProceedToReadyToSendUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        

                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "", buttonTitle: "Ok") {
                            
                            let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.TabBarController) as! TabBarController
                            isSelectedMenu = 4
                            self.navigationController?.pushViewController(vc, animated: true)

//                            self.tabBarController?.selectedIndex = 1
                        }
                        
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
