//
//  ConsolidateListProductsVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 06/01/21.
//

import UIKit

class ConsolidateListProductsVC: UIViewController {
    
    //MARK:-  IB Outlets
    @IBOutlet weak var consolidateCollectionVW: UICollectionView!
    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var ConsolidatelistTb: UITableView!
   
    
    var nameArr = ["America","Australia","Canda","New Jersy","USA"]
    var consolidateCountryArr = [ConsolidateCountryListArr]()
    var consolidatePackagesListArr = [ConsolidatePackageListArr]()
    var ordersArr = [Order]()
    var countryId = String()
    var selectedCountry = Int()
    var selectedPackagesIdsArr = [String]()
    var deletePackageId = String()
    var editProductId = String()
    var refreshControl = UIRefreshControl()
    var isEditPackageSelected = Bool()
    var isMenuPackageSelected = Bool()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedCountry = 0
        isEditPackageSelected = false
        navTitle(heading: NavTitle.Consolidate)
        headinglbl.highlight(searchedText: "single")
        ConsolidatelistTb.delegate = self
        ConsolidatelistTb.dataSource = self
        ConsolidatelistTb.reloadData()
        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.ConsolidatelistTb.addSubview(self.refreshControl)
            self.ConsolidatelistTb.refreshControl = self.refreshControl
        }
        ConsolidatelistTb.estimatedRowHeight = ConsolidatelistTb.rowHeight
        ConsolidatelistTb.rowHeight = UITableView.automaticDimension
        ConsolidatelistTb.separatorStyle = .none

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isEditPackageSelected = false
        isMenuPackageSelected = false
        selectedCountry = 0
        selectedPackagesIdsArr = []
        getConsolidateCountriesListApi()
        getConsolidatePackagesListApi()
    }
    
    @objc func refresh(sender:AnyObject){
        isEditPackageSelected = false
        isMenuPackageSelected = false
        getConsolidateCountriesListApi()
        getConsolidatePackagesListApi()
        ConsolidatelistTb.reloadData()
        self.refreshControl.endRefreshing()
    }

    //MARK:-  IB Actions
    
    @objc func onClickPackageMenuBtn(_ sender: UIButton) {
        
        let cell = ConsolidatelistTb.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? ConsolidateListProductTableViewCell
        isEditPackageSelected = false
        deletePackageId = ""
        if isMenuPackageSelected == false {
            cell?.popUpVw.isHidden = false
            cell?.packageMenuBtn.setImage(#imageLiteral(resourceName: "remove_gray"), for: .normal)
            isMenuPackageSelected = true
        }else {
            cell?.popUpVw.isHidden = true
            cell?.packageMenuBtn.setImage(#imageLiteral(resourceName: "more"), for: .normal)
            isMenuPackageSelected = false

        }

    }
    
    @objc func onClickPackageEditBtn(_ sender: UIButton) {
        
        
        let orders = consolidatePackagesListArr[sender.tag].orders
        
        if orders.count > 2 {
            
        let cell = ConsolidatelistTb.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? ConsolidateListProductTableViewCell
        isMenuPackageSelected = false
        cell?.packageMenuBtn.setImage(#imageLiteral(resourceName: "more"), for: .normal)
        deletePackageId = consolidatePackagesListArr[sender.tag].packageID

        if isEditPackageSelected == false {
            cell?.popUpVw.isHidden = true
            isEditPackageSelected = true
            cell?.productsListTblVw.reloadData()
        }else {
            cell?.popUpVw.isHidden = false
            isEditPackageSelected = false
            cell?.productsListTblVw.reloadData()

        }
            
        }else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: "You can't edit this package because you have two products only in this package")
        }

    }
    
    @objc func onClickPackageDeleteBtn(_ sender: UIButton) {
        
        deletePackageId = consolidatePackagesListArr[sender.tag].packageID
        deletePackageApi()
    }
    
    @objc func onClickProductRemoveBtn(_ sender: UIButton){
        
//        if isEditPackageSelected == true {
        deletePackageId = ordersArr[sender.tag].packageID
            editProductId = ordersArr[sender.tag].id
            editPackageApi()

//        }

    }
    @IBAction func onClickProceedToShipmentBtn(_ sender: Any) {
        
        for (index,value) in consolidatePackagesListArr.enumerated() {
            
            let id = consolidatePackagesListArr[index].packageID
            
            selectedPackagesIdsArr.append(id)
        }
        
        createPackagingAndShippingApi()
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.PackagingAndShippingVC) as! PackagingAndShippingVC
//        vc.countryId = countryId
//        vc.packagesIdsArr = selectedPackagesIdsArr
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
extension ConsolidateListProductsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 4
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return consolidateCountryArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = consolidateCollectionVW.dequeueReusableCell(withReuseIdentifier: "ConsolidateCollectionViewCell", for: indexPath) as! ConsolidateCollectionViewCell
        cell.headinglbl.backgroundColor = UIColor.white
        cell.headinglbl.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.1647058824, blue: 0.5764705882, alpha: 1)
        cell.headinglbl.layer.borderWidth = 0.8
        cell.headinglbl.padding = UIEdgeInsets(top: 8, left: 15, bottom: 8, right: 15)
        cell.headinglbl.text = consolidateCountryArr[indexPath.row].name
        cell.headinglbl.sizeToFit()
        cell.headinglbl.layer.cornerRadius = cell.headinglbl.frame.height/2
        cell.headinglbl.layer.masksToBounds = true
        cell.isSelected = false
        if selectedCountry == indexPath.item {
            
            cell.headinglbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.headinglbl.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.1647058824, blue: 0.5764705882, alpha: 1)

        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCountry = indexPath.row
        let selectedCell = collectionView.cellForItem(at: indexPath)
        selectedCell?.isSelected = true
        collectionView.scrollToItem(at:indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
        countryId = consolidateCountryArr[indexPath.row].countryID
        getConsolidatePackagesListApi()
        // self.SlotsSelectedCell = nameArr[indexPath.row]
    }
}
//MARK:- Extensions
extension ConsolidateListProductsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == ConsolidatelistTb {
          
        return consolidatePackagesListArr.count
            
        }else {
            
//            let orders = consolidatePackagesListArr[0].orders
            return ordersArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == ConsolidatelistTb {
            
            guard let cell = ConsolidatelistTb.dequeueReusableCell(withIdentifier: "ConsolidateListProductTableViewCell", for: indexPath) as? ConsolidateListProductTableViewCell else{return UITableViewCell()}
            
            cell.popUpVw.isHidden = true
            cell.packageMenuBtn.setImage(#imageLiteral(resourceName: "more"), for: .normal)
            cell.selectionStyle = .none
            cell.productsListTblVw.separatorStyle = .none
            cell.productsListTblVw.register(UINib(nibName: PackageListTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: PackageListTableViewCell.identifier)

            cell.loadPackageData(package: consolidatePackagesListArr[indexPath.row])
            ordersArr = consolidatePackagesListArr[indexPath.row].orders
            if ordersArr.count > 0 {
                
                cell.productListTblVwDynamicHeight.constant = CGFloat(ordersArr.count * 25)
                cell.productsListTblVw.delegate = self
                cell.productsListTblVw.dataSource = self
                cell.productsListTblVw.reloadData()

            }
            
            cell.packageMenuBtn.tag = indexPath.row
            cell.packageDeleteBtn.tag = indexPath.row
            cell.packageEditBtn.tag = indexPath.row
            cell.packageEditBtn.addTarget(self, action: #selector(onClickPackageEditBtn), for: .touchUpInside)
            cell.packageMenuBtn.addTarget(self, action: #selector(onClickPackageMenuBtn), for: .touchUpInside)
            cell.packageDeleteBtn.addTarget(self, action: #selector(onClickPackageDeleteBtn), for: .touchUpInside)
            
            return cell

        }else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PackageListTableViewCell.identifier, for: indexPath) as? PackageListTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            
            if ordersArr.count > 0 {
                
                cell.loadProductData(productData: ordersArr[indexPath.row])
            }
            
          /*  if isEditPackageSelected == true {
                
                cell.productRemoveBtn.setImage(#imageLiteral(resourceName: "remove_gray"), for: .normal)
            }else {
                cell.productRemoveBtn.setImage(#imageLiteral(resourceName: "arrow"), for: .normal)

            }*/
            
            cell.productRemoveBtn.setImage(#imageLiteral(resourceName: "remove_gray"), for: .normal)

            cell.productRemoveBtn.tag = indexPath.row
            cell.productRemoveBtn.addTarget(self, action: #selector(onClickProductRemoveBtn), for: .touchUpInside)
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == ConsolidatelistTb {
            
            let height = CGFloat((ordersArr.count * 30) + 150)
            print("height: \(height)")
            return UITableView.automaticDimension
        }else {
            
            return UITableView.automaticDimension
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
//MARK:-  API Services

extension ConsolidateListProductsVC {
    
    func getConsolidateCountriesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.ConsolidateCountriesListUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(ConsolidateCountryListModel.self, from: data){
                            if loginData.status == 200{
                                self.consolidateCountryArr = loginData.data
                                print(self.consolidateCountryArr)
                                if self.consolidateCountryArr.count > 0 {
                                    
                                    self.countryId = self.consolidateCountryArr[0].countryID
//                                    self.noDataImgVw.isHidden = true
                                    self.consolidateCollectionVW.isHidden = false
                                    DispatchQueue.main.async {
                                        
                                        self.consolidateCollectionVW.delegate = self
                                        self.consolidateCollectionVW.dataSource = self
                                        self.consolidateCollectionVW.reloadData()

                                    }

                                    self.getConsolidatePackagesListApi()
                                }else {
                                    
//                                    self.noDataImgVw.isHidden = false
                                    self.consolidateCollectionVW.isHidden = true
                                }

                            }else {
                                
                                self.hideActivityIndicator()
                                self.consolidateCollectionVW.isHidden = true
//                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            
                            self.hideActivityIndicator()
                            self.consolidateCollectionVW.isHidden = true
//                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func getConsolidatePackagesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.country_id:"",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ConsolidatePackagesListUrl)")

            ApiHandler.getData(url: AppUrls.ConsolidatePackagesListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(ConsolidatePackageListModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.consolidatePackagesListArr = loginData.data
                                print("Dict Data: \(String(describing: self.consolidatePackagesListArr))")
                                if self.consolidatePackagesListArr.count > 0 {
                                    self.ConsolidatelistTb.isHidden = false
                                    DispatchQueue.main.async {
                                        self.ConsolidatelistTb.delegate = self
                                        self.ConsolidatelistTb.dataSource = self
                                        self.ConsolidatelistTb.reloadData()

                                    }
                                }else {
                                    
                                    self.ConsolidatelistTb.isHidden = true
                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)

                                }
                            }else {
                                self.hideActivityIndicator()
                                self.ConsolidatelistTb.isHidden = true

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.ConsolidatelistTb.isHidden = true
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func createPackagingAndShippingApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.country_id: countryId,
                PARAMS.packages_id: selectedPackagesIdsArr,
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.SavePackagesShippingUrl)")

            ApiHandler.getJsonData(url: AppUrls.SavePackagesShippingUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            DispatchQueue.main.async {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.PackagingAndShippingVC) as! PackagingAndShippingVC
                                vc.countryId = self.countryId
//                                vc.packagesIdsArr = selectedPackagesIdsArr
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func deletePackageApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.package_id: deletePackageId,
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.DeletePackageUrl)")

            ApiHandler.getJsonData(url: AppUrls.DeletePackageUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {
                            self.isEditPackageSelected = false
                            self.isMenuPackageSelected = false
                            self.getConsolidatePackagesListApi()
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func editPackageApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.package_id: deletePackageId,
                PARAMS.order_ids: editProductId
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.EditPackageUrl)")

            ApiHandler.getJsonData(url: AppUrls.EditPackageUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {
                            self.isEditPackageSelected = false
                            self.isMenuPackageSelected = false
                            self.getConsolidatePackagesListApi()
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
