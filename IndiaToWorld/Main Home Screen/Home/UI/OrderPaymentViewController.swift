//
//  OrderPaymentViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 17/05/21.
//

import UIKit

class OrderPaymentViewController: UIViewController {
    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var basicTableView: UIView!
    @IBOutlet weak var emptyBasicView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bookNewPackageBtn: UIButton!
    @IBOutlet weak var buyFormeBtn: UIButton!
   
    var actionButton : ActionButton!
    var allPackagesListArr = [OrderPaymentsArr]()
    var refreshControl = UIRefreshControl()
    var ordersArr = [PaymentsOrder]()

    
    //MARK:-  View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.register(UINib(nibName: OrderPaymentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: OrderPaymentTableViewCell.identifier)
        tableview.estimatedRowHeight = tableview.rowHeight
        tableview.rowHeight = UITableView.automaticDimension

        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.tableview.addSubview(self.refreshControl)
            self.tableview.refreshControl = self.refreshControl
        }
        tableview.separatorStyle = .none
        basicTableView.isHidden = false
        emptyBasicView.isHidden = true
//        headinglbl.highlight(searchedText: "deliver")
        floatButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableview.separatorStyle = .none
        getAllPackagesListApi()
    }

    @objc func refresh(sender:AnyObject){
        
        getAllPackagesListApi()
        tableview.reloadData()
        self.refreshControl.endRefreshing()
    }

    func floatButton(){
        let one = ActionButtonItem(title: "Book A New Package", image: #imageLiteral(resourceName: "FEb"))
        one.action = { item in
            
            self.floatBtnAction() }
        let two = ActionButtonItem(title: "Buy For Me", image: #imageLiteral(resourceName: "FEb"))
        two.action = { item in
            
            self.floatBtnAction1() }
        actionButton = ActionButton(attachedToView: self.view, items: [one,two,])
        actionButton.setTitle("+", forState: UIControl.State())
      //  actionButton.backgroundColor = #colorLiteral(red: 0.08235294118, green: 0.5725490196, blue: 0.9019607843, alpha: 1)
        actionButton.backgroundColor = UIColor.red
        actionButton.action = { button in button.toggleMenu()}
    }

    //MARK:-  IB Actions
    
    @IBAction func bookNewPackagesAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buyFormeAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BuyForMeProductsViewController) as!  BuyForMeProductsViewController
//        vc.isFrom = "Buy"
//        navigationController?.pushViewController(vc, animated: true)
        
        self.tabBarController?.selectedIndex = 2

        
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        self.floatBtnAction()
        
    }
    
    @objc func floatBtnAction()
       {
          print("1")
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
       
       }
    @objc func floatBtnAction1()
    {
       print("2")
     self.tabBarController?.selectedIndex = 2
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookNewpackagesVC) as! BookNewpackagesVC
//        navigationController?.pushViewController(vc, animated: true)
    
    }
    //MARK:-  IB Actions
    
    @objc func onClickPayNowBtn(_ sender: UIButton){
        
       
        DispatchQueue.main.async {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.OrderPaymentDetailsViewController) as! OrderPaymentDetailsViewController
            if self.allPackagesListArr[sender.tag].packageTitle == nil {
                
                if self.allPackagesListArr[sender.tag].orderID == "" {
                    
                    vc.orderId = self.allPackagesListArr[sender.tag].bookingid ?? ""

                }else {
                    vc.orderId = self.allPackagesListArr[sender.tag].orderID ?? ""

                }
                vc.orderType = "single"
            }else {
                
                vc.orderId = self.allPackagesListArr[sender.tag].packageOrderID ?? ""
                vc.orderType = "package"
            }
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
}
//MARK:- Extensions
extension OrderPaymentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableview {
            
        return allPackagesListArr.count
            
        }else {
            
            return ordersArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableview {
            
            guard let cell = tableview.dequeueReusableCell(withIdentifier: OrderPaymentTableViewCell.identifier, for: indexPath) as? OrderPaymentTableViewCell else{return UITableViewCell()}
            
            cell.ordersTblVw.register(UINib(nibName: PackageListTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: PackageListTableViewCell.identifier)

            if allPackagesListArr.count > 0 {
                
                cell.loadOrderPaymentsList(orderData: allPackagesListArr[indexPath.row])
                
                ordersArr = allPackagesListArr[indexPath.row].orders ?? []

                if allPackagesListArr[indexPath.row].orders == nil {
                    
                    cell.ordersTblVwDynamicHeight.constant = 0

                }else {
                    if ordersArr.count > 0 {
                        
                        cell.ordersTblVwDynamicHeight.constant = CGFloat(ordersArr.count * 25)
                        cell.ordersTblVw.delegate = self
                        cell.ordersTblVw.dataSource = self
                        cell.ordersTblVw.reloadData()

                    }else {
                        
                        cell.ordersTblVwDynamicHeight.constant = 0

                    }

                }

                
                if allPackagesListArr[indexPath.row].paymentDetailsStatus == "1"{
                    
                    cell.payNowBtn.isEnabled = true
                    cell.payNowBtn.backgroundColor = #colorLiteral(red: 0.0403117165, green: 0.1795804501, blue: 0.4042866826, alpha: 1)

                    cell.paymentDetailsStatusLbl.text = "Payment details are available"
                    cell.paymentDetailsStatusLbl.textColor = #colorLiteral(red: 0.003921568627, green: 0.5764705882, blue: 0.1294117647, alpha: 1)
                    cell.paymentDetailsVw.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.5764705882, blue: 0.1294117647, alpha: 0.2)

                }else {
                    cell.payNowBtn.isEnabled = false
                    cell.payNowBtn.backgroundColor = #colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)
                    cell.paymentDetailsStatusLbl.text = "Payment details are under process"
                    cell.paymentDetailsStatusLbl.textColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
                    cell.paymentDetailsVw.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)

                }
            }
            
            cell.payNowBtn.tag = indexPath.row
            cell.payNowBtn.addTarget(self, action: #selector(onClickPayNowBtn), for: .touchUpInside)
           cell.selectionStyle = .none
            cell.selectionStyle = .none
            
            return cell

        }else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PackageListTableViewCell.identifier, for: indexPath) as? PackageListTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            
            if ordersArr.count > 0 {
                
                cell.loadOrderProductData(productData: ordersArr[indexPath.row])
            }
            
          /*  if isEditPackageSelected == true {
                
                cell.productRemoveBtn.setImage(#imageLiteral(resourceName: "remove_gray"), for: .normal)
            }else {
                cell.productRemoveBtn.setImage(#imageLiteral(resourceName: "arrow"), for: .normal)

            }*/
            
            cell.productRemoveBtn.setImage(#imageLiteral(resourceName: "arrow"), for: .normal)

            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableview {
            
            let height = CGFloat((ordersArr.count * 35) + 150)
            print("height: \(height)")
            return UITableView.automaticDimension
        }else {
            
            return UITableView.automaticDimension
        }
           }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//MARK:-  API Services

extension OrderPaymentViewController {
    
    func getAllPackagesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:"order_payment",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AllPackagesListUrl)")

            ApiHandler.getData(url: AppUrls.AllPackagesListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(OrderPaymentsModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.allPackagesListArr = loginData.data
                                print("Dict Data: \(String(describing: self.allPackagesListArr))")

                                if self.allPackagesListArr.count > 0 {
                                    self.tableview.isHidden = false
                                    self.emptyBasicView.isHidden = true

                                    DispatchQueue.main.async {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.reloadData()

                                    }
                                }else {
                                    
                                    self.tableview.isHidden = true
                                    self.emptyBasicView.isHidden = false
                                }
                                
                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    

}

