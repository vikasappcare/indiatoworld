//
//  INTransitViewController.swift
//  IndiaToWorld
//
//  Created by Appcare on 05/01/21.
//

import UIKit
class INTransitViewController: UIViewController {
    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var basicTableView: UIView!
    @IBOutlet weak var emptyBasicView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bookNewPackageBtn: UIButton!
    @IBOutlet weak var buyFormeBtn: UIButton!
    
    var actionButton : ActionButton!
    var allPackagesListArr = [InTransitProductsListArr]()
    var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.register(InTransitTableViewCell.nib(), forCellReuseIdentifier: InTransitTableViewCell.identifier)
        tableview.estimatedRowHeight = tableview.rowHeight
        tableview.rowHeight = UITableView.automaticDimension
        DispatchQueue.main.async {
             self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
            self.tableview.addSubview(self.refreshControl)
            self.tableview.refreshControl = self.refreshControl
        }
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.reloadData()
        basicTableView.isHidden = false
        emptyBasicView.isHidden = true
        floatButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        tableview.delegate = self
//        tableview.dataSource = self
        tableview.separatorStyle = .none
//        tableview.reloadData()

        getAllPackagesListApi()
    }

    @objc func refresh(sender:AnyObject){
        
        getAllPackagesListApi()
        tableview.reloadData()
        self.refreshControl.endRefreshing()
    }

    func floatButton(){
        let one = ActionButtonItem(title: "Book A New Package", image: #imageLiteral(resourceName: "FEb"))
        one.action = { item in
            
            self.floatBtnAction() }
        let two = ActionButtonItem(title: "Buy For Me", image: #imageLiteral(resourceName: "FEb"))
        two.action = { item in
            
            self.floatBtnAction1() }
        actionButton = ActionButton(attachedToView: self.view, items: [one,two,])
        actionButton.setTitle("+", forState: UIControl.State())
       // actionButton.backgroundColor = #colorLiteral(red: 0.08235294118, green: 0.5725490196, blue: 0.9019607843, alpha: 1)
        actionButton.backgroundColor = UIColor.red
        actionButton.action = { button in button.toggleMenu()}
    }

    @objc func floatBtnAction()
       {
          print("1")
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
       
       }
    @objc func floatBtnAction1()
    {
       print("2")
     self.tabBarController?.selectedIndex = 2
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookNewpackagesVC) as! BookNewpackagesVC
//        navigationController?.pushViewController(vc, animated: true)
    
    }
    //MARK:-  IB Actions

    
    @IBAction func bookNewPackagesAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BookaNewPackagesVC) as! BookaNewPackagesVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buyFormeAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(identifier: Identifiers.BuyForMeProductsViewController) as!  BuyForMeProductsViewController
//        vc.isFrom = "Buy"
//        navigationController?.pushViewController(vc, animated: true)
        
        self.tabBarController?.selectedIndex = 2

        
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        self.floatBtnAction()
        
    }
        
    @objc func onClickViewMoreBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.ViewMoreVC) as! ViewMoreVC
//            vc.productId = self.allPackagesListArr[sender.tag].productID ?? ""
//            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
}
//MARK:- Extensions
extension INTransitViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPackagesListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableview.dequeueReusableCell(withIdentifier: InTransitTableViewCell.identifier, for: indexPath) as! InTransitTableViewCell
        
        if allPackagesListArr.count > 0{
            
            cell.loadIntransitData(intransitData: allPackagesListArr[indexPath.row])
        }
        cell.viewMoreBtn.isHidden = true
        cell.viewMoreBtn.addTarget(self, action: #selector(onClickViewMoreBtn), for: .touchUpInside)
       cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
//MARK:-  API Services

extension INTransitViewController {
    
    func getAllPackagesListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:"InTransit",
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AllPackagesListUrl)")

            ApiHandler.getData(url: AppUrls.AllPackagesListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(InTransitProductsListModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.allPackagesListArr = loginData.data
                                print("Dict Data: \(String(describing: self.allPackagesListArr))")

                                if self.allPackagesListArr.count > 0 {
                                    self.tableview.isHidden = false
                                    self.basicTableView.isHidden = false
                                    self.emptyBasicView.isHidden = true

                                    DispatchQueue.main.async {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.reloadData()

                                    }
                                }else {
                                    self.basicTableView.isHidden = true
                                    self.emptyBasicView.isHidden = false

                                    self.tableview.isHidden = true
                                }
                                
                            }else {
                                self.hideActivityIndicator()
                                self.basicTableView.isHidden = true
                                self.emptyBasicView.isHidden = false

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()
                            self.basicTableView.isHidden = true
                            self.emptyBasicView.isHidden = false

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
//extension UIView {
//    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
//
//}
