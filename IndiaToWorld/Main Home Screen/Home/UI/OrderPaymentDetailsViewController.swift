//
//  OrderPaymentDetailsViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 17/05/21.
//

import UIKit
import Razorpay

class OrderPaymentDetailsViewController: UIViewController {
    
    @IBOutlet weak var planLbl: UILabel!
    
    @IBOutlet weak var orderVw: UIView!
    @IBOutlet weak var orderIDLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productTypeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var shippingPreferenceLbl: UILabel!
    @IBOutlet weak var packagePreferenceLbl: UILabel!
    @IBOutlet weak var packageSizeLbl: UILabel!
    @IBOutlet weak var packageWeightLbl: UILabel!
    @IBOutlet weak var shortStorageDaysLbl: UILabel!
    @IBOutlet weak var shippingCostLbl: UILabel!
    @IBOutlet weak var packagingCostLbl: UILabel!
    @IBOutlet weak var packageCostBtn: UIButton!
    @IBOutlet weak var packageWeightCostLbl: UILabel!
    @IBOutlet weak var shortStorageCostLbl: UILabel!
    @IBOutlet weak var grandTotalLbl: UILabel!
    @IBOutlet weak var proceedPaymentBtn: UIButton!
    
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var popViewHeight: NSLayoutConstraint! //128
    @IBOutlet weak var popTV: UITableView!
    
    let arrValue = [[String]]()
    var orderId = String()
    var productId = String()
    var paymentDetailsDict : PackageOrderPaymentDetailsDict!
    var singleOrderPaymentDetails:SingleOrderPaymentDetailsDict!
    var orderType = String()
    var razorpay: RazorpayCheckout!
    var transcationId = String()
    var paymentStatus = String()
    var toDayDate = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedText = NSMutableAttributedString(string: "Crown Plan")
        attributedText.addAttribute(.underlineStyle, value: NSNumber(value: 1), range: NSRange(location: 0, length: planLbl.text?.count ?? 0))
        planLbl.attributedText = attributedText
        setUpTableView()
        popView.isHidden = true
        popView.layer.borderColor = UIColor.gray.cgColor
        popView.layer.borderWidth = 1
        popView.layer.cornerRadius = 8
        
        razorpay = RazorpayCheckout.initWithKey("rzp_test_2dpgkg40hOPhTo", andDelegate: self)

        if orderType == "single"{
            
            getPaymentDetailsForSingleOrderApi()
        }else {
            
            getPaymentDetailsForPackagesApi()
        }

    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        toDayDate = formatter.string(from: Date())
        print("Current Date : \(toDayDate)")

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        orderVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
    }
    
    internal func showPaymentForm(){
        
        var amount = Int()

        if orderType == "single"{
            
            amount = singleOrderPaymentDetails.grandTotal
        }else {
            
            amount = paymentDetailsDict.packageDetails.grandTotal

        }
            let amount1 = Float(amount) * 100
               let amount2 = Int(amount1)
        let options: [String:Any] = [
                    "amount": amount2, //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "Total amount to be paid for shipping an order",
//                    "order_id": "order_DBJOWzybf0sJbb",
                    "image": "",
            "name": AppHelper.getFullName(),
                    "prefill": [
                        "contact": AppHelper.getMobileNo(),
                        "email": AppHelper.getEmailId()
                    ],
                    "theme": [
                        "color": "#012A93"
                      ]
                ]
        razorpay.open(options)
    }
    func setUpTableView() {
        popTV.delegate = self
        popTV.dataSource = self
        //popTV.register(POPTableViewCell.self, forCellReuseIdentifier: POPTableViewCell.identifier)
    }

    func loadDataForSingleOrder(data:SingleOrderPaymentDetailsDict){
        
        
        let arr = data.packageDetails
        
        if arr[0].bookingid == "" || arr[0].bookingid == nil{

            orderIDLbl.text = String(format: "OrderId:%@",  arr[0].orderID )

        }else {

            orderIDLbl.text = String(format: "OrderId:%@", arr[0].bookingid ?? "")

        }
        productNameLbl.text = arr[0].productName
        productTypeLbl.text = arr[0].productDelivery
        addressLbl.text = arr[0].address
        packageSizeLbl.text = arr[0].packagingSize
        packageWeightLbl.text = arr[0].packagingWeight
        shortStorageDaysLbl.text = arr[0].shortStorageDays
        shippingPreferenceLbl.text = data.shippingTitle
        packagePreferenceLbl.text = data.packageTitle
        planLbl.text = data.planName

        
        
        shippingCostLbl.text = data.shippingPrice
        packagingCostLbl.text = data.packagePrice
//        packageWeightCostLbl.text = arr[0].packagingWeightCost
        grandTotalLbl.text = String(format: "₹%@", String(data.grandTotal))
    }
    
    func loadDataForPackages(data:PackageOrderPaymentDetailsDict){
        
        let dict = data.packageDetails
        
        if dict.packageID != "" {

            orderIDLbl.text = String(format: "OrderId:%@",  dict.packageID )

        }
        productNameLbl.text = dict.packageTitle
        productTypeLbl.text = String(format: "%@ items", dict.totalItems )

        addressLbl.text = dict.address
//        packageSizeLbl.text = dict.packagingSize
//        packageWeightLbl.text = dict.packagingWeight
//        shortStorageDaysLbl.text = dict.shortStorageDays
        shippingPreferenceLbl.text = data.shippingTitle
        packagePreferenceLbl.text = data.packageTitle
        planLbl.text = data.planName

        
        
        shippingCostLbl.text = data.shippingPrice
        packagingCostLbl.text = data.packagePrice
//        packageWeightCostLbl.text = dict.packagingWeightCost
        grandTotalLbl.text = String(format: "₹%@", String(dict.grandTotal))
    }
    //MARK:-  Selector
    @IBAction func onTapPayments(_ sender: UIButton) {
        print("payment success")
        
        showPaymentForm()
        
    }

    @IBAction func onTapPackageCost(_ sender: UIButton) {
        
//        let camera = UIAction(title: "Camera", image: UIImage(systemName: "camera")) { _ in
//            print("Camera tapped")
//        }
//
//        let photo = UIAction(title: "Photo", image: UIImage(systemName: "camera")) { _ in
//            print("photo tapped")
//        }
//
//        let menu = UIMenu(title: "", children: [camera, photo])
//        sender.showsMenuAsPrimaryAction = true
//        sender.menu = menu
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            popView.isHidden = false
            
            popViewHeight.constant = CGFloat(25 * arrValue.count)
        }else{
            popView.isHidden = true
            
            popViewHeight.constant = 0
        }
    }
    
}

extension OrderPaymentDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = popTV.dequeueReusableCell(withIdentifier: POPTableViewCell.identifier, for: indexPath) as! POPTableViewCell
//        cell.itemsLbl.text = arrValue[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK:-  RazorpayPaymentCompletionProtocol

extension OrderPaymentDetailsViewController: RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        print(str)
        paymentStatus = "Failed"
         showAlertMessage(vc: self, titleStr: "", messageStr: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        print(payment_id)
        paymentStatus = "Success"
        transcationId = payment_id
        userPaymentStatusAPI()

    }
}


//MARK:-  API Services

extension OrderPaymentDetailsViewController {
    
    func getPaymentDetailsForSingleOrderApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:orderType,
                PARAMS.order_id:orderId
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.PaymentDetails)")

            ApiHandler.getData(url: AppUrls.PaymentDetails, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(SingleOrderPaymentDetailsModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.singleOrderPaymentDetails = loginData.data
                                print("Dict Data: \(String(describing: self.singleOrderPaymentDetails))")

                                self.loadDataForSingleOrder(data: self.singleOrderPaymentDetails)
                                
                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func getPaymentDetailsForPackagesApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.type:orderType,
                PARAMS.order_id:orderId
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.PaymentDetails)")

            ApiHandler.getData(url: AppUrls.PaymentDetails, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(PackageOrderPaymentDetailsModel.self, from: data){
                            if loginData.status == 200{
//                                let dataArr = loginData.data
                                self.paymentDetailsDict = loginData.data
                                print("Dict Data: \(String(describing: self.paymentDetailsDict))")

                                self.loadDataForPackages(data: self.paymentDetailsDict)
                                
                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func userPaymentStatusAPI(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.order_id: orderId,
                PARAMS.txn_id: transcationId,
                PARAMS.payment_status: paymentStatus,
                PARAMS.date_time:toDayDate
            ]
            
            ApiHandler.getJsonData(url: AppUrls.UserPaymentStatusUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            DispatchQueue.main.async {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                                vc.isFromOrderPayment = "Intransit"
                                vc.bookingId = data["order_id"] as? String ?? ""
                                self.navigationController?.pushViewController(vc, animated: true)

                            }
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
