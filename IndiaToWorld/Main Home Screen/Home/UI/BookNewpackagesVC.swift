//
//  BookNewpackagesVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 31/12/20.
//

import UIKit
protocol SegmentDelegate: class {
    func SegmentChange(index:Int)
}
class BookNewpackagesVC: UIViewController,SegmentDelegate{
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var BasicView: UIView!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(BookNewpackagesVC.currentPageChanged(notification:)), name: Notification.Name(rawValue: "currentPageChanged"), object: nil)

        navTitle(heading: NavTitle.Book_a_New_Package)
        ViewEmbedder.embed(withIdentifier: Identifiers.BookaNewPackagesVC,parent: self,container: self.BasicView){ vc in
            // do things when embed complete
            vc.isEditing = true
            // ReadyToBuyVC
        }
        segmentControl.selectedSegmentIndex = 0
        //segmentControl.setEnabled(false, forSegmentAt: 1)
        
    }
    
    @objc func currentPageChanged(notification: Notification) {

        SegmentChange(index: 1)
    }

    @IBAction func segmentControlAction(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0 {
            ViewEmbedder.embed(withIdentifier: Identifiers.BookaNewPackagesVC,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
        }else{
            ViewEmbedder.embed(withIdentifier: Identifiers.SelectedAddressViewController,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
        }
    }
    
    func SegmentChange(index:Int){
        print("ok")
        segmentControl.selectedSegmentIndex = index
        if segmentControl.selectedSegmentIndex == 0 {
            print("Select 0")
            ViewEmbedder.embed(withIdentifier: Identifiers.BookaNewPackagesVC,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
            
        }else{
            print("Select 1")
            ViewEmbedder.embed(withIdentifier: Identifiers.SelectedAddressViewController,parent: self,container: self.BasicView){ vc in
                // do things when embed complete
                vc.isEditing = true
                // ReadyToBuyVC
            }
            //            segmentControl.setEnabled(true, forSegmentAt: 1)
            //            segmentControl.setEnabled(false, forSegmentAt: 0)
        }
        
    }
    
    
    
}
