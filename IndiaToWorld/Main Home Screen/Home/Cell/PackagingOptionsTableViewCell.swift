//
//  PackagingOptionsTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 17/05/21.
//

import UIKit

class PackagingOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var packageImgVw: UIImageView!
    
    @IBOutlet weak var packageOptionLbl: UILabel!
    
    class var identifier:String {
        return "\(self)"
    }

    class var nibName:String {
        return "\(self)"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
