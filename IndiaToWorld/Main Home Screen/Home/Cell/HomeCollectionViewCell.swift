//
//  HomeCollectionViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 30/12/20.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var namelbl: SSPaddingLabel!
    
    class var nibName:String {
        return "\(self)"
    }
    class var identifier:String {
        return "\(self)"
    }

    override var isSelected: Bool {
         didSet {
             if isSelected { // Selected cell
                namelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                namelbl.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
            }
              else { // Normal cell
                namelbl.textColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
                namelbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
         }
     }
}
