//
//  InTransitTableViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 07/01/21.
//

import UIKit

class InTransitTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var dashedVw: UIView!
    @IBOutlet weak var addressDashedVw: UIView!
    @IBOutlet weak var orderLblVw: UIView!
    @IBOutlet weak var addressBgVw: UIView!
    @IBOutlet weak var orderIdLbl: UILabel!
    
    @IBOutlet weak var productNameLbl: UILabel!
    
    @IBOutlet weak var eCommercePlotNameLbl: UILabel!
    
    
    @IBOutlet weak var deliveredAddressLbl: UILabel!
    
    
    
    
    
    static let identifier = "InTransitTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "InTransitTableViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMoreBtn.layer.borderWidth = 1
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8470588235)

        drawDottedLine(start: CGPoint(x: addressDashedVw.bounds.minY, y: addressDashedVw.bounds.minX), end: CGPoint(x: addressDashedVw.bounds.minY, y: addressDashedVw.bounds.maxY), view: addressDashedVw)

        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        viewMoreBtn.layer.borderWidth = 1
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)

        drawDottedLine(start: CGPoint(x: addressDashedVw.bounds.minY, y: addressDashedVw.bounds.minX), end: CGPoint(x: addressDashedVw.bounds.minY, y: addressDashedVw.bounds.maxY), view: addressDashedVw)

        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        addressBgVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        viewMoreBtn.layer.borderWidth = 1
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)

        drawDottedLine(start: CGPoint(x: addressDashedVw.bounds.minY, y: addressDashedVw.bounds.minX), end: CGPoint(x: addressDashedVw.bounds.minY, y: addressDashedVw.bounds.maxY), view: addressDashedVw)

        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
    }

    func loadIntransitData(intransitData: InTransitProductsListArr) {
        
        if intransitData.packageOrderID == nil {
            
            if intransitData.bookingid != "" {
                
                orderIdLbl.text = String(format: "OrderId:%@", intransitData.bookingid ?? "")

            }else if intransitData.orderID != ""{
                
                orderIdLbl.text = String(format: "OrderId:%@", intransitData.orderID ?? "")

            }

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", intransitData.packageOrderID ?? "")

        }
        
        if intransitData.packageTitle == nil{
            productNameLbl.text = intransitData.productName
        }else{
        productNameLbl.text = intransitData.packageTitle
        }
       
        if intransitData.totalItems == nil{
            
            if intransitData.productDelivery == ""{
                eCommercePlotNameLbl.text = "India To World"

            }else {
                eCommercePlotNameLbl.text = intransitData.productDelivery

            }
        }else{
            eCommercePlotNameLbl.text = String(format: "%@ items", intransitData.totalItems ?? "")
        }
        deliveredAddressLbl.text = intransitData.address
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
