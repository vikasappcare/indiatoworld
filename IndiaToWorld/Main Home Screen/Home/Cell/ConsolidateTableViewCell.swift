//
//  ConsolidateTableViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 06/01/21.
//

import UIKit

class ConsolidateTableViewCell: UITableViewCell {
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var dashedVw: UIView!
    @IBOutlet weak var orderLblBgVw: UIView!
    @IBOutlet weak var bgvIew: CCardView!
    @IBOutlet weak var addressBgVw: UIView!
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var productImgVw: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDescriptionLbl: UILabel!
    @IBOutlet weak var orderStatusBtn: UIButton!
    @IBOutlet weak var bookingTypeLbl: UILabel!
    @IBOutlet weak var bookingDateLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var checkBoxBtn: UIButton!
    
    @IBOutlet weak var checkBoxImgVw: UIImageView!
    
    
    static let identifier = "ConsolidateTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "ConsolidateTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgvIew.layer.borderWidth = 0.3
        bgvIew.layer.cornerRadius = 10
        bgvIew.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)

    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        bgvIew.layer.borderWidth = 0.3
        bgvIew.layer.cornerRadius = 10
        bgvIew.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        addressBgVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        bgvIew.layer.borderWidth = 0.3
        bgvIew.layer.cornerRadius = 10
        bgvIew.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        addressBgVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }

    func loadConsolidateData(consolidateItem:ConsolidateProductsListArr){
        
        if consolidateItem.orderID == "" {
            
            orderIdLbl.text = String(format: "OrderId:%@", consolidateItem.bookingid ?? "")

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", consolidateItem.orderID ?? "")

        }

        if consolidateItem.bookingDate == "" {
            
            bookingDateLbl.text = consolidateItem.expectedDate

        }else {
            
            bookingDateLbl.text = consolidateItem.bookingDate

        }
        
        if consolidateItem.ecomercePlotform == "" {
            
            bookingTypeLbl.text = consolidateItem.type

        }else {
            bookingTypeLbl.text = consolidateItem.ecomercePlotform
        }
        
        
        if consolidateItem.productDelivery == "" {
            
            productDescriptionLbl.text = consolidateItem.productDiscription

        }else {
            productDescriptionLbl.text = consolidateItem.productDelivery
        }
        productNameLbl.text = consolidateItem.productName
        orderStatusBtn.setTitle(consolidateItem.type, for: .normal)
//            addressLbl.text = ""
        addressLbl.text = consolidateItem.address


    }
    
    func loadOrderHistoryData(historyItem:OrderHistoryArr){
        
        if historyItem.orderID == "" {
            
            orderIdLbl.text = String(format: "OrderId:%@", historyItem.bookingid)

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", historyItem.orderID)

        }

        if historyItem.bookingDate == "" {
            
            bookingDateLbl.text = historyItem.expectedDate

        }else {
            
            bookingDateLbl.text = historyItem.bookingDate

        }
        
        if historyItem.ecomercePlotform == "" {
            
            bookingTypeLbl.text = historyItem.type

        }else {
            bookingTypeLbl.text = historyItem.ecomercePlotform
        }
        if historyItem.type == "buyforme" {
            productNameLbl.text = historyItem.productName
            productDescriptionLbl.text = historyItem.productDiscription
            orderStatusBtn.setTitle(historyItem.type, for: .normal)
//            addressLbl.text = ""
            addressLbl.text = historyItem.address

        }else {
            productNameLbl.text = historyItem.productName
            productDescriptionLbl.text = historyItem.productDelivery
            orderStatusBtn.setTitle(historyItem.type, for: .normal)
//            addressLbl.text = ""
            addressLbl.text = historyItem.address

        }

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
