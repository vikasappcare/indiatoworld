//
//  PreferencesTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 16/06/21.
//

import UIKit

class PreferencesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemLbl: UILabel!
    
    class var identifier: String {
        
        return "\(self)"
    }
    
    class var nibName: String {
        
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
