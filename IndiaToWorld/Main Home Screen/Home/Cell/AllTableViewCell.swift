//
//  AllTableViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 30/12/20.
//

import UIKit

class AllTableViewCell: UITableViewCell {
    
    static let identifier = "AllTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "AllTableViewCell", bundle: nil)
    }

    @IBOutlet weak var dashedVw: UIView!
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var chatVw: UIView!
    @IBOutlet weak var orderLblBgVw: UIView!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var productImgVw: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var bookingTypeLbl: UILabel!
    @IBOutlet weak var bookingDateLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var chatNowBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productImgVw.layer.cornerRadius = 10
        backGroundView.layer.cornerRadius = 15
        backGroundView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        backGroundView.layer.borderWidth = 0.5
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
        backGroundView.layer.masksToBounds = true

        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        chatVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        productImgVw.layer.cornerRadius = 10
        backGroundView.layer.cornerRadius = 15
        backGroundView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        backGroundView.layer.borderWidth = 0.5

        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        chatVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        productImgVw.layer.cornerRadius = 10
        backGroundView.layer.cornerRadius = 15
        backGroundView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        backGroundView.layer.borderWidth = 0.5
//        backGroundView.layer.masksToBounds = true
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        chatVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }
    
    func loadPackageList(packageData: AllPackagesArr){
        
        if packageData.orderID == "" {
            
            orderIdLbl.text = String(format: "OrderId:%@", packageData.bookingid)

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", packageData.orderID)

        }
        
        if packageData.bookingDate == "" {
            
            bookingDateLbl.text = packageData.expectedDate

        }else {
            
            bookingDateLbl.text = packageData.bookingDate

        }
        
        if packageData.type == "buyforme" {
            
            bookingTypeLbl.text = "Buy For Me"

        }else {
            bookingTypeLbl.text = packageData.type

        }

        productNameLbl.text = packageData.productName
        addressLbl.text = packageData.address

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
