//
//  POPTableViewCell.swift
//  Screens
//
//  Created by apple on 17/05/21.
//

import UIKit

class POPTableViewCell: UITableViewCell {

    static var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var itemsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
