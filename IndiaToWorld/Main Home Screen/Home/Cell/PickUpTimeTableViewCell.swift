//
//  PickUpTimeTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 06/01/21.
//

import UIKit

class PickUpTimeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var pickUpTimeLbl: UILabel!
    
    @IBOutlet weak var pickUpTimeImgVw: UIImageView!
    
    
    class var identifier:String {
        return "\(self)"
    }

    class var nibName:String {
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
