//
//  ConsolidateListProductTableViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 06/01/21.
//

import UIKit

class ConsolidateListProductTableViewCell: UITableViewCell {


    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var packageNameLbl: UILabel!
    @IBOutlet weak var packageDescriptionLbl: UILabel!
    @IBOutlet weak var planNameBtn: UIButton!
    @IBOutlet weak var dashedVw: UIView!
    @IBOutlet weak var productsListTblVw: UITableView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var ordersVw: UIView!
    @IBOutlet weak var addressVw: UIView!
    @IBOutlet weak var productListTblVwDynamicHeight: NSLayoutConstraint!
    
    @IBOutlet weak var packageMenuBtn: UIButton!
    @IBOutlet weak var packageDeleteBtn: UIButton!
    @IBOutlet weak var packageEditBtn: UIButton!
    @IBOutlet weak var popUpVw: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        popUpVw.layer.cornerRadius = 5
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        ordersVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        addressVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        popUpVw.layer.cornerRadius = 5
        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        ordersVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        addressVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }

    func loadPackageData(package: ConsolidatePackageListArr) {
        
        packageNameLbl.text = package.packageTitle //String(format: "Package %@", package.packageID)
        packageDescriptionLbl.text = String(format: "%@ items", package.totalItems)
        addressLbl.text = package.address
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
