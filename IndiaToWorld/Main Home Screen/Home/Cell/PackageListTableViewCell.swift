//
//  PackageListTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 10/02/21.
//

import UIKit

class PackageListTableViewCell: UITableViewCell {

    @IBOutlet weak var productNameLbl: UILabel!
   
    @IBOutlet weak var productRemoveBtn: UIButton!
    class var identifier: String {
        return "\(self)"
    }
    
    class var nibName: String {
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadProductData(productData: Order){
        
        productNameLbl.text = productData.productName
    }
    func loadOrderProductData(productData: PaymentsOrder){
        
        productNameLbl.text = productData.productName
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
