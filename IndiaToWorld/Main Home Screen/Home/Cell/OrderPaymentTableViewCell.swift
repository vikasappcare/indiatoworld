//
//  OrderPaymentTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 17/05/21.
//

import UIKit

class OrderPaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var orderBgVw: UIView!
    
    @IBOutlet weak var orderIdLbl: UILabel!
    
    @IBOutlet weak var productOrPackageNameLbl: UILabel!
    
    @IBOutlet weak var ecommerceOrItemsCountLbl: UILabel!
    
    
    @IBOutlet weak var payNowBtn: UIButton!
    
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var paymentDetailsStatusLbl: UILabel!
    @IBOutlet weak var paymentDetailsVw: UIView!
    
    @IBOutlet weak var ordersTblVw: UITableView!
    
    @IBOutlet weak var ordersTblVwDynamicHeight: NSLayoutConstraint!
    
    class var identifier: String {
        
        return "\(self)"
    }
    
    class var nibName: String {
        
        return "\(self)"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        orderBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        paymentDetailsVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }
    
    func loadOrderPaymentsList(orderData:OrderPaymentsArr){
        
        
        if orderData.packageOrderID == nil {
            
            if orderData.bookingid == "" || orderData.bookingid == nil{

                orderIdLbl.text = String(format: "OrderId:%@", orderData.orderID ?? "")

            }else if orderData.orderID == "" || orderData.orderID == nil{

                orderIdLbl.text = String(format: "OrderId:%@", orderData.bookingid ?? "")

            }
        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", orderData.packageOrderID ?? "")

        }
        
        if orderData.packageTitle == nil{
            productOrPackageNameLbl.text = orderData.productName
        }else{
            productOrPackageNameLbl.text = orderData.packageTitle
        }
       
        if orderData.totalItems == nil{
            
            if orderData.productDelivery == "" {
                
                ecommerceOrItemsCountLbl.text = "India to World"
                
            }else {
            ecommerceOrItemsCountLbl.text = orderData.productDelivery
            }
        }else{
            ecommerceOrItemsCountLbl.text = String(format: "%@ items", orderData.totalItems ?? "")
        }
        addressLbl.text = orderData.address
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
