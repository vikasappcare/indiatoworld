//
//  ReadyToSendTableViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 05/01/21.
//

import UIKit

class ReadyToSendTableViewCell: UITableViewCell {
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var dashedVw: UIView!
    @IBOutlet weak var orderLblBgVw: UIView!
    @IBOutlet weak var addressBgVw: UIView!
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var productImgVw: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDeliveryLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var checkBoxImgVw: UIImageView!
    
    static let identifier = "ReadyToSendTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "ReadyToSendTableViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
//        drawDottedLine(start: CGPoint(x: dashedVw.bounds.minX, y: dashedVw.bounds.minY), end: CGPoint(x: dashedVw.bounds.maxX, y: dashedVw.bounds.minY), view: dashedVw)
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)

    }
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        addressBgVw.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        viewMoreBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        viewMoreBtn.layer.borderWidth = 1
        orderLblBgVw.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)

    }
    func loadPackageList(packageData: AllPackagesArr){
        
        if packageData.orderID == "" {
            
            orderIdLbl.text = String(format: "OrderId:%@", packageData.bookingid)

        }else {
            
            orderIdLbl.text = String(format: "OrderId:%@", packageData.orderID)

        }

        productNameLbl.text = packageData.productName
        
        if packageData.productDelivery == "" {
            
            productDeliveryLbl.text = "India to World"

        }else {
        productDeliveryLbl.text = packageData.productDelivery
        }
//        addressLbl.text = packageData.address

//        productNameLbl.text = packageData.productName
//        let chosenImgUrl = packageData.invoice
//        let chosenImgUrlString = chosenImgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        productImgVw.kf.setImage(with: URL(string: chosenImgUrlString ?? ""), placeholder: UIImage(named: "incomingPackages"), options: nil, progressBlock: nil)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
