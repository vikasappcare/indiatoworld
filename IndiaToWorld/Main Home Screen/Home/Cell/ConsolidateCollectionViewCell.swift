//
//  ConsolidateCollectionViewCell.swift
//  IndiaToWorld
//
//  Created by Appcare on 06/01/21.
//

import UIKit

class ConsolidateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headinglbl: SSPaddingLabel!
    
    override var isSelected: Bool {
         didSet {
             if isSelected { // Selected cell
                headinglbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               headinglbl.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
            }
              else { // Normal cell
               headinglbl.textColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
               headinglbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
         }
     }
}
