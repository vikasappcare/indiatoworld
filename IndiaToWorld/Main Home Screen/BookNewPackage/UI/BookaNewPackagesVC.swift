//
//  BookaNewPackagesVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 04/01/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class BookaNewPackagesVC: UIViewController {
    
    var delegate: SegmentDelegate?
    var imgdata = Data()
    var expDate = String()
    var imageData1 = [Data]()
    
    
    @IBOutlet weak var productDeliveryTF: CustomTF!
    @IBOutlet weak var productNameTF: CustomTF!
    @IBOutlet weak var eCommerceNameTF: CustomTF!
    @IBOutlet weak var productTypeTF: CustomTF!
    @IBOutlet weak var ecpDeliveryDateTF: CustomTF!
    @IBOutlet weak var invoiceUploadTF: CustomTF!
    @IBOutlet weak var invoiceChangeBtn: UIButton!
    
    @IBOutlet weak var bgBtn: UIButton!
    
    @IBOutlet weak var popUpVw: UIView!
    
    @IBOutlet weak var doneBtn: CustomButton!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.invoiceChangeBtn.setTitle("Upload", for: .normal)
        popUpVw.isHidden = true
        bgBtn.isHidden = true
        navTitle(heading: NavTitle.Book_a_New_Package)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        popUpVw.roundCorners(corners: [.topLeft,.topRight], radius: 30)
    }
    
    func pickDate() {
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-dd-MM"
        let selectedTime = timeFormatter.string(from: datePicker.date)
        print("BEGIN:India2world ====> Current Time : \(selectedTime)")
        
        expDate = selectedTime
    }
    
    @IBAction func onClickExpDateBtn(_ sender: UIButton) {
        
        popUpVw.isHidden = false
        bgBtn.isHidden = false
        
        
    }
    
    @IBAction func onClickBgBtn(_ sender: UIButton) {
        
        popUpVw.isHidden = true
        bgBtn.isHidden = true
        
    }
    @IBAction func onClickDoneBtn(_ sender: UIButton) {
        
        popUpVw.isHidden = true
        bgBtn.isHidden = true
        ecpDeliveryDateTF.text = expDate
        
    }
    @IBAction func onClickDatePicker(_ sender: UIDatePicker) {
        
        pickDate()
    }
    
    @IBAction func onClickNextBtn(_ sender: UIButton) {
        
        
        self.productDeliveryTF.text = self.productDeliveryTF.text?.trimmingCharacters(in: .whitespaces)
        self.productNameTF.text = self.productNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.eCommerceNameTF.text = self.eCommerceNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.productTypeTF.text = self.productTypeTF.text?.trimmingCharacters(in: .whitespaces)
        self.ecpDeliveryDateTF.text = self.ecpDeliveryDateTF.text?.trimmingCharacters(in: .whitespaces)
        self.invoiceUploadTF.text = self.invoiceUploadTF.text?.trimmingCharacters(in: .whitespaces)
        
        if productDeliveryTF.hasText {
            
            if productNameTF.hasText {
                
                if eCommerceNameTF.hasText {
                    
                    if productTypeTF.hasText {
                        if ecpDeliveryDateTF.hasText {
                            
                            if imgdata.count > 0 {
                                
                                
                                if Reachability.isConnectedToNetwork(){
                                    self.uploadPicDocumentforUser(data:imgdata)
                                }else{
                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
                                    
                                }

//                                NotificationCenter.default.post(name: Notification.Name(rawValue: "currentPageChanged"), object: 1)
//
//                                ISFROMBUYFORME = false
//                                BOOKIN_DATA_DICT = ["product_delivery":productDeliveryTF.text ?? "","ecommerce_plotform":eCommerceNameTF.text ?? "","product_type":productTypeTF.text ?? "","product_name":productNameTF.text ?? "","exp_date":ecpDeliveryDateTF.text ?? "","invoice_image":imgdata]
                            } else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.uploadInvoiceImg)
                            }
                            
                        } else {
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterExpDeliveryDate)
                        }
                        
                    } else {
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.productTypeTF)
                    }
                    
                } else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterEcommerceType)
                }
                
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterProductName)
            }
            
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.enterProductDeliveryType)
        }
        
        
        //        delegate?.updateSegIndx(i: 1)
    }
    
    @IBAction func onClicknvoiceChangeBtn(_ sender: UIButton) {
        
        ImagePickerManager().pickImage(self) { (img) in
            let Profileimage = img
            let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
            self.invoiceUploadTF.text = "\(Date().timeIntervalSince1970).jpeg"
            self.invoiceChangeBtn.setTitle("Change", for: .normal)
            self.imgdata = imgData
            print(img, "image print")
        }
        
    }
    
}
extension BookaNewPackagesVC {
    
    func uploadPicDocumentforUser(data:Data)  {
        
        self.imageData1.append(data)
        let url = AppUrls.BookForMeUrl
        let parameters: [String:Any] = [PARAMS.uid:AppHelper.getUserId(),
                                        PARAMS.ecomerce_plotform:eCommerceNameTF.text ?? "",
                                        PARAMS.product_type:productTypeTF.text ?? "",
                                        PARAMS.product_delivery:productDeliveryTF.text ?? "",
                                        PARAMS.product_quantity:"1",
                                        PARAMS.product_name:productNameTF.text ?? "",
                                        PARAMS.address_id:"",
                                        PARAMS.expected_date:ecpDeliveryDateTF.text ?? ""
        ]
        let headers: HTTPHeaders = [
            HEADER.API_KEY:HEADER.API_KEY_VALUE
        ]
        UploadDocumentImage(isUser: true, endUrl: url, imageData: imageData1, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
        print("url",url)
        print("image",imageData1)
        print(parameters,"parameters")
        
    }
    func UploadDocumentImage(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        if isLoader {
            Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
        }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            //               if let data = [imageData]{
            //                   multipartFormData.append(data, withName: "images[]", fileName: "\(Date.init().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            //                   // multipartFormData.append(data, withName: "profilepic[]", fileName: "imageNew.jpeg", mimeType: "image/jpeg")
            //               }
            for imagesData in self.imageData1 {
                multipartFormData.append(imagesData, withName: "invoice", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
        },
        to: endUrl, method: .post,headers: headers )
        .responseJSON(completionHandler: { (response) in
            Indicator.shared().hideIndicator(vc: vc)
            if let err = response.error{
                Indicator.shared().hideIndicator(vc: vc)
                print(err)
                onError?(err)
                return
            }
            let json = response.data
            if (json != nil)
            {
                let jsonObject = JSON(json!)
                print(jsonObject)
                
                if jsonObject["status"].intValue == 200 {
                    
                    DispatchQueue.main.async {
                        //                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                        showAlertWithOkAction(self, title: ALERT_TITLE, message: jsonObject["message"].stringValue, buttonTitle: "OK") {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                            vc.bookingId = jsonObject["data"].stringValue
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                        
                    }
                }else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                    
                }
                
            }
        })
    }
}
