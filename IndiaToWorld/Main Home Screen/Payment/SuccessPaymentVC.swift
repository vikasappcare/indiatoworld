//
//  SuccessPaymentVC.swift
//  IndiaToWorld
//
//  Created by Appcare on 05/01/21.
//

import UIKit

class SuccessPaymentVC: UIViewController {

    //MARK:-  IB Outlets

    @IBOutlet weak var headinglbl: UILabel!
    @IBOutlet weak var checkYourPackageAction: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: NavTitle.Success)
        headinglbl.highlight(searchedText:  "In Transit")
    }

    //MARK:-  IB Actions
    
    @IBAction func CheckPckAgeAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: Identifiers.ConsolidateVC) as! ConsolidateVC
        navigationController?.pushViewController(vc, animated: true)
    }
}
