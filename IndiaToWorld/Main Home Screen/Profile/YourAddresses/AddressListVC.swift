//
//  AddressListVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit
class AddressListVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteAddrBtn: UIButton!
    var selectedcell = 0
    var selectedAddressId = ""
    var adrressBtnSelected = false
    var getaddressListModelInfo : getaddressListModel?
    var DeleteAddressModelInfo : DeleteAddressModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: NavTitle.Your_Address)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        deleteAddrBtn.layer.borderColor = #colorLiteral(red: 0.2324627638, green: 0.4635577202, blue: 1, alpha: 1)
        getaddressListRequest()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getaddressListRequest()
    }
    //MARK:- Selector
    //Delete Address
    @IBAction func deleteAddrAction(_ sender: Any) {
        deleteAddress()
    }
    
    //Add new Address
    @IBAction func addNewAddrAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.NewAddressVC) as! NewAddressVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:_DeleteAdd
    func deleteAddress(){
        let alertController = UIAlertController(
            title: "Alert",
            message: "Would you like to delete address?",
            preferredStyle: UIAlertController.Style.alert
        )
        
        let cancelAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            // ...
            self.deleteAddressRequest()
        }
        
        let okayAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // ...
        }
        
        alertController.addAction(okayAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
            // ...
        }
    }
    
}
extension AddressListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getaddressListModelInfo?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressListTVCell.identifier, for: indexPath) as! AddressListTVCell
        cell.selectionStyle = .none
        cell.addrTytleLbl.text = getaddressListModelInfo?.data?[indexPath.row].addressType
        cell.fullAddrLbl.text = getaddressListModelInfo?.data?[indexPath.row].address
        cell.selectedBtn.tag = indexPath.row
        if getaddressListModelInfo?.data?[indexPath.row].addressType == "office"{
            cell.addrTypeImg.image = #imageLiteral(resourceName: "YourAddresses")
        }else{
            cell.addrTypeImg.image = #imageLiteral(resourceName: "Home")
        }
        if indexPath.item == selectedcell {
            cell.selectedBtn.isHidden = false
            cell.selectedBtn.setImage(#imageLiteral(resourceName: "SelcetedIcon"), for: .normal)
            print("index",getaddressListModelInfo?.data?[selectedcell].id ?? "")
            selectedAddressId = getaddressListModelInfo?.data?[selectedcell].id ?? ""
            cell.backGroundView.backgroundColor = #colorLiteral(red: 0.7725490196, green: 0.8156862745, blue: 0.9568627451, alpha: 0.5261048576)
        }else{
            cell.backGroundView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.selectedBtn.isHidden = true
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedcell = indexPath.row
        tableView.reloadData()
    }
}
//MARK:-  API Services

extension AddressListVC {
    
    func getaddressListRequest(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.GetaddressList)")
            
            ApiHandler.getData(url: AppUrls.GetaddressList, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { [self] (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let AddressListData = try? JSONDecoder().decode(getaddressListModel.self, from: data){
                            if AddressListData.status == 200{
                                let dataArr = AddressListData
                                self.getaddressListModelInfo = dataArr
                                DispatchQueue.main.async {
                                    tableView.reloadData()
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: AddressListData.message ?? "")
                            }
                            
                        }else{
                            
//                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func deleteAddressRequest(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
                PARAMS.id:selectedAddressId
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.Deleteaddress)")
            
            ApiHandler.getData(url: AppUrls.Deleteaddress, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { [self] (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let deleteaddressData = try? JSONDecoder().decode(DeleteAddressModel.self, from: data){
                            if deleteaddressData.status == 200{
                                let dataArr = deleteaddressData
                                self.DeleteAddressModelInfo = dataArr
                                showAlertWithOkAction(self, title: STATUS, message: self.DeleteAddressModelInfo?.message ?? "" , buttonTitle: "OK") {
                                    DispatchQueue.main.async {
                                        selectedcell = 0
                                        tableView.reloadData()
                                        getaddressListRequest()
                                    }
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: deleteaddressData.message)
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}

