//
//  SelectedAddressViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 21/01/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectedAddressViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAddrBtn: UIButton!
    @IBOutlet weak var confirmBookingBtn: UIButton!
    
    var selectedcell = 0
    var selectedAddressId = "1"
    var adrressBtnSelected = false
    var getaddressListModelInfo : getaddressListModel?
    var DeleteAddressModelInfo : DeleteAddressModel?
    var isFromConsolidate = Bool()
    
    var imageData1 = [Data]()
    var imgdata = Data()
    var orderedItemsId = [String]()
    var contryId = String()
    var iscontryId = String()
    //MARK:-
   // ProceedSelected
    var isFromProceed = Bool()
    var bookingPackageId = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: NavTitle.Your_Address)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        getaddressListRequest()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getaddressListRequest()
        
        confirmBookingBtn.setTitle("Confirm Consolidate", for: .normal)

     /*   if isFromConsolidate == true {
            
            confirmBookingBtn.setTitle("Confirm Consolidate", for: .normal)
        }else {
            confirmBookingBtn.setTitle("Confirm Booking", for: .normal)
        }*/
    }
    
   /* func uploadPicDocumentforUser(data:Data)  {
        
        self.imageData1.append(data)
        let url = AppUrls.BookForMeUrl
        let parameters: [String:Any] = [PARAMS.uid:AppHelper.getUserId(),
                                        PARAMS.ecomerce_plotform:BOOKIN_DATA_DICT["ecommerce_plotform"] ?? "",
                                        PARAMS.product_type:BOOKIN_DATA_DICT["product_type"] ?? "",
                                        PARAMS.product_delivery:BOOKIN_DATA_DICT["product_delivery"] ?? "",
                                        PARAMS.product_quantity:"1",
                                        PARAMS.product_name:BOOKIN_DATA_DICT["product_name"] ?? "",
                                        PARAMS.address_id:selectedAddressId,
                                        PARAMS.expected_date:BOOKIN_DATA_DICT["exp_date"] ?? ""
        ]
        let headers: HTTPHeaders = [
            HEADER.API_KEY:HEADER.API_KEY_VALUE
        ]
        UploadDocumentImage(isUser: true, endUrl: url, imageData: imageData1, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
        print("url",url)
        print("image",imageData1)
        print(parameters,"parameters")
        
    }
    func UploadDocumentImage(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        if isLoader {
            Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
        }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            //               if let data = [imageData]{
            //                   multipartFormData.append(data, withName: "images[]", fileName: "\(Date.init().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            //                   // multipartFormData.append(data, withName: "profilepic[]", fileName: "imageNew.jpeg", mimeType: "image/jpeg")
            //               }
            for imagesData in self.imageData1 {
                multipartFormData.append(imagesData, withName: "invoice", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
        },
        to: endUrl, method: .post,headers: headers )
        .responseJSON(completionHandler: { (response) in
            Indicator.shared().hideIndicator(vc: vc)
            if let err = response.error{
                Indicator.shared().hideIndicator(vc: vc)
                print(err)
                onError?(err)
                return
            }
            let json = response.data
            if (json != nil)
            {
                let jsonObject = JSON(json!)
                print(jsonObject)
                
                if jsonObject["status"].intValue == 200 {
                    
                    DispatchQueue.main.async {
                        //                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                        showAlertWithOkAction(self, title: ALERT_TITLE, message: jsonObject["message"].stringValue, buttonTitle: "OK") {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                            vc.bookingId = jsonObject["data"].stringValue
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                        
                    }
                }else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                    
                }
                
            }
        })
    }
    
    func uploadPicDocumentforBuyForMe(data:Data)  {
        
        self.imageData1.append(data)
        let url = AppUrls.BuyForMeUrl
        let parameters: [String:Any] = [PARAMS.uid:AppHelper.getUserId(),
                                        PARAMS.product_name:BUY_FOR_ME_DATA_DICT["product_name"] ?? "",
                                        PARAMS.product_discription:BUY_FOR_ME_DATA_DICT["product_details"] ?? "",
                                        PARAMS.product_link:BUY_FOR_ME_DATA_DICT["product_link"] ?? "",
                                        PARAMS.product_quantity:BUY_FOR_ME_DATA_DICT["product_quantity"] ?? "",
                                        PARAMS.additional_note:BUY_FOR_ME_DATA_DICT["additional_note"] ?? "",
                                        PARAMS.address_id:selectedAddressId,
                                        PARAMS.booking_date:BUY_FOR_ME_DATA_DICT["booking_date"] ?? ""
        ]
        let headers: HTTPHeaders = [
            HEADER.API_KEY:HEADER.API_KEY_VALUE
        ]
        UploadDocumentImageForBuyingProduct(isUser: true, endUrl: url, imageData: imageData1, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
        print("url",url)
        print("image",imageData1)
        print(parameters,"parameters")
        
    }
    func UploadDocumentImageForBuyingProduct(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        if isLoader {
            Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
        }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            //               if let data = [imageData]{
            //                   multipartFormData.append(data, withName: "images[]", fileName: "\(Date.init().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            //                   // multipartFormData.append(data, withName: "profilepic[]", fileName: "imageNew.jpeg", mimeType: "image/jpeg")
            //               }
            for imagesData in self.imageData1 {
                multipartFormData.append(imagesData, withName: "referance_images", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
        },
        to: endUrl, method: .post,headers: headers )
        .responseJSON(completionHandler: { (response) in
            Indicator.shared().hideIndicator(vc: vc)
            if let err = response.error{
                Indicator.shared().hideIndicator(vc: vc)
                print(err)
                onError?(err)
                return
            }
            let json = response.data
            if (json != nil)
            {
                let jsonObject = JSON(json!)
                print(jsonObject)
                if jsonObject["status"].intValue == 200 {
                    
                    DispatchQueue.main.async {
                        showAlertWithOkAction(self, title: ALERT_TITLE, message: jsonObject["message"].stringValue, buttonTitle: "OK") {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessBookingVC) as! SuccessBookingVC
                            vc.bookingId = jsonObject["data"].stringValue
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                        
                    }
                }else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                    
                }

            }
        })
    }
    */
    //MARK:-  IB Action
    //Delete Address
    @IBAction func onClickConfirmBookinBtn(_ sender: Any) {
        //        deleteAddress()
        
        if isFromProceed == true{
            let vc = storyboard?.instantiateViewController(identifier: Identifiers.PackagingAndShippingVC) as!  PackagingAndShippingVC
            vc.isFromProceed = true
            vc.bookingPackageId = bookingPackageId
            vc.singleSelectedAddress = selectedAddressId
            navigationController?.pushViewController(vc, animated: true)
        }
        
        
        if isFromConsolidate == true {
            
            //            let vc = storyboard?.instantiateViewController(identifier: Identifiers.ConsolidateListProductsVC) as!  ConsolidateListProductsVC
            //            navigationController?.pushViewController(vc, animated: true)
            createPackageApi()
            
        }else {
         /*
            if ISFROMBUYFORME == true {
                if Reachability.isConnectedToNetwork(){
                    self.uploadPicDocumentforBuyForMe(data:BUY_FOR_ME_DATA_DICT["ref_image"] as! Data)
                }else{
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
                    
                }
                
                
            }else {
                
                if Reachability.isConnectedToNetwork(){
                    self.uploadPicDocumentforUser(data:BOOKIN_DATA_DICT["invoice_image"] as! Data)
                }else{
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
                    
                }
                
            }*/
        }
    }
    
    //Add new Address
    @IBAction func addNewAddrAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.NewAddressVC) as! NewAddressVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:_DeleteAdd
    //    func deleteAddress(){
    //        let alertController = UIAlertController(
    //            title: "Alert",
    //            message: "Would you like to DeleteAddress?",
    //            preferredStyle: UIAlertController.Style.alert
    //        )
    //
    //        let cancelAction = UIAlertAction(title: " DeleteAddres", style: .destructive) { (action) in
    //            // ...
    //            self.deleteAddressRequest()
    //        }
    //
    //        let okayAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
    //            // ...
    //        }
    //
    //        alertController.addAction(okayAction)
    //        alertController.addAction(cancelAction)
    //
    //        self.present(alertController, animated: true) {
    //            // ...
    //        }
    //    }
    
}
extension SelectedAddressViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getaddressListModelInfo?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressListTVCell.identifier, for: indexPath) as! AddressListTVCell
        cell.selectionStyle = .none
        cell.addrTytleLbl.text = getaddressListModelInfo?.data?[indexPath.row].addressType
        cell.fullAddrLbl.text = getaddressListModelInfo?.data?[indexPath.row].address
        cell.selectedBtn.tag = indexPath.row
        if getaddressListModelInfo?.data?[indexPath.row].addressType == "office"{
            cell.addrTypeImg.image = #imageLiteral(resourceName: "YourAddresses")
        }else{
            cell.addrTypeImg.image = #imageLiteral(resourceName: "Home")
        }
        if indexPath.item == selectedcell {
            cell.selectedBtn.isHidden = false
            cell.selectedBtn.setImage(#imageLiteral(resourceName: "SelcetedIcon"), for: .normal)
            print("index",getaddressListModelInfo?.data?[selectedcell].id ?? "")
            selectedAddressId = getaddressListModelInfo?.data?[selectedcell].id ?? ""
            cell.backGroundView.backgroundColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 0.2)
        }else{
            cell.backGroundView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.selectedBtn.isHidden = true
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedcell = indexPath.row
        tableView.reloadData()
    }
}
//MARK:-  API Services

extension SelectedAddressViewController {
    
    func getaddressListRequest(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.GetaddressList)")
            
            ApiHandler.getData(url: AppUrls.GetaddressList, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { [self] (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let AddressListData = try? JSONDecoder().decode(getaddressListModel.self, from: data){
                            if AddressListData.status == 200{
                                let dataArr = AddressListData
                                self.getaddressListModelInfo = dataArr
                                DispatchQueue.main.async {
                                    tableView.reloadData()
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: AddressListData.message ?? "")
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func createPackageApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.country_id: contryId,
                PARAMS.order_ids: orderedItemsId,
                PARAMS.address_id: selectedAddressId
            ]
            
            ApiHandler.getJsonData(url: AppUrls.CreateConsolidatePackagesUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
               
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {
                            
                            let vc = self.storyboard?.instantiateViewController(identifier: Identifiers.ConsolidateListProductsVC) as!  ConsolidateListProductsVC
                            self.navigationController?.pushViewController(vc, animated: true)
                            
//                            DispatchQueue.main.async {
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.PackagingAndShippingVC) as! PackagingAndShippingVC
//                                vc.contryId = self.iscontryId
////                                vc.packagesIdsArr = selectedPackagesIdsArr
//                                self.navigationController?.pushViewController(vc, animated: true)
//
//                            }
                            
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
                print("params",parameters)
                print("url",AppUrls.CreateConsolidatePackagesUrl)
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}

