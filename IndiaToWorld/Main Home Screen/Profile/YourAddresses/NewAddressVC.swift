//
//  NewAddressVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit
import DropDown
import GoogleMaps
import CoreLocation
import DropDown


class NewAddressVC: UIViewController {
    @IBOutlet weak var addresstextVw: CustomTextView!
    @IBOutlet weak var countryTF: CustomTF!
    @IBOutlet weak var stateTF: CustomTF!
    @IBOutlet weak var addressnameTF: CustomTF!
    @IBOutlet weak var googleMapsView: GMSMapView!
    
    @IBOutlet weak var addressNameDropDownBtn: UIButton!
    
    @IBOutlet weak var postalCodeTF: CustomTF!
    //MARK:-Googlemaps
    private var locationManager:CLLocationManager?
    private var marker = GMSMarker()
    private var latValue:String?
    private var longValue:String?
    var addAddressModelInfo : addAddressModel?
    let addressNamesDropDown = DropDown()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.New_Address)
        self.addresstextVw.isUserInteractionEnabled = false
        getUserLocation()
        
        self.addressNamesDropDown.anchorView = self.addressnameTF
        self.addressNamesDropDown.bottomOffset = CGPoint(x: 0, y: self.addressnameTF.bounds.height+15)
        self.addressNamesDropDown.dataSource = ["Home","Work","Office","Others"]
        self.addressNamesDropDown.selectionAction = { [weak self] (index, item) in
            self?.addressnameTF.text = item
            
        }
    }

//MARK:-  IB Actions
    
    @IBAction func onClickAddressNameDropDownBtn(_ sender: UIButton) {
        
        addressNamesDropDown.show()
    }
    
    @IBAction func submitAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.AddressListVC) as! AddressListVC
//        navigationController?.pushViewController(vc, animated: true)
       if addressnameTF.text!.isEmpty{
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.addrName)
        }else if countryTF.text!.isEmpty{
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.country)
        }else if stateTF.text!.isEmpty{
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.state)
        }else{
            addAddressRequest()
        }
        
        
    }
    @IBAction func didTapOnMyLocation(_ sender: Any) {
        
        googleMapsView.clear()
        getUserLocation()
    }
    
}
extension NewAddressVC{
    //MARK:- getting location
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.requestAlwaysAuthorization()
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.allowsBackgroundLocationUpdates = true
        googleMapsView.isMyLocationEnabled = true
        
        googleMapsView.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.startUpdatingLocation()
        }
        
    }
    
    //MARK:-ADDING MARKER
    func addMarker(lat:Double, long:Double) {
        //Adding a marker to the view
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.map = googleMapsView
        let locCamera = GMSCameraPosition.camera(withLatitude: lat,
                                                 longitude: long,
                                                 zoom: 15)
        googleMapsView.camera = locCamera
        marker.isDraggable = true
        reverseGeocoding(marker: marker)
        marker.map = googleMapsView
    }
    //MARK:- Reverse GeoCoding
    func reverseGeocoding(marker: GMSMarker) {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { [weak self] response , error in
            guard let self = self else{return}
            
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                
                print("Response is = \(address)")
                print("Response is = \(lines)")
                self.addresstextVw.text = String(format: "%@,%@,%@", address.thoroughfare ?? "",address.subLocality ?? "",address.locality ?? "")

                self.countryTF.text = address.country
                self.stateTF.text = address.administrativeArea
                self.postalCodeTF.text = address.postalCode
                currentAddress = lines.joined(separator: "\n")
            }
            self.googleMapsView.camera = GMSCameraPosition(target: coordinate, zoom: 15)
            marker.title = currentAddress
            self.latValue = "\(marker.position.latitude)"
            self.longValue = "\(marker.position.longitude)"
            print(self.latValue!, self.longValue!, "pointer")
            print("current",currentAddress)
            // self.addressLbl.text = currentAddress
            marker.map = self.googleMapsView
        }
    }
}
//MARK:- Extensions
//location Delegate
extension NewAddressVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latValue = "\(locValue.latitude)"
        longValue = "\(locValue.longitude)"
        addMarker(lat: locValue.latitude, long: locValue.longitude)
        locationManager?.stopUpdatingLocation()
    }
    
}

//marker moving
extension NewAddressVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
        reverseGeocoding(marker: marker)
        print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
    }
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
}

//dismiss View on tap
extension NewAddressVC: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldReceive touch: UITouch) -> Bool {
        return (touch.view === self.view)
    }
}
//MARK:-  API Services

extension NewAddressVC {
    
    func addAddressRequest(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
                PARAMS.address_type:self.addressnameTF.text ?? "",
                PARAMS.addaddress:self.addresstextVw.text ?? "",
                PARAMS.country:self.countryTF.text ?? "",
                PARAMS.state:self.stateTF.text ?? "",
                PARAMS.addltd:self.latValue ?? "",
                PARAMS.addlng:self.longValue ?? "",
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.AddaddressUrl)")
            
            ApiHandler.getData(url: AppUrls.AddaddressUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let addressData = try? JSONDecoder().decode(addAddressModel.self, from: data){
                            if addressData.status == 200{
                                showAlertWithOkAction(self, title: STATUS, message: addressData.message , buttonTitle: "OK") {
                                    DispatchQueue.main.async {
                                        self.navigationController?.backToViewController(vc: AddressListVC.self)

                                    }
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: addressData.message)
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}

