//
//  Address.swift
//  IndiaToWorld
//
//  Created by Appcare on 19/01/21.
//

import Foundation
// MARK: - addAddressModel
struct addAddressModel: Codable {
    let status: Int
    let message: String
}
// MARK: - addAddressModel
struct DeleteAddressModel: Codable {
    let status: Int
    let message: String
}
// MARK: - getaddressListModel
struct getaddressListModel: Codable {
    let status: Int?
    let message: String?
    let data: [AddressListModelData]?
}
struct AddressListModelData: Codable {
    let id, uid, addressType, address: String?
    let state, country, ltd, lng: String?
    let cd, status: String?
    
    enum CodingKeys: String, CodingKey {
        case id, uid
        case addressType = "address_type"
        case address, state, country, ltd, lng, cd, status
    }
}
