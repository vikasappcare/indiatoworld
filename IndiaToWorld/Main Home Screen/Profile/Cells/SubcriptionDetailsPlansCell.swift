//
//  SubcriptionDetailsPlansCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 04/01/21.
//

import UIKit

class SubcriptionDetailsPlansCell: UICollectionViewCell {
    
    
    @IBOutlet weak var bgVw: CardView!
    @IBOutlet weak var planTitleLbl: UILabel!
    @IBOutlet weak var validityLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    class var identifier:String {
        return "\(self)"
    }

}
