//
//  AddressListTVCell.swift
//  IndiaToWorld
//
//  Created by Vikas on 17/12/20.
//

import UIKit
import Foundation
class AddressListTVCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var selectedBtn: UIButton!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var addrTypeImg: UIImageView!
    @IBOutlet weak var addrTytleLbl: UILabel!
    @IBOutlet weak var fullAddrLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()  
        // Initialization code
        backGroundView.layer.cornerRadius = 5
        backGroundView.layer.borderWidth = 1.5
        backGroundView.layer.borderColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
       // backGroundView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override var isSelected: Bool {
         didSet {
             if isSelected { // Selected cell
                backGroundView.layer.cornerRadius = 10
                backGroundView.layer.borderWidth = 1.5
                backGroundView.layer.borderColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
                backGroundView.backgroundColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
                
             } else { // Normal cell
                backGroundView.layer.cornerRadius = 10
                backGroundView.layer.borderWidth = 1.5
                backGroundView.layer.borderColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
               // backGroundView.backgroundColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
            }
         }
     }

}
