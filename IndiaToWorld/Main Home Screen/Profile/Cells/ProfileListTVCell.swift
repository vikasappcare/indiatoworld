//
//  ProfileListTVCell.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class ProfileListTVCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var imageList: UIImageView!
    @IBOutlet weak var nameList: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
