//
//  SubscriptionTVCell.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class SubscriptionTVCell: UITableViewCell {

    class var identifier:String {
        return "\(self)"
    }
    
    @IBOutlet weak var imageSub: UIImageView!
    @IBOutlet weak var planNameLbl: UILabel!
    @IBOutlet weak var planDetailsLbl: UILabel!
    @IBOutlet weak var viewInDetailBtn: UIButton!
    @IBOutlet weak var planSelectedCheckBoxBtn: UIButton!
    @IBOutlet weak var planSelectedCheckBoxImgVw: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
