//
//  SubscriptionPlanBenefitsTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 05/01/21.
//

import UIKit

class SubscriptionPlanBenefitsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var planBenefitsLbl: UILabel!
    
    class var identifier:String {
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
