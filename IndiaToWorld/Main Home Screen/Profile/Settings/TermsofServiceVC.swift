//
//  TermsofServiceVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit

class TermsofServiceVC: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    var privacyArr = [TermsOfUseArr]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Terms_of_Service)
        getTermsOfConditionsApi()
        
    }
    
    
}
//MARK:-  API Services

extension TermsofServiceVC {
    
    func getTermsOfConditionsApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.TermsAndConditionsUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(TermsOfUseModel.self, from: data){
                            if loginData.status == 200{
                                self.privacyArr = loginData.data
                                print(self.privacyArr)
                                
                                self.textView.text = self.privacyArr[0].discription
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
