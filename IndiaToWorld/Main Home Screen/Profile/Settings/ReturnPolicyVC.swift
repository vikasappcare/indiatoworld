//
//  ReturnPolicyVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit

class ReturnPolicyVC: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    var privacyArr = [PrivacyPolicyArr]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getReturnPolicyApi()
        navTitle(heading: NavTitle.Return_Policy)
    }
    
    
}
//MARK:-  API Services

extension ReturnPolicyVC {
    
    func getReturnPolicyApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.RefundPolicyUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(PrivacyPolicyModel.self, from: data){
                            if loginData.status == 200{
                                self.privacyArr = loginData.data
                                print(self.privacyArr)
                                
                                self.textView.text = self.privacyArr[0].discription
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
