//
//  PrivacyPolicy.swift
//  IndiaToWorld
//
//  Created by Bhasker on 20/01/21.
//

import Foundation
// MARK: - Welcome
struct PrivacyPolicyModel: Codable {
    let status: Int
    let message: String
    let data: [PrivacyPolicyArr]
}

// MARK: - Datum
struct PrivacyPolicyArr: Codable {
    let id, discription, sts, cd: String
}
struct TermsOfUseModel: Codable {
    let status: Int
    let message: String
    let data: [TermsOfUseArr]
}

// MARK: - Datum
struct TermsOfUseArr: Codable {
    let id, discription, status, cd: String
}
