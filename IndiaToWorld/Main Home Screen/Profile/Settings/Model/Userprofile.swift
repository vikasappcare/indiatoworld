//
//  Userprofile.swift
//  IndiaToWorld
//
//  Created by Appcare on 19/01/21.
//

import Foundation


struct updateprofileData: Codable {
    let status: Int
    let message: String
}
/*
// MARK: - Welcome
struct getUserProfileModel: Codable {
    let status: Int
    let message: String
    let data: [UserProfileData]?
}

// MARK: - Datum
struct UserProfileData: Codable {
    let id, firstName, uid, lastName: String
    let pPic: String
    let email, mobile, cCode, otpStatus: String
    let uotp, address2, address1, password: String
    let countryID, stateID, cityID, postalCode: String
    let userStatus, deviceID, deviceToken, deviceType: String
    let role, fromDate, toDate, userPlanID: String
    let chatLastID, cd, planTitle: String

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case uid
        case lastName = "last_name"
        case pPic = "p_pic"
        case email, mobile
        case cCode = "c_code"
        case otpStatus = "otp_status"
        case uotp
        case address2 = "address_2"
        case address1 = "address_1"
        case password
        case countryID = "country_id"
        case stateID = "state_id"
        case cityID = "city_id"
        case postalCode = "postal_code"
        case userStatus = "user_status"
        case deviceID = "device_id"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case role
        case fromDate = "from_date"
        case toDate = "to_date"
        case userPlanID = "user_plan_id"
        case chatLastID = "chat_last_id"
        case cd
        case planTitle = "plan_title"
    }
}
*/
// MARK: - Welcome
struct getUserProfileModel: Codable {
    let status: Int
    let message: String
    let data: [UserProfileData]?
}

// MARK: - Datum
struct UserProfileData: Codable {
    let id, firstName, uid, lastName: String
    let pPic, email, mobile, cCode: String
    let otpStatus, uotp: String?
    let address2, address1, password, countryID: String
    let stateID, cityID, postalCode, userStatus: String
    let deviceID, deviceToken, deviceType, role: String
    let fromDate, toDate, userPlanID, chatLastID: String
    let cd,planTitle: String

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case uid
        case lastName = "last_name"
        case pPic = "p_pic"
        case email, mobile
        case cCode = "c_code"
        case otpStatus = "otp_status"
        case uotp
        case address2 = "address_2"
        case address1 = "address_1"
        case password
        case countryID = "country_id"
        case stateID = "state_id"
        case cityID = "city_id"
        case postalCode = "postal_code"
        case userStatus = "user_status"
        case deviceID = "device_id"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case role
        case fromDate = "from_date"
        case toDate = "to_date"
        case userPlanID = "user_plan_id"
        case chatLastID = "chat_last_id"
        case cd
        case planTitle = "plan_title"

    }
}
