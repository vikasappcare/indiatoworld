//
//  FAQ.swift
//  IndiaToWorld
//
//  Created by Bhasker on 04/03/21.
//

import Foundation
// MARK: - Welcome
struct FAQModel: Codable {
    let status: Int
    let message: String
    let data: [FAQArr]
}

// MARK: - Datum
struct FAQArr: Codable {
    let id, question, answer, status: String
    let cd: String
}
