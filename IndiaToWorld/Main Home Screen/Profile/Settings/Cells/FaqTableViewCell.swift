//
//  FaqTableViewCell.swift
//  IndiaToWorld
//
//  Created by Bhasker on 04/03/21.
//

import UIKit

class FaqTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var answerLbl: UILabel!
    
    class var identifier: String {
        return "\(self)"
    }

    class var nibName: String {
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
