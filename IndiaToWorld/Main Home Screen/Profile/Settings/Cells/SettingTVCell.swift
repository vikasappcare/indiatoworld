//
//  SettingTVCell.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit

class SettingTVCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
