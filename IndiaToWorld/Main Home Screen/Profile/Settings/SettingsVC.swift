//
//  SettingsVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit

class SettingsVC: UIViewController {

    static var settingArray = ["Change Password", "Return Policy", "Privacy Policy", "About", "FAQ's", "Terms & Conditions"]
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Settings)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.layer.cornerRadius = 10
    }
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SettingsVC.settingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingTVCell.identifier, for: indexPath) as! SettingTVCell
        cell.nameLbl.text = SettingsVC.settingArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.ChangePswdVC) as! ChangePswdVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.ReturnPolicyVC) as! ReturnPolicyVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 2 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.PrivacyPoliciesVC) as! PrivacyPoliciesVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 3 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.AboutVC) as! AboutVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 4 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.FAQVC) as! FAQVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 5 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.TermsofServiceVC) as! TermsofServiceVC
            navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
}
