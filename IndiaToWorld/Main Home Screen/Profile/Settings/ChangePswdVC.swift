//
//  ChangePswdVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit

class ChangePswdVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var oldPswdTF: UITextField!
    @IBOutlet weak var newPswdTF: UITextField!
    @IBOutlet weak var confirmNewPswdTF: UITextField!
    @IBOutlet weak var oldPswdBtn: UIButton!
    @IBOutlet weak var newPswdBtn: UIButton!
    @IBOutlet weak var confirmPswdBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Change_Password)
    }
    
    //MARK:- selector
    //Password action
    @IBAction func passwordAction(_ sender: UIButton) {
        
        if sender.tag == 91 {
            
            oldPswdTF.isSecureTextEntry = false
            oldPswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            
        }else if sender.tag == 92 {
            
            newPswdTF.isSecureTextEntry = false
            newPswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            
        }else if sender.tag == 93 {
            
            confirmNewPswdTF.isSecureTextEntry = false
            confirmPswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            
        }
        
    }
    
    //forgotpswd
    @IBAction func forgotPswdAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.ForgotPasswordVC) as! ForgotPasswordVC
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //Save Pswd
    @IBAction func saveDetailsAction(_ sender: Any) {
        
        self.newPswdTF.text = self.newPswdTF.text?.trimmingCharacters(in: .whitespaces)
        self.confirmNewPswdTF.text = self.confirmNewPswdTF.text?.trimmingCharacters(in: .whitespaces)
        self.oldPswdTF.text = self.oldPswdTF.text?.trimmingCharacters(in: .whitespaces)

        if oldPswdTF.hasText {
            
            if newPswdTF.hasText{
                
                if confirmNewPswdTF.hasText{
                    
                    if newPswdTF.text == confirmNewPswdTF.text {
                        
                        userChangePasswordApi()

                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.samePswd)

                    }
                    
                } else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.confirmNewPswd)
                }

            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.newPswd)
            }
            
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.oldPswd)
        }
    }
}
//MARK:-  API Services

extension ChangePswdVC {
    
    func userChangePasswordApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                                            PARAMS.password:newPswdTF.text ?? "",
                                            PARAMS.cpassword:confirmNewPswdTF.text ?? "",
                                            PARAMS.curentpassword:oldPswdTF.text ?? "",
                                            PARAMS.uid:AppHelper.getUserId()
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.ChangePasswordUrl)")

            ApiHandler.getJsonData(url: AppUrls.ChangePasswordUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
