//
//  FAQVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 16/12/20.
//

import UIKit

class FAQVC: UIViewController {

    @IBOutlet weak var descriptionLbl: UILabel!
   
    @IBOutlet weak var faqTblVw: UITableView!
    var privacyArr = [FAQArr]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.FAQ)
        tableVwNibs()
        getFaqApi()
    }
    
    func tableVwNibs(){
        
        faqTblVw.register(UINib(nibName: FaqTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: FaqTableViewCell.identifier)
    }
    
}
extension FAQVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return privacyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: FaqTableViewCell.identifier, for: indexPath) as! FaqTableViewCell
        cell.selectionStyle = .none
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
//MARK:-  API Services

extension FAQVC {
    
    func getFaqApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.FaqsUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(FAQModel.self, from: data){
                            if loginData.status == 200{
                                self.privacyArr = loginData.data
                                print(self.privacyArr)
                                
                                if self.privacyArr.count > 0 {
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.faqTblVw.delegate = self
                                        self.faqTblVw.dataSource = self
                                        self.faqTblVw.reloadData()
                                        
                                    }
                                }
//                                self.descriptionLbl.text = self.privacyArr[0].discription
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
