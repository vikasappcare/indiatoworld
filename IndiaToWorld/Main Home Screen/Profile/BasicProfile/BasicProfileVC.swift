//
//  BasicProfileVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 15/12/20.
//

import UIKit
import Alamofire
import SwiftyJSON
class BasicProfileVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fNameTF: UITextField!
    @IBOutlet weak var lNameTF: UITextField!
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var phoneNoTF: UITextField!
    @IBOutlet weak var deliveryAddrTF: UITextField!
    @IBOutlet weak var membershipView: UIView!
    @IBOutlet weak var membershipLbl: UILabel!
    
    @IBOutlet weak var StartDateLbl: UILabel!
    
    @IBOutlet weak var planImgVw: UIImageView!
    @IBOutlet weak var endDateLbl: UILabel!
    
    @IBOutlet weak var membershipDynamicHeight: NSLayoutConstraint!
    
    var getUserProfileModelInfo : getUserProfileModel?
    var updateprofileDataInfo: updateprofileData?
    var imageData1 = [Data]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Basic_Profile)
        userDetailsRequest()
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        
    }
    
    //MARK:- Selector
    @IBAction func editImageAction(_ sender: Any) {
        
        ImagePickerManager().pickImage(self) { (img) in
            self.profileImage.image = img
            let Profileimage = img
                        let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
                        if Reachability.isConnectedToNetwork(){
                            self.uploadPicDocumentforUSer(data:imgData)
                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)

                        }
            print(img, "image print")
        }
        
    }
    
    @IBAction func saveDetailsAction(_ sender: Any) {
        
        
        self.fNameTF.text = self.fNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.lNameTF.text = self.lNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.emailIDTF.text = self.emailIDTF.text?.trimmingCharacters(in: .whitespaces)
        self.phoneNoTF.text = self.phoneNoTF.text?.trimmingCharacters(in: .whitespaces)
        self.deliveryAddrTF.text = self.deliveryAddrTF.text?.trimmingCharacters(in: .whitespaces)
        if fNameTF.hasText {
            if lNameTF.hasText {
                if emailIDTF.hasText {
                    
                    if AppHelper.isValidEmail(with: emailIDTF.text){
                        
                        if phoneNoTF.hasText {
                            
                            if deliveryAddrTF.hasText {
                                
                                userupdateProfileRequest()
                                
                            } else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.addrName)
                            }
                            
                        } else {
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.mobileNo)
                        }
                        
                    } else {
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailFormat)
                    }
                    
                } else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailID)
                }
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.lastName)
            }
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.firstName)
        }
    }
    
    
}
extension BasicProfileVC {
    
    func userDetailsRequest(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.getUserProfile)")
            
            ApiHandler.getData(url: AppUrls.getUserProfile, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { [self] (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let AddressListData = try? JSONDecoder().decode(getUserProfileModel.self, from: data){
                            if AddressListData.status == 200{
                                let dataArr = AddressListData
                                self.getUserProfileModelInfo = dataArr
                                DispatchQueue.main.async {
                                    userReponse()
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: AddressListData.message ?? "")
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    
    func userReponse(){
        if getUserProfileModelInfo?.data?[0].pPic != ""{
            let chosenImgUrl = getUserProfileModelInfo?.data?[0].pPic
                    let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                   self.profileImage.kf.setImage(with: URL(string: chosenImgUrlString ?? ""), placeholder: UIImage(named: "emptyIcon"), options: nil, progressBlock: nil)
        }else{
            // profileImage
        }

        fNameTF.text = getUserProfileModelInfo?.data?[0].firstName
        lNameTF.text = getUserProfileModelInfo?.data?[0].lastName
        emailIDTF.text = getUserProfileModelInfo?.data?[0].email
        phoneNoTF.text = getUserProfileModelInfo?.data?[0].mobile
        deliveryAddrTF.text = getUserProfileModelInfo?.data?[0].address1
        
        if getUserProfileModelInfo?.data?[0].fromDate == "" || getUserProfileModelInfo?.data?[0].toDate == "" || getUserProfileModelInfo?.data?[0].planTitle == ""
        {
            
            planImgVw.isHidden = true
            StartDateLbl.isHidden = true
            endDateLbl.isHidden = true

            membershipDynamicHeight.constant = 80
            membershipLbl.text = "You don't have any membership plan!! Please choose any plan to consolidate your packages.."

        }else {
            
            planImgVw.isHidden = false
            StartDateLbl.isHidden = false
            endDateLbl.isHidden = false
            membershipDynamicHeight.constant = 80
            membershipLbl.text = String(format: "Current Membership: %@", getUserProfileModelInfo?.data?[0].planTitle ?? "")
            StartDateLbl.text = String(format: "Start Date: %@", getUserProfileModelInfo?.data?[0].fromDate ?? "")
            endDateLbl.text = String(format: "End Date: %@", getUserProfileModelInfo?.data?[0].toDate ?? "")

            if getUserProfileModelInfo?.data?[0].planTitle == "silver" {

                planImgVw.image = UIImage(named: "DimPlan")

            }else if getUserProfileModelInfo?.data?[0].planTitle == "Gold" {

                planImgVw.image = UIImage(named: "GoldPlan")

            }else {
            planImgVw.image = UIImage(named: "CrownPlan")
            }

        }
    }
    
    
        
        func userupdateProfileRequest(){

            if Connectivity.isConnectedToInternet() {
                showActivityIndicator()
                let parameters: [String:Any] = [PARAMS.first_name:fNameTF.text ?? "",
                                                PARAMS.last_name:lNameTF.text ?? "",
                                                PARAMS.email:emailIDTF.text ?? "",
                                                PARAMS.mobile:phoneNoTF.text ?? "",
                                                PARAMS.address_1:deliveryAddrTF.text ?? "",
                                                PARAMS.uid:AppHelper.getUserId()
            ]
                print("Parameters:  \(parameters)")
                print("UrlString:  \(AppUrls.profileupdate)")

                ApiHandler.getData(url: AppUrls.profileupdate, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                    self.hideActivityIndicator()

                    if let data = data{
                        print(data)
                        do{

                            if let loginData = try? JSONDecoder().decode(updateprofileData.self, from: data){
                                if loginData.status == 200{

                                    self.updateprofileDataInfo = loginData
                                    showAlertWithOkAction(self, title: STATUS, message: loginData.message , buttonTitle: "OK") {
                                        DispatchQueue.main.async {
                                            self.userDetailsRequest()
                                        }
                                    }
                                }else {

                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)

                                }

                            }else{

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                            }
                        }
                    }else{
                        print("Error\(String(describing: error))")
                        self.hideActivityIndicator()
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                    }
                }

            }else {

                self.hideActivityIndicator()
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
            }
        }
    
    
    func uploadPicDocumentforUSer(data:Data)  {
          
            self.imageData1.append(data)
            let url = AppUrls.profile_photo_update
        let parameters: [String:Any] = [PARAMS.uid:AppHelper.getUserId()
                   ]
            let headers: HTTPHeaders = [
                HEADER.API_KEY:HEADER.API_KEY_VALUE
            ]
            UploadDocumentImage(isUser: true, endUrl: url, imageData: imageData1, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
            print("url",url)
            print("image",imageData1)
            print(parameters,"parameters")
            
        }
    func UploadDocumentImage(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
              if isLoader {
                  Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
              }
              AF.upload(multipartFormData: { multipartFormData in
                  for (key, value) in parameters {
                      if let temp = value as? String {
                          multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                      }
                      if let temp = value as? Int {
                          multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                      }
                      if let temp = value as? NSArray {
                          temp.forEach({ element in
                              let keyObj = key + "[]"
                              if let string = element as? String {
                                  multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                              } else
                                  if let num = element as? Int {
                                      let value = "\(num)"
                                      multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                              }
                          })
                      }
                  }
                  //               if let data = [imageData]{
                  //                   multipartFormData.append(data, withName: "images[]", fileName: "\(Date.init().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                  //                   // multipartFormData.append(data, withName: "profilepic[]", fileName: "imageNew.jpeg", mimeType: "image/jpeg")
                  //               }
                  for imagesData in self.imageData1 {
                      multipartFormData.append(imagesData, withName: "profilepic", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                  }
              },
                        to: endUrl, method: .post,headers: headers )
                  .responseJSON(completionHandler: { (response) in
                      Indicator.shared().hideIndicator(vc: vc)
                      if let err = response.error{
                          Indicator.shared().hideIndicator(vc: vc)
                          print(err)
                          onError?(err)
                          return
                      }
                      let json = response.data
                      if (json != nil)
                      {
                          let jsonObject = JSON(json!)
                          print(jsonObject)
                          DispatchQueue.main.async {
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr:jsonObject["message"].stringValue)
                          }
                      }
                  })
          }
    }

