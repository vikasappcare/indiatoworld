//
//  UserProfilePictureViewController.swift
//  IndiaToWorld
//
//  Created by Bhasker on 02/06/21.
//

import UIKit

class UserProfilePictureViewController: UIViewController {
    
    //MARK: -  Outlets
    
    @IBOutlet var scrollVw: UIScrollView!
    @IBOutlet var profileImgVw: UIImageView!
    
    var cancelBarBtnItem = UIBarButtonItem()
    
    var profilePicStr = String()

    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        setUpScrollVw()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpUI()
    }
    
    static func getInstance() -> UserProfilePictureViewController {

        let storyBoard = UIStoryboard(name: "UserProfilePicture", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: "UserProfilePictureViewController") as! UserProfilePictureViewController
    }
    
    func setUpUI() {
        
        
//        cancelBarBtnItem = UIBarButtonItem(image: UIImage(named: "cross"), style: .plain, target: self, action: #selector(self.onClickCancelBtn))
//
//        navigationItem.rightBarButtonItems = [cancelBarBtnItem]
        
        let rightButton = UIButton(type: UIButton.ButtonType.custom)
        rightButton.setTitle("Cancel", for: UIControl.State.normal)
        rightButton.setTitleColor(.black, for: UIControl.State.normal)
        rightButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        rightButton.addTarget(self, action: #selector(onClickCancelBtn), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = barButton

        let profileImgUrlString = profilePicStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        profileImgVw.kf.setImage(with: URL(string: profileImgUrlString ), placeholder: UIImage(named: "incomingPackages"), options: nil, progressBlock: nil)

//        profileImgVw.image = UIImage(named:"incomingPackages")

//        profileImgVw.sd_setImage(with: URL(string: profileImgUrlString), placeholderImage: UIImage(named: "placeholder1"))
    }
    
    func setUpScrollVw() {
        
        scrollVw.minimumZoomScale = 1.0
        scrollVw.maximumZoomScale = 6.0
        scrollVw.contentSize = profileImgVw.frame.size
        scrollVw.delegate = self
        scrollVw.showsVerticalScrollIndicator = false
        scrollVw.showsHorizontalScrollIndicator = true
        scrollVw.isScrollEnabled = true
    }
}

extension UserProfilePictureViewController {
    
    //MARK: - IB Actions
    
    @objc func onClickCancelBtn() {
        
        dismiss(animated: true, completion: nil)
    }
}

extension UserProfilePictureViewController : UIScrollViewDelegate {
    
    //MARK: - ScrollView Delegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return profileImgVw
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollVw.zoomScale == scrollVw.minimumZoomScale {
            
            //dismiss(animated: true, completion: nil)
        }
    }
    
}
