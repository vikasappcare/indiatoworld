//
//  wareHouseDetails.swift
//  IndiaToWorld
//
//  Created by Bhasker on 20/01/21.
//

import Foundation
// MARK: - Welcome
struct WareHouseDetailsModel: Codable {
    let status: Int
    let message: String
    let data: [WareHouseDetailsArr]
}

// MARK: - Datum
struct WareHouseDetailsArr: Codable {
    let id, uid, email, contact: String
    let address, cd: String
}
