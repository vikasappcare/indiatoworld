//
//  WareHouseDetailsVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 15/12/20.
//

import UIKit

class WareHouseDetailsVC: UIViewController {

    //MARK:-  IB Outlets
    
    @IBOutlet weak var warehouseImage: UIImageView!
    @IBOutlet weak var uidTF: UITextField!
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var contactNoTF: UITextField!
    @IBOutlet weak var addrTF: UITextView!
    @IBOutlet weak var uidBtn: CustomButton!
    @IBOutlet weak var emailBtn: CustomButton!
    @IBOutlet weak var contactNumberBtn: CustomButton!
    @IBOutlet weak var addressBtn: CustomButton!
    
    
    var wareHouseDetailsArr = [WareHouseDetailsArr]()
    var alertStr = String()
    
    
    //MARK:-  View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Warehouse_Details)
        getWareHouseDetailsApi()
    }
  
    func wareDetails(detailsDict: WareHouseDetailsArr){
        
        uidTF.text = detailsDict.uid
        emailIDTF.text = detailsDict.email
        contactNoTF.text = detailsDict.contact
        addrTF.text = String(format: "Uid: %@, %@", detailsDict.uid,detailsDict.address)
        
    }
    
    func showCodeAlert() {
        let alertController = UIAlertController.init(title: alertStr, message: nil, preferredStyle: .alert)
        present(alertController, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0){
                alertController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func codeNotCopyAlert() {
        
        let alertController = UIAlertController.init(title: "Code Not Copy", message: nil, preferredStyle: .alert)
        present(alertController, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0){
                alertController.dismiss(animated: true, completion: nil)
            }
        }

    }
    
    func checkingCodeCopying()
    
    {
        // read from clipboard
        let content = UIPasteboard.general.string
        
        if content?.isEmpty != nil{
            
            showCodeAlert()
            
        }else{
            
            codeNotCopyAlert()
        }

    }
    //MARK:-  IB Actions
    

    @IBAction func onClickCopyUidBtn(_ sender: UIButton) {
        
        alertStr = "Copy Uid Successfully"
        // write to clipboard
        UIPasteboard.general.string = self.uidTF.text
        checkingCodeCopying()
    }
    @IBAction func onClickCopyEmailBtn(_ sender: UIButton) {
        
        // write to clipboard
        alertStr = "Copy Email Id Successfully"

        UIPasteboard.general.string = self.emailIDTF.text
        
        checkingCodeCopying()
        
    }
    
    @IBAction func onClickCopyContactNumberBtn(_ sender: UIButton) {
        
        // write to clipboard
        alertStr = "Copy Contact Number Successfully"

        UIPasteboard.general.string = self.contactNoTF.text
        
        checkingCodeCopying()
        
    }
    
    
    @IBAction func onClickCopyAddressBtn(_ sender: UIButton) {
        
        // write to clipboard
        alertStr = "Copy Address Successfully"

        UIPasteboard.general.string = self.addrTF.text
        
        checkingCodeCopying()
    }
}
//MARK:-  API Services

extension WareHouseDetailsVC {
    
    func getWareHouseDetailsApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.WareDetailsUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(WareHouseDetailsModel.self, from: data){
                            if loginData.status == 200{
                                self.wareHouseDetailsArr = loginData.data
                                print(self.wareHouseDetailsArr)
                                self.wareDetails(detailsDict: self.wareHouseDetailsArr[0])
                                
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
