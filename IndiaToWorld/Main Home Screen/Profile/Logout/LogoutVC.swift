//
//  LogoutVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseInstanceID

class LogoutVC: UIViewController {

    @IBOutlet weak var logoutOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoutOutlet.layer.borderColor = #colorLiteral(red: 0.1803921569, green: 0.3568627451, blue: 1, alpha: 1).cgColor
        logoutOutlet.layer.borderWidth = 1
        logoutOutlet.layer.cornerRadius = 5
    }

    // MARK: -  User Logout Session

    func userLogoutSession() {
        
        let defaults = UserDefaults.standard
        
        defaults.set(false, forKey: LOGGED_IN)
        defaults.synchronize()
        
        resetUserDefaults()

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.LoginViewController) as! LoginViewController
//        let nav = UINavigationController.init(rootViewController: vc)
//        nav.modalPresentationStyle = .fullScreen
//        nav.modalTransitionStyle = .crossDissolve
//        present(nav, animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
//        DispatchQueue.main.async {
//
//            let appDelegate = UIApplication.shared.delegate
//            appDelegate?.window??.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let vc = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    // MARK: -  Reset UserDefaults
    
    func resetUserDefaults() {
        
        print("BEGIN:IndiaToWorld ======> userDefaults successfully removed from the app")
        
        DispatchQueue.main.async {
            
            let instance = InstanceID.instanceID()
            instance.deleteID { (error) in
                
                if error != nil {
                    
                   let ERROR_MESSAGE = error?.localizedDescription ?? ""
                    print("Instance ID Error : \(ERROR_MESSAGE)")
                    
                } else {
                    
                    print("FCM TOKEN Deleted Successfully");
                }
            }
        }

        let userDefaults = UserDefaults.standard
        let dict = userDefaults.dictionaryRepresentation()
        
        dict.keys.forEach { key in
            
            userDefaults.removeObject(forKey: key)
        }
    }
    //MARK:- selector
    @IBAction func logoutAction(_ sender: Any) {
        
        userLogoutSession()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
