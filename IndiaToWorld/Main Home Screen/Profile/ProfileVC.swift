//
//  ProfileVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import Kingfisher
class ProfileVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var userIDLbl: UILabel!
    var getUserProfileModelInfo : getUserProfileModel?
    var imagesArray = [UIImage(named: "BasicProfile"),UIImage(named: "WarehouseDetails"),UIImage(named: "SubscriptionPlans"),UIImage(named: "Tools"),UIImage(named: "currency"),UIImage(named: "YourAddresses"),UIImage(named: "Settings"),UIImage(named: "Logout")]//UIImage(named: "Payments")
    static let listValues = ["Basic Profile", "Warehouse Details", "Subscription Plans", "Shipping Cost Calculator","Currency Convertor", "Your Addresses", "Settings", "Logout"] //"Payments"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.cornerRadius = 5
        tableView.layer.borderWidth = 2
        tableView.layer.borderColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
      //  profileImage.backgroundColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 0.7306376072)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userDetailsRequest()
        self.tabBarController?.tabBar.isHidden = false
        
    }
}

extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProfileVC.listValues.count //8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileListTVCell.identifier, for: indexPath) as! ProfileListTVCell
        cell.nameList.text = ProfileVC.listValues[indexPath.row]
        cell.imageList.image = imagesArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //profile
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.BasicProfileVC) as! BasicProfileVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1 {
            //warehouse
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.WareHouseDetailsVC) as! WareHouseDetailsVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 2 {
            //subscription
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SubscriptionVC) as! SubscriptionVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else if indexPath.row == 3 {
            //Shipping cost calculator
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.CostCalculatorVC) as! CostCalculatorVC
            navigationController?.pushViewController(vc, animated: true)

        }else if indexPath.row == 4 {
            //Currency converter
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.CurrencyConvertorVC) as! CurrencyConvertorVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 5{
            //Address
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.AddressListVC) as! AddressListVC
            navigationController?.pushViewController(vc, animated: true)
            
        }
//        else if indexPath.row == 5 {
//            //payments
//            //            let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//            //            navigationController?.pushViewController(vc, animated: true)
//
//        }
        else if indexPath.row == 6 {
            //settings
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.SettingsVC) as! SettingsVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 7 {
            //logout
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.LogoutVC) as! LogoutVC
            vc.modalPresentationStyle = .overCurrentContext
            navigationController?.present(vc, animated: false, completion: nil)
        }
        
    }
    
}

//MARK:-  API Services

extension ProfileVC {
    
    func userDetailsRequest(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid: AppHelper.getUserId(),
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.getUserProfile)")
            
            ApiHandler.getData(url: AppUrls.getUserProfile, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { [self] (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let AddressListData = try? JSONDecoder().decode(getUserProfileModel.self, from: data){
                            if AddressListData.status == 200{
                                let dataArr = AddressListData
                                self.getUserProfileModelInfo = dataArr
                                DispatchQueue.main.async {
                                    userReponse()
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: AddressListData.message ?? "")
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    
    func userReponse(){
        if getUserProfileModelInfo?.data?[0].pPic != ""{
            
            let name = String(format: "%@ %@", getUserProfileModelInfo?.data?[0].firstName ?? "",getUserProfileModelInfo?.data?[0].lastName ?? "")
            UserDefaults.standard.set(name, forKey: FULL_NAME)
            UserDefaults.standard.synchronize()

            let chosenImgUrl = getUserProfileModelInfo?.data?[0].pPic
                    let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                   self.profileImage.kf.setImage(with: URL(string: chosenImgUrlString ?? ""), placeholder: UIImage(named: "emptyIcon"), options: nil, progressBlock: nil)
        }else{
            // profileImage
        }
        profileNameLbl.text = String(format: "%@ %@", getUserProfileModelInfo?.data?[0].firstName ?? "",getUserProfileModelInfo?.data?[0].lastName ?? "")
        userIDLbl.text = String(format: "ID: %@", getUserProfileModelInfo?.data?[0].uid ?? "")
    }
}

