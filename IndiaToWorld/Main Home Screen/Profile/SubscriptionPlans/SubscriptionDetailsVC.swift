//
//  SubscriptionDetailsVC.swift
//  IndiaToWorld
//
//  Created by Bhasker on 04/01/21.
//

import UIKit
import Razorpay


class SubscriptionDetailsVC: UIViewController {
    
    
    @IBOutlet weak var subscriptionPlanCollVw: UICollectionView!
    @IBOutlet weak var planBenefitsTblVw: UITableView!
    @IBOutlet weak var choosePlanBtn: CustomButton!
    
    var planBenefitsArray = [String]()
    var planArray = [SubscriptionPlanDetailsArr]()
    var planId = String()
    var subPlanId = String()
    var selectedcell = 0
    var razorpay: RazorpayCheckout!
    var transcationId = String()
    var orderId = String()
    var amount = String()
    var toDayDate = String()
    var paymentStatus = String()

    //    static let validityArray = ["Validity: 30 Days","Validity: 365 days"]
    //    static let priceArray = ["₹300/-","₹3300/-"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Subscription_Plans)
        planBenefitsTblVw.tableFooterView = UIView()
        planBenefitsTblVw.separatorStyle = .none
        planBenefitsTblVw.estimatedRowHeight = planBenefitsTblVw.rowHeight
        planBenefitsTblVw.rowHeight = UITableView.automaticDimension
        razorpay = RazorpayCheckout.initWithKey("rzp_test_2dpgkg40hOPhTo", andDelegate: self)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        toDayDate = formatter.string(from: Date())
        print("Current Date : \(toDayDate)")

        self.tabBarController?.tabBar.isHidden = true
        
        getSubscriptionPlansDetailsApi()
    }
    
    internal func showPaymentForm(){
        
        let amount1 = Float(self.amount)! * 100
               let amount2 = Int(amount1)

        let options: [String:Any] = [
                    
                    "amount": amount2, //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "purchase description",
//                    "order_id": "order_DBJOWzybf0sJbb",
                    "image": "",
                    "name": AppHelper.getFullName(),
                    "prefill": [
                        "contact": AppHelper.getMobileNo(),
                        "email": AppHelper.getEmailId()
                    ],
                    "theme": [
                        "color": "#012A93"
                      ]
                ]
        razorpay.open(options)
    }
    //MARK:-  IB Actions
    
    @IBAction func onClickChhosePlanBtn(_ sender: UIButton) {
        
        if subPlanId != "" {
            
            saveUserPlanApi()
            
        }else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.subPlan)
        }
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessPaymentVC) as! SuccessPaymentVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension SubscriptionDetailsVC: RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        print(str)
        paymentStatus = "Failed"

         showAlertMessage(vc: self, titleStr: "", messageStr: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        print(payment_id)
        paymentStatus = "Success"

        transcationId = payment_id
        userPaymentStatusAPI()

    }
}
//MARK:-  UITableViewDelegate, UITableViewDataSource
extension SubscriptionDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planBenefitsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionPlanBenefitsTableViewCell.identifier, for: indexPath) as! SubscriptionPlanBenefitsTableViewCell
        
        cell.planBenefitsLbl.text = planBenefitsArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}
//MARK:-  UITableViewDelegate, UITableViewDataSource
extension SubscriptionDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return planArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SubcriptionDetailsPlansCell.identifier, for: indexPath) as! SubcriptionDetailsPlansCell
        
        cell.planTitleLbl.text = planArray[indexPath.row].subPlanTitle
        cell.validityLbl.text = planArray[indexPath.row].planValidity
        cell.priceLbl.text = planArray[indexPath.row].planAmount
        
        if indexPath.item == selectedcell {
            cell.bgVw.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 1, alpha: 1)
        }else{
            cell.bgVw.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width / 2 , height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedcell = indexPath.row
        subscriptionPlanCollVw.reloadData()
        if planArray.count > 0{
            
            planBenefitsArray = planArray[indexPath.row].planBenifits
            subPlanId = planArray[indexPath.row].subPlanID
            amount = planArray[indexPath.row].planAmount
        }
        planBenefitsTblVw.reloadData()
    }
}
//MARK:-  API Services

extension SubscriptionDetailsVC {
    
    func getSubscriptionPlansDetailsApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.plan_id:planId
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.MembershipsPlansDetailsUrl)")
            
            ApiHandler.getData(url: AppUrls.MembershipsPlansDetailsUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let plansDetails = try? JSONDecoder().decode(SubscriptionPlanDetailsModel.self, from: data){
                            if plansDetails.status == 200{
                                self.planArray = plansDetails.data
                                print(self.planArray)
                                self.planBenefitsArray = self.planArray[0].planBenifits
                                self.subPlanId = self.planArray[0].subPlanID
                                self.amount = self.planArray[0].planAmount

                                if self.planArray.count > 0 {
                                    
                                    //                                    self.noDataImgVw.isHidden = true
                                    self.planBenefitsTblVw.isHidden = false
                                    self.subscriptionPlanCollVw.isHidden = false
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.subscriptionPlanCollVw.delegate = self
                                        self.subscriptionPlanCollVw.dataSource = self
                                        self.subscriptionPlanCollVw.reloadData()
                                        if self.planBenefitsArray.count > 0 {
                                            
                                            self.planBenefitsTblVw.delegate = self
                                            self.planBenefitsTblVw.dataSource = self
                                            self.planBenefitsTblVw.reloadData()
                                            
                                        }
                                    }
                                }else {
                                    self.subscriptionPlanCollVw.isHidden = true
                                    self.planBenefitsTblVw.isHidden = true
                                }
                                
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: plansDetails.message)
                                self.subscriptionPlanCollVw.isHidden = true
                                self.planBenefitsTblVw.isHidden = true
                                
                            }
                            
                            
                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                            self.subscriptionPlanCollVw.isHidden = true
                            self.planBenefitsTblVw.isHidden = true
                            
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    
    func saveUserPlanApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.plan_id: planId,
                PARAMS.sub_plan_id: subPlanId,
            ]
            
            ApiHandler.getJsonData(url: AppUrls.SaveUserPlansUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            self.orderId = data["Invoiceid"] as? String ?? ""
                            self.showPaymentForm()
//                            self.navigationController?.backToViewController(vc: ReadyToSendVC.self)
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessPaymentVC) as! SuccessPaymentVC
//                            self.navigationController?.pushViewController(vc, animated: true)

//                            self.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
   
    func userPaymentStatusAPI(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.uid:AppHelper.getUserId(),
                PARAMS.order_id: orderId,
                PARAMS.txn_id: transcationId,
                PARAMS.payment_status: paymentStatus,
                PARAMS.date_time:toDayDate
            ]
            
            ApiHandler.getJsonData(url: AppUrls.UserPaymentStatusUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        showAlertWithOkAction(self, title: STATUS, message: data["message"] as? String ?? "" , buttonTitle: "OK") {

                            self.navigationController?.popViewController(animated: true)
//                            self.navigationController?.backToViewController(vc: ReadyToSendVC.self)
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SuccessPaymentVC) as! SuccessPaymentVC
//                            self.navigationController?.pushViewController(vc, animated: true)

//                            self.navigationController?.popViewController(animated: true)
                        }
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")
                        
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
