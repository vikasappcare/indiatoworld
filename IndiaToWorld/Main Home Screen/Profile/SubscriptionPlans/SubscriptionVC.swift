//
//  SubscriptionVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class SubscriptionVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    var isFrom = ""
    var imagesArray = [UIImage(named: "CrownPlan"),UIImage(named: "DimPlan"),UIImage(named: "GoldPlan")]
   
     var subArray = [SubscriptionPlansArr]()
    var isCheckBoxSelected = Bool()
    var selectedPlanIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Subscription_Plans)
        selectedPlanIndex = 0
        if isFrom == "Consolidate"{
        nextBtn.isHidden = false
       }else{
        nextBtn.isHidden = true
       }
        isCheckBoxSelected = false
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        selectedPlanIndex = 0
        if isFrom == "Consolidate"{
        nextBtn.isHidden = false
       }else{
        nextBtn.isHidden = true
       }
        isCheckBoxSelected = false
        self.tabBarController?.tabBar.isHidden = true
        getSubscriptionPlnsApi()

    }

    //MARK:-  IB Actions
    
    @objc func onClickPlanSelectedCheckBoxBtn(_ sender: UIButton){
        
        let cell = tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? SubscriptionTVCell
        if isCheckBoxSelected == false {
            
            cell?.planSelectedCheckBoxImgVw.image = UIImage(named: "SelcetedIcon")
            isCheckBoxSelected = true

        }else {
            
            cell?.planSelectedCheckBoxImgVw?.image = UIImage(named: "UnCheckRadioButton")
            isCheckBoxSelected = false

        }
//        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
    }
    
    @objc func onClickViewInDetailBtn(_ sender: UIButton){
        
        DispatchQueue.main.async {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.SubscriptionDetailsVC) as! SubscriptionDetailsVC
            vc.planId = self.subArray[sender.tag].id
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionDetailsVC") as! SubscriptionDetailsVC
        vc.planId = self.subArray[selectedPlanIndex].id
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK:-  UITableViewDelegate, UITableViewDataSource
extension SubscriptionVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionTVCell.identifier, for: indexPath) as! SubscriptionTVCell
        
        cell.planNameLbl.text = String(format: "%@ - %@", subArray[indexPath.row].title,subArray[indexPath.row].price)
        cell.planDetailsLbl.text = subArray[indexPath.row].discription
        
        if subArray[indexPath.row].title == "Crown" {
            cell.imageSub.image = UIImage(named: "CrownPlan")
        }else if subArray[indexPath.row].title == "silver" {
            cell.imageSub.image = UIImage(named: "DimPlan")
        }else {
            cell.imageSub.image = UIImage(named: "GoldPlan")
        }
        

        cell.viewInDetailBtn.tag = indexPath.row
        cell.planSelectedCheckBoxBtn.tag = indexPath.row
        
        cell.viewInDetailBtn.addTarget(self, action: #selector(onClickViewInDetailBtn), for: .touchUpInside)
        cell.viewInDetailBtn.addTarget(self, action: #selector(onClickPlanSelectedCheckBoxBtn), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        selectedPlanIndex = indexPath.row
//        DispatchQueue.main.async {
//
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewMoreVC") as! ViewMoreVC
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
    }
    
}
//MARK:-  API Services

extension SubscriptionVC {
    
    func getSubscriptionPlnsApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.MembershipsPlansUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(SubscriptionPlansModel.self, from: data){
                            if loginData.status == 200{
                                self.subArray = loginData.data
                                print(self.subArray)
                                if self.subArray.count > 0 {
                                    
//                                    self.noDataImgVw.isHidden = true
                                    self.tableView.isHidden = false
                                    DispatchQueue.main.async {
                                        
                                        self.tableView.delegate = self
                                        self.tableView.dataSource = self
                                        self.tableView.reloadData()

                                    }

                                }else {
                                    
//                                    self.noDataImgVw.isHidden = false
                                    self.tableView.isHidden = true
                                }

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
