//
//  SubscriptionPlanDetails.swift
//  IndiaToWorld
//
//  Created by Bhasker on 02/02/21.
//

import Foundation

// MARK: - Welcome
struct SubscriptionPlanDetailsModel: Codable {
    let status: Int
    let message: String
    let data: [SubscriptionPlanDetailsArr]
}

// MARK: - Datum
struct SubscriptionPlanDetailsArr: Codable {
    let planTitle, planID, subPlanTitle, subPlanID: String
    let planValidity, planAmount: String
    let planBenifits: [String]

    enum CodingKeys: String, CodingKey {
        case planTitle = "plan_title"
        case planID = "plan_id"
        case subPlanTitle = "sub_plan_title"
        case subPlanID = "sub_plan_id"
        case planValidity = "plan_validity"
        case planAmount = "plan_amount"
        case planBenifits = "plan_benifits"
    }
}
