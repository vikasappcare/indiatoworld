//
//  SubscriptionPlans.swift
//  IndiaToWorld
//
//  Created by Bhasker on 20/01/21.
//

import Foundation
/*// MARK: - Welcome
struct SubscriptionPlansModel: Codable {
    let status: Int
    let message: String
    let data: [SubscriptionPlansArr]
}

// MARK: - Datum
struct SubscriptionPlansArr: Codable {
    let id, title, discription, price: String
    let duration, cd: String
}*/
// MARK: - Welcome
struct SubscriptionPlansModel: Codable {
    let status: Int
    let message: String
    let data: [SubscriptionPlansArr]
}

// MARK: - Datum
struct SubscriptionPlansArr: Codable {
    let id, title, discription, price: String
    let monthlyPlan, monthlyPlanPrice, annualPlan, annualPlanPrice: String
    let cd: String

    enum CodingKeys: String, CodingKey {
        case id, title, discription, price
        case monthlyPlan = "monthly_plan"
        case monthlyPlanPrice = "monthly_plan_price"
        case annualPlan = "annual_plan"
        case annualPlanPrice = "annual_plan_price"
        case cd
    }
}
