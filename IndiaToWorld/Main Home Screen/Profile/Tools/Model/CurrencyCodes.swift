//
//  CurrencyCodes.swift
//  IndiaToWorld
//
//  Created by Bhasker on 20/01/21.
//

import Foundation

// MARK: - Welcome
struct CurrencyCodesModel: Codable {
    let status: Int
    let message: String
    let data: [CurrencyCodesArr]
}

// MARK: - Datum
struct CurrencyCodesArr: Codable {
    let title, symbol, code: String
}
