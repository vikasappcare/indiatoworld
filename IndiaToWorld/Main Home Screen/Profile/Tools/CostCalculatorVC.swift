//
//  CostCalculatorVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import DropDown


class CostCalculatorVC: UIViewController {

    @IBOutlet weak var heightTF: UITextField!
    
    @IBOutlet weak var widthTF: UITextField!
    
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var depthTF: UITextField!
    
    
    @IBOutlet weak var selectedCountryTF: UITextField!
    
    @IBOutlet weak var indiaEstimatedPriceTF: UITextField!
    
    
    @IBOutlet weak var selectedCoutryCodeDropdownBtn: CustomButton!
    
    
    
    let countryCodesDropDown = DropDown()
    var currencyCodesArr = [CurrencyCodesArr]()
    var codeDropDownArr = [String]()
    var selectedCountryCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Shipping_Cost_Calculator)

        selectedCountryTF.isEnabled = false
        indiaEstimatedPriceTF.isEnabled = false
        DispatchQueue.main.async {
            
            self.getCurrencyCodesApi()

        }

        self.countryCodesDropDown.anchorView = self.selectedCoutryCodeDropdownBtn
        self.countryCodesDropDown.bottomOffset = CGPoint(x: 0, y: self.selectedCoutryCodeDropdownBtn.bounds.height+15)
        self.countryCodesDropDown.dataSource = self.codeDropDownArr
        self.countryCodesDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedCountryCode = self?.currencyCodesArr[index].code ?? ""
            self?.selectedCoutryCodeDropdownBtn.setTitle(item, for: .normal)
            self?.selectedCountryTF.placeholder = self?.currencyCodesArr[index].symbol ?? ""
        }
    }
    
    
    @IBAction func onClickSelectedCOutryDropDownBtn(_ sender: UIButton) {
        
        countryCodesDropDown.show()

    }
    
    @IBAction func onClickCostCalculateBtn(_ sender: UIButton) {
        
        costCalculatorApi()
    }
    
}
//MARK:-  API Services

extension CostCalculatorVC {
    
    func getCurrencyCodesApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.CurrencyCodesUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(CurrencyCodesModel.self, from: data){
                            if loginData.status == 200{
                                self.currencyCodesArr = loginData.data
                                
                                for item in self.currencyCodesArr {
                                    self.codeDropDownArr.append(item.title)
                                }
                                
                                self.countryCodesDropDown.dataSource = self.codeDropDownArr

                                self.selectedCoutryCodeDropdownBtn.setTitle(self.codeDropDownArr[0], for: .normal)
//                                self.selectedCountryCode = self.codeDropDownArr[0]
                                self.selectedCountryTF.placeholder = self.currencyCodesArr[0].symbol
                                self.selectedCountryCode = self.currencyCodesArr[0].code

                                print(self.codeDropDownArr)
                                

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func costCalculatorApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                PARAMS.code2:selectedCountryCode,
                PARAMS.code1: "INR",
                PARAMS.height:heightTF.text ?? "",
                PARAMS.width:widthTF.text ?? "",
                PARAMS.depth:depthTF.text ?? "",
                PARAMS.actual_weight:weightTF.text ?? "",
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.CostCalculatorUrl)")

            ApiHandler.getJsonData(url: AppUrls.CostCalculatorUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        let inrStr = String(data["totalinINR"] as? Int ?? 0)
                        let otherStr = String(data["totalinothercountry"] as? Int ?? 0)

                        self.selectedCountryTF.text = String(format: "%@",otherStr )
                        self.indiaEstimatedPriceTF.text = String(format: " ₹ %@", inrStr)

                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
