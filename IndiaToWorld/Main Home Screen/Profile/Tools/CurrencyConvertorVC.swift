//
//  CurrencyConvertorVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import DropDown

class CurrencyConvertorVC: UIViewController {

    
    @IBOutlet weak var selectedCountryTF: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var selectedCountryBtn: CustomButton!
    @IBOutlet weak var convertedAmountLbl: UILabel!
    
    @IBOutlet weak var calculateBtn: UIButton!
    let countryCodesDropDown = DropDown()
    var currencyCodesArr = [CurrencyCodesArr]()
    var codeDropDownArr = [String]()
    var selectedCountryCode = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Currency_Convertor)
        selectedCountryTF.isEnabled = false
        DispatchQueue.main.async {
            
            self.getCurrencyCodesApi()

        }
        
        self.countryCodesDropDown.anchorView = self.selectedCountryTF
        self.countryCodesDropDown.bottomOffset = CGPoint(x: 0, y: self.selectedCountryTF.bounds.height+15)
        self.countryCodesDropDown.dataSource = self.codeDropDownArr
        self.countryCodesDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedCountryTF.text = item
            self?.selectedCountryCode = self?.currencyCodesArr[index].code ?? ""
            self?.selectedCountryTF.placeholder = self?.currencyCodesArr[index].symbol ?? ""

        }

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
//            self.countryCodesDropDown.anchorView = self.selectedCountryTF
//            self.countryCodesDropDown.bottomOffset = CGPoint(x: 0, y: self.selectedCountryTF.bounds.height+15)
//            self.countryCodesDropDown.dataSource = self.codeDropDownArr
//            self.countryCodesDropDown.selectionAction = { [weak self] (index, item) in
//                self?.selectedCountryTF.text = item
//            }

        }
    }
    //MARK:-  IB Actions
    
    @IBAction func onClickCalculateBtn(_ sender: UIButton) {
        
        currencyConverterApi()
    }
    
    @IBAction func onClickSelectedCountryBtn(_ sender: UIButton) {
        
        countryCodesDropDown.show()
    }
    
}
//MARK:-  API Services

extension CurrencyConvertorVC {
    
    func getCurrencyCodesApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.CurrencyCodesUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(CurrencyCodesModel.self, from: data){
                            if loginData.status == 200{
                                self.currencyCodesArr = loginData.data
                                
                                for item in self.currencyCodesArr {
                                    self.codeDropDownArr.append(item.title)
                                }
                                print(self.codeDropDownArr)
                                self.countryCodesDropDown.dataSource = self.codeDropDownArr
                                self.selectedCountryTF.text = self.codeDropDownArr[0]
                                self.selectedCountryCode = self.currencyCodesArr[0].code
                                self.selectedCountryTF.placeholder = self.currencyCodesArr[0].symbol


                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func currencyConverterApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            let parameters: [String:Any] = [
                                            PARAMS.code1:selectedCountryCode,
                                            PARAMS.code2: "INR",
                                            PARAMS.amount:amountTF.text ?? "",
            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.CurrencyConverterUrl)")

            ApiHandler.getJsonData(url: AppUrls.CurrencyConverterUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    
                    if data["status"] as? Int == 200{
                        
                        self.convertedAmountLbl.text = String(format: " ₹ %@", data["data"] as? String ?? "")
                        
                    }else {
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: data["message"] as? String ?? "")

                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
