//
//  ToolsListVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class ToolsListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    static let toolsList = ["Currency Convertor", "Shipping Cost Calculator"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: NavTitle.Tools)
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }

}

extension ToolsListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ToolsListVC.toolsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ToolsTVCell.identifier, for: indexPath) as! ToolsTVCell
        cell.nameTF.text = ToolsListVC.toolsList[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.CurrencyConvertorVC) as! CurrencyConvertorVC
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.CostCalculatorVC) as! CostCalculatorVC
            navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
}
