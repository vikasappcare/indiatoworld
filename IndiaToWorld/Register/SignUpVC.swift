//
//  SignUpVC.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit
import ADCountryPicker
import CoreTelephony
import libPhoneNumber_iOS
import DropDown


class SignUpVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var fNameTF: UITextField!
    @IBOutlet weak var lNameTF: UITextField!
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var phoneNoTF: UITextField!
    @IBOutlet weak var deliveryAddrTF: UITextField!
    @IBOutlet weak var deliveryAddr1TF: UITextField!
    @IBOutlet weak var newPswdTF: UITextField!
    @IBOutlet weak var pswdBtn: UIButton!
    @IBOutlet weak var countryImgVw: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var countryCodeBtn: CustomButton!
    
    @IBOutlet weak var countryNameTF: CustomTF!
    
    @IBOutlet weak var stateNameTF: CustomTF!
    
    @IBOutlet weak var cityNameTF: CustomTF!
    
    
    @IBOutlet weak var postalCodeTF: CustomTF!
    
    
    let countryNamesDropDown = DropDown()
    let stateNamesDropDown = DropDown()
    let cityNamesDropDown = DropDown()

    var countryListArr = [CountryList]()
    var stateListArr = [StatesList]()
    var cityListArr = [CityList]()

    var countryDropDownArr = [String]()
    var stateDropDownArr = [String]()
    var cityDropDownArr = [String]()

    var selectedCountryCode = String()
    var selectedStateCode = String()
    var selectedCityCode = String()

    var logInDataDictModel: RegisterModelDict?
    var getCurrentRegion = String()
    var dialCodeStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        navTitle(heading: NavTitle.Sign_up)
        configureView()
        getCountryListApi()
        self.countryNamesDropDown.anchorView = self.countryNameTF
        self.countryNamesDropDown.bottomOffset = CGPoint(x: 0, y: self.countryNameTF.bounds.height+15)
        self.countryNamesDropDown.dataSource = self.countryDropDownArr
        self.countryNamesDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedCountryCode = self?.countryListArr[index].countryID ?? ""
            self?.countryNameTF.text = item
            
            self?.getStateListApi()
        }
        
        self.stateNamesDropDown.anchorView = self.stateNameTF
        self.stateNamesDropDown.bottomOffset = CGPoint(x: 0, y: self.stateNameTF.bounds.height+15)
        self.stateNamesDropDown.dataSource = self.stateDropDownArr
        self.stateNamesDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedStateCode = self?.stateListArr[index].stateID ?? ""
            self?.stateNameTF.text = item
            self?.getCityListApi()
        }
        
        self.cityNamesDropDown.anchorView = self.cityNameTF
        self.cityNamesDropDown.bottomOffset = CGPoint(x: 0, y: self.cityNameTF.bounds.height+15)
        self.cityNamesDropDown.dataSource = self.countryDropDownArr
        self.cityNamesDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedCityCode = self?.cityListArr[index].cityID ?? ""
            self?.cityNameTF.text = item
        }

    }
    
    func configureView() {
        
//        getCurrentRegion = NSLocale.current.regionCode ?? ""
//        print(AppHelper.getCountryCallingCode(countryRegionCode: getCurrentRegion))
//        countryCodeLbl.text = AppHelper.getCountryCallingCode(countryRegionCode: getCurrentRegion)
//        dialCodeStr = AppHelper.getCountryCallingCode(countryRegionCode: getCurrentRegion)
        countryCodeLbl.text = "+91"
        dialCodeStr = "+91"
        pswdBtn.setImage(#imageLiteral(resourceName: "Hideeye"), for: .normal)
        newPswdTF.isSecureTextEntry = true

//        countryImgVw.image = UIImage(named: "IN")

    }
    //MARK:- Selector
    
    @IBAction func onClickCountryCodeBtn(_ sender: UIButton) {
        
        let picker = ADCountryPicker(style: .grouped)
        picker.delegate = self
        picker.showCallingCodes = true
        picker.showFlags = true
        picker.hidesNavigationBarWhenPresentingSearch = false
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        pickerNavigationController.modalPresentationStyle = .fullScreen
        self.present(pickerNavigationController, animated: true, completion: nil)

    }
    
    //Password
    @IBAction func passwordAction(_ sender: UIButton) {
        
//        pswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
//        newPswdTF.isSecureTextEntry = false
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            pswdBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            newPswdTF.isSecureTextEntry = false
        }else{
            pswdBtn.setImage(#imageLiteral(resourceName: "Hideeye"), for: .normal)
            newPswdTF.isSecureTextEntry = true
        }

    }
    
    @IBAction func onClickCountryBtn(_ sender: UIButton) {
        
        countryNamesDropDown.show()
        
    }
    
    @IBAction func onClickStateBtn(_ sender: UIButton) {
        
        stateNamesDropDown.show()
    }
    
    
    @IBAction func onClickCityBtn(_ sender: UIButton) {
        
        cityNamesDropDown.show()
    }
    
    //signin
    @IBAction func signInAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //Sigup submit
    @IBAction func loginAction(_ sender: Any) {
        
        
        self.fNameTF.text = self.fNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.lNameTF.text = self.lNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.emailIDTF.text = self.emailIDTF.text?.trimmingCharacters(in: .whitespaces)
        self.phoneNoTF.text = self.phoneNoTF.text?.trimmingCharacters(in: .whitespaces)
        self.deliveryAddrTF.text = self.deliveryAddrTF.text?.trimmingCharacters(in: .whitespaces)
        self.countryNameTF.text = self.countryNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.stateNameTF.text = self.stateNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.cityNameTF.text = self.cityNameTF.text?.trimmingCharacters(in: .whitespaces)
        self.postalCodeTF.text = self.postalCodeTF.text?.trimmingCharacters(in: .whitespaces)

        self.newPswdTF.text = self.newPswdTF.text?.trimmingCharacters(in: .whitespaces)
        
        if fNameTF.hasText {
            if lNameTF.hasText {
                if emailIDTF.hasText {
                    
                    if AppHelper.isValidEmail(with: emailIDTF.text){
                        
                        if phoneNoTF.hasText {
                            
                            if deliveryAddrTF.hasText {
                                
                                if countryNameTF.hasText {
                                    
                                    if stateNameTF.hasText {
                                        
                                        if cityNameTF.hasText {
                                            
                                            if postalCodeTF.hasText {
                                                
                                                if newPswdTF.hasText {
                                                    
                                                    userRegisterApi()
                                                    
                                                    
                                                } else {
                                                    
                                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.newPswd)
                                                }

                                                
                                            } else {
                                                
                                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.postalCode)
                                            }
                                        } else {
                                            
                                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.city)
                                        }                                    } else {
                                        
                                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.state)
                                    }                                } else {
                                    
                                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.country)
                                }
                                
                            } else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.addrName)
                            }
                            
                        } else {

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.mobileNo)
                        }
                        
                    } else {
                        
                        showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailFormat)
                    }
                    
                } else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailID)
                }
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.lastName)
            }
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.firstName)
        }
    }
}

//MARK:  ADCountryPickerDelegate Methods
extension SignUpVC : ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
    
        countryCodeLbl.text = dialCode
        dialCodeStr = dialCode
        getCurrentRegion = code
        let flagImage =  picker.getFlag(countryCode: code)
//        countryImgVw.image = flagImage
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
//MARK:-  API Services

extension SignUpVC {
    
    
    func getCountryListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                        
            ApiHandler.getData(url: AppUrls.CountryListUrl, parameters: nil, method: .get, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(CountryModelData.self, from: data){
                            if loginData.status == 200{
                                self.countryListArr = loginData.data
                                self.countryDropDownArr = []
                                for item in self.countryListArr {
                                    self.countryDropDownArr.append(item.name)
                                }
                                
                                print(self.countryDropDownArr)
                                self.countryNamesDropDown.dataSource = self.countryDropDownArr

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func getStateListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            let parameters: [String:Any] = [PARAMS.country_id:selectedCountryCode,
            ]

            ApiHandler.getData(url: AppUrls.StateListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(StateModelData.self, from: data){
                            if loginData.status == 200{
                                self.stateListArr = loginData.data
                                self.stateDropDownArr = []
                                for item in self.stateListArr {
                                    self.stateDropDownArr.append(item.name)
                                }
                                
                                print(self.stateDropDownArr)
                                self.stateNamesDropDown.dataSource = self.stateDropDownArr

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    
    func getCityListApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
                  
            let parameters: [String:Any] = [PARAMS.state_id:selectedStateCode,
            ]
            ApiHandler.getData(url: AppUrls.CityListUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
               self.hideActivityIndicator()

                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(CityModelData.self, from: data){
                            if loginData.status == 200{
                                self.cityListArr = loginData.data
                                self.cityDropDownArr = []
                                for item in self.cityListArr {
                                    self.cityDropDownArr.append(item.name)
                                }
                                
                                print(self.cityDropDownArr)
                                self.cityNamesDropDown.dataSource = self.cityDropDownArr

                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }


                        }else{
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")

                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")

                }
            }

        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
    func userRegisterApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
            guard let deviceid = UserDefaults.standard.value(forKey: "deviceID") else {return}
            guard let deviceToken = UserDefaults.standard.value(forKey: "FCM_TOKEN") else {return}

            let parameters: [String:Any] = [PARAMS.first_name:fNameTF.text ?? "",
                                            PARAMS.last_name:lNameTF.text ?? "",
                                            PARAMS.email:emailIDTF.text ?? "",
                                            PARAMS.mobile:phoneNoTF.text ?? "",
                                            PARAMS.password:newPswdTF.text ?? "",
                                            PARAMS.address1:deliveryAddrTF.text ?? "",
                                            PARAMS.address2:"",
                                            PARAMS.deviceid:deviceid,
                                            PARAMS.devicetype:"ios",
                                            PARAMS.devicetoken:deviceToken,
                                            PARAMS.c_code: countryCodeLbl.text ?? "",
                                            PARAMS.country_id: countryNameTF.text ?? "",
                                            PARAMS.state_id: stateNameTF.text ?? "",
                                            PARAMS.city_id: cityNameTF.text ?? "",
                                            PARAMS.postal_code: postalCodeTF.text ?? ""

            ]
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.RegisterUrl)")

            ApiHandler.getData(url: AppUrls.RegisterUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(RegisterModel.self, from: data){
                            if loginData.status == 200{
                                
                                self.logInDataDictModel = loginData.date
                                showAlertWithOkAction(self, title: STATUS, message: loginData.message , buttonTitle: "OK") {
                                    DispatchQueue.main.async {
                                        
                                        self.navigationController?.popViewController(animated: true)

                                        
                                    }
                                }
                            }else {
                                
                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                                
                            }
                            
                        }else{
                            
                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                            
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}
