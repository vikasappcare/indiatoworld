//
//  ViewController.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var passwordBtn: UIButton!
    
    
    var logInDataDictModel: LoginModelDict?
    var defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordBtn.setImage(#imageLiteral(resourceName: "Hideeye"), for: .normal)
        passwordTF.isSecureTextEntry = true

        emailTF.text = "bhasker@appcare.co.in"
        passwordTF.text = "1234567"

        //        for family in UIFont.familyNames.sorted() {
        //            let names = UIFont.fontNames(forFamilyName: family)
        //            print("Family: \(family) Font names: \(names)")
        //        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        UserDefaults.standard.set(deviceID, forKey: "deviceID")

    }
    
    
    //MARK:- IB Actions
    
    //forgot password
    @IBAction func forgotPswdAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.ForgotPasswordVC) as! ForgotPasswordVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onClickPasswordHideShowBtn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            passwordBtn.setImage(#imageLiteral(resourceName: "openeye"), for: .normal)
            passwordTF.isSecureTextEntry = false
        }else{
            passwordBtn.setImage(#imageLiteral(resourceName: "Hideeye"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
        
    }
    //Signup
    @IBAction func signUpAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.SignUpVC) as! SignUpVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //Login
    @IBAction func loginAction(_ sender: Any) {
        
        self.emailTF.text = self.emailTF.text?.trimmingCharacters(in: .whitespaces)
        self.passwordTF.text = self.passwordTF.text?.trimmingCharacters(in: .whitespaces)
        
        if emailTF.hasText {
            
            if AppHelper.isValidEmail(with: emailTF.text){
                
                if passwordTF.hasText {
                    
                    userRegisterApi()
                } else {
                    
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.newPswd)
                }
                
            } else {
                
                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailFormat)
            }
            
        } else {
            
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: Alertmessage.emailID)
        }
    }
    
}

//MARK:-  API Services

extension LoginViewController {
    
    func userRegisterApi(){
        
        if Connectivity.isConnectedToInternet() {
            
            showActivityIndicator()
            
//            guard let deviceid = UserDefaults.standard.value(forKey: "deviceID") else {return}
//            guard let deviceToken = UserDefaults.standard.value(forKey: "FCM_TOKEN") else {return}

            
            let deviceid = UserDefaults.standard.value(forKey: "deviceID")
            let deviceToken = UserDefaults.standard.value(forKey: "FCM_TOKEN")
            
            
            let parameters: [String:Any] = [
                PARAMS.username:emailTF.text ?? "",
                PARAMS.password:passwordTF.text ?? "",
                PARAMS.deviceid:deviceid,
                PARAMS.devicetype:"ios",
                PARAMS.devicetoken:deviceToken,
            ]
            
            print("Parameters:  \(parameters)")
            print("UrlString:  \(AppUrls.LoginUrl)")

            ApiHandler.getData(url: AppUrls.LoginUrl, parameters: parameters, method: .post, headers: [HEADER.API_KEY:HEADER.API_KEY_VALUE], view: self) { (data,value, error) in
                self.hideActivityIndicator()
                
                if let data = data{
                    print(data)
                    do{
                        
                        if let loginData = try? JSONDecoder().decode(LoginModel.self, from: data){
                            if loginData.status == 200{
                                let dataArr = loginData.data
                                self.logInDataDictModel = dataArr[0]
                                print("Dict Data: \(String(describing: self.logInDataDictModel))")
                                let userId = self.logInDataDictModel?.uid
                                                                
                                // UserDefaults.standard.set(self.logInDataDict, forKey: LOGIN_RESPONSE)
                                self.defaults.set(userId, forKey: USER_ID)
                                self.defaults.set(self.logInDataDictModel?.email, forKey: EMAIL_ID)
                                self.defaults.set(self.logInDataDictModel?.mobile, forKey: MOBILE_NO)
                                self.defaults.set(self.logInDataDictModel?.firstName, forKey: FULL_NAME)
                                
                                self.defaults.set(true, forKey: LOGGED_IN)
                                self.defaults.synchronize()

                                showAlertWithOkAction(self, title: STATUS, message: loginData.message , buttonTitle: "OK") {
                                    DispatchQueue.main.async {
                                        
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: Identifiers.TabBarController) as! TabBarController
                                        self.navigationController?.pushViewController(vc, animated: true)

                                    }
                                }
                            }else {
                                self.hideActivityIndicator()

                                showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: loginData.message)
                            }
                            
                        }else{
                            self.hideActivityIndicator()

                            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: value?["message"] as? String ?? "")
                        }
                    }
                }else{
                    print("Error\(String(describing: error))")
                    self.hideActivityIndicator()
                    showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: error?.localizedDescription ?? "")
                    
                }
            }
            
        }else {
            
            self.hideActivityIndicator()
            showAlertMessage(vc: self, titleStr: ALERT_TITLE, messageStr: INTERNET_MSG)
        }
    }
}


