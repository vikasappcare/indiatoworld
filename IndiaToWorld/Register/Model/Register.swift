//
//  Register.swift
//  IndiaToWorld
//
//  Created by Bhasker on 18/01/21.
//

import Foundation
// MARK: - Welcome
struct RegisterModel: Codable {
    let status: Int
    let message: String
    let date: RegisterModelDict
}

// MARK: - DateClass
struct RegisterModelDict: Codable {
    let firstName, lastName, password, email: String
    let mobile, cCode, address1, address2: String
    let deviceType, deviceToken, deviceID, uid: String
    let userStatus: String

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case password, email, mobile
        case cCode = "c_code"
        case address1 = "address_1"
        case address2 = "address_2"
        case deviceType = "device_type"
        case deviceToken = "device_token"
        case deviceID = "device_id"
        case uid
        case userStatus = "user_status"
    }
}
