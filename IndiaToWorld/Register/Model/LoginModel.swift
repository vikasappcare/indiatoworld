//
//  LoginModel.swift
//  IndiaToWorld
//
//  Created by Bhasker on 18/01/21.
//

import Foundation
// MARK: - Welcome
struct LoginModel: Codable {
    let status: Int
    let message: String
    let data: [LoginModelDict]
}

// MARK: - Datum
struct LoginModelDict: Codable {
    let id, firstName, uid, lastName: String?
    let email, mobile, cCode: String?
    let otpStatus, uotp: String?
    let address2, address1, password, userStatus: String?
    let deviceID, deviceToken, deviceType, cd: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case uid
        case lastName = "last_name"
        case email, mobile
        case cCode = "c_code"
        case otpStatus = "otp_status"
        case uotp
        case address2 = "address_2"
        case address1 = "address_1"
        case password
        case userStatus = "user_status"
        case deviceID = "device_id"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case cd
    }
}
