//
//  CountryStatesCity.swift
//  IndiaToWorld
//
//  Created by Bhasker on 03/06/21.
//

import Foundation
// MARK: - Welcome
struct CountryModelData: Codable {
    let status: Int
    let message: String
    let data: [CountryList]
}

// MARK: - Datum
struct CountryList: Codable {
    let countryID, sortname, name: String

    enum CodingKeys: String, CodingKey {
        case countryID = "country_id"
        case sortname, name
    }
}


// MARK: - Welcome
struct StateModelData: Codable {
    let status: Int
    let message: String
    let data: [StatesList]
}

// MARK: - Datum
struct StatesList: Codable {
    let stateID, name: String

    enum CodingKeys: String, CodingKey {
        case stateID = "state_id"
        case name
    }
}
// MARK: - Welcome
struct CityModelData: Codable {
    let status: Int
    let message: String
    let data: [CityList]
}

// MARK: - Datum
struct CityList: Codable {
    let cityID, name: String

    enum CodingKeys: String, CodingKey {
        case cityID = "city_id"
        case name
    }
}

