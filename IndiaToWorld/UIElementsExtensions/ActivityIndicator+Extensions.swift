//
//  ActivityIndicator+Extensions.swift
//  IndiaToWorld
//
//  Created by Bhasker on 18/01/21.
//
import Foundation
import UIKit
import SVProgressHUD
//import FTIndicator

extension UIViewController {

    //MARK: -  Activity Indicator Show / Hide

    func showActivityIndicator() {

        DispatchQueue.main.async {

            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.setForegroundColor(.systemBlue)
            SVProgressHUD.show(withStatus: "Please Wait...")

//            FTIndicator.setIndicatorStyle(UIBlurEffect.Style.extraLight)
//            FTIndicator.showProgress(withMessage: "Please Wait...", userInteractionEnable: false)
        }
    }

    func hideActivityIndicator() {

        DispatchQueue.main.async {

            SVProgressHUD.dismiss()

//            FTIndicator.dismissProgress()
        }
    }

    //MARK: -  Keyboard Show / Hide

    func showKeyBoard() {

        view .endEditing(false)
    }

    func hideKeyBoard() {

        view .endEditing(true)
    }


    //MARK: -  Empty Back Button Title

    open override func awakeFromNib() {

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

}

