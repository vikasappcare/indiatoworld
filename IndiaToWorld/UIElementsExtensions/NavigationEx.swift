//
//  NavigationEx.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import Foundation
import UIKit

extension UINavigationController {
    
    //Coming back to any viewcontroller
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}

extension UIViewController{
    
    public func navTitle(heading:String) {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: CustomFont.productSansRegular, size: 15)
        titleLabel.text = heading
        navigationItem.titleView = titleLabel
    }
    
}
