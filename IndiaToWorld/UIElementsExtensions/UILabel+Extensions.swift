//
//  UILabel+Extensions.swift
//  IndiaToWorld
//
//  Created by Bhasker on 06/01/21.
//

import Foundation
import UIKit

// MARK: -  Use this through Programatically

extension UILabel {
    
    /// rounds corner on Label
    /// - Parameter radius: size of the rounded corner
    
    func uiLabelCornerRadius(radius: CGFloat) {
        
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    // shadow border on Label
    
    func uiLabelShadowBorder(with color: CGColor) {
        
        self.layer.shadowColor = color
        self.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 7.0
        self.layer.masksToBounds = false
    }
    
    /// border layer on Label
    /// - Parameter width: size of the border
    
    func uiLabelBorderLayerWidth(width: CGFloat) {
        
        self.layer.borderWidth = width
    }
    
    /// border layer color on Label
    /// - Parameter color: color of the border line
    
    func uiLabelBorderLayerColor(color: UIColor) {
        
        self.layer.borderColor = color.cgColor
    }
}

// MARK: -  Use this through StoryBoard

@IBDesignable

class UILabelAdditions: UILabel {
    
    @IBInspectable var cornerRadius : CGFloat = 0.0 {
        
        didSet {
            
            self.applyCornerRadius()
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        
        didSet {
            
            self.applyCornerRadius()
        }
    }
    
    @IBInspectable var borderWidth : Double = 0 {
        
        didSet {
            
            self.applyCornerRadius()
        }
    }
    
    @IBInspectable var circular : Bool = false {
        
        didSet {
            
            self.applyCornerRadius()
        }
    }
    
    func applyCornerRadius() {
        
        if(self.circular) {
            
            self.layer.cornerRadius = self.bounds.size.height/2
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = CGFloat(self.borderWidth)
            
        } else {
            
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = CGFloat(self.borderWidth)
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.applyCornerRadius()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        applyCornerRadius()
    }
    
}
