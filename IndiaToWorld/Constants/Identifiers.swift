//
//  Identifiers.swift
//  IndiaToWorld
//
//  Created by Appcare on 13/01/21.
//

import Foundation
import UIKit


//MARK:- Identifiers(VC)
struct Identifiers {
    static let NotificationsViewController = "NotificationsViewController"
    static let SuccessSubscriptionPlansVC = "SuccessSubscriptionPlansVC"
    static let SuccessPaymentVC = "SuccessPaymentVC"
    static let AllPackagesVC = "AllPackagesVC"
    static let ReadyToBuyVC = "ReadyToBuyVC"
    static let IncomingViewController = "IncomingViewController"
    static let InReviewViewController = "InReviewViewController"
    static let BookNewpackagesVC = "BookNewpackagesVC"
    static let ReadyToSendVC = "ReadyToSendVC"
    static let OrderPaymentViewController = "OrderPaymentViewController"
    static let OrderPaymentDetailsViewController = "OrderPaymentDetailsViewController"
    static let INTransitViewController = "INTransitViewController"
    static let ConsolidateVC = "ConsolidateVC"
    static let BuyForMeProductsViewController = "BuyForMeProductsViewController"
    static let ForgotPasswordVC = "ForgotPasswordVC"
    static let SignUpVC = "SignUpVC"
    static let LoginViewController = "LoginViewController"
    static let TabBarController = "TabBarController"
    static let OTPVerifyVC = "OTPVerifyVC"
    static let NewPasswordVC = "NewPasswordVC"
    static let ViewMoreVC = "ViewMoreVC"
    static let BookaNewPackagesVC = "BookaNewPackagesVC"
    static let AddressListVC = "AddressListVC"
    static let SubscriptionVC = "SubscriptionVC"
    static let ConsolidateListProductsVC = "ConsolidateListProductsVC"
    static let PackagingAndShippingVC = "PackagingAndShippingVC"
    static let WareHouseDetailsVC = "WareHouseDetailsVC"
    static let SuccessBookingVC = "SuccessBookingVC"
    static let SubscriptionDetailsVC = "SubscriptionDetailsVC"
    static let CurrencyConvertorVC = "CurrencyConvertorVC"
    static let CostCalculatorVC = "CostCalculatorVC"
    static let NewAddressVC = "NewAddressVC"
    static let ChangePswdVC = "ChangePswdVC"
    static let ReturnPolicyVC = "ReturnPolicyVC"
    static let PrivacyPoliciesVC = "PrivacyPoliciesVC"
    static let AboutVC = "AboutVC"
    static let FAQVC = "FAQVC"
    static let TermsofServiceVC = "TermsofServiceVC"
    static let LogoutVC = "LogoutVC"
    static let SettingsVC = "SettingsVC"
    static let ToolsListVC = "ToolsListVC"
    static let BasicProfileVC = "BasicProfileVC"
    static let SelectedAddressViewController = "SelectedAddressViewController"
    static let UserChatViewController = "UserChatViewController"
    static let ReturnConformationViewController = "ReturnConformationViewController"
}
