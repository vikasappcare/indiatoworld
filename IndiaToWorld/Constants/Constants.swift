//
//  Constants.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import Foundation
import UIKit
//MARK: - provideAPIKey

struct provideAPIKey {
    static let clientProvideAPIKey = "AIzaSyAACnJ2nRQulo75qm5ddR6vlwy6YlX1VYA"
}
//MARK: - Alert Titles

let STATUS = "Status"
let ALERT_TITLE = "Alert!"
let BUTTON_TITLE = "Ok"
let REQUEST_TIMED_OUT = "Unable to connect, Please try again later."
let INTERNET_MSG = "The Internet connection appears to be offline."
let ERROR = "Error"
let TRY_AGAIN = "Please Try Again..."

//MARK: - Invalid Keys For App Helper Class
let INVALID_FUL_NAME = "No Name Available"
let INVALID_USER_ID = "No User Id"
let INVALID_USER_TOKEN = "No User Token"
let INVALID_LOGIN_TYPE = "No Login Type"
let INVALID_GST_NO = "No GST No"
let INVALID_MOBILE_NO = "No Mobile No"
let INVALID_EMAIL_ID = "No Email Id"

//MARK: - UserDefaults Keys

let LOGIN_RESPONSE = "LOGIN_RESPONSE"
let TOKEN_KEY = "token"
let USER_ID = "id"
let FCM_TOKEN = "fcm_token"
let LOGIN_TYPE = "login_type"
let FULL_NAME = "FULL_NAME"
let PHONE_NUMBER = "PHONE_NUMBER"
let VALID_EMAIL = "VALID_EMAIL"
let VALID_ADDRESS = "VALID_ADDRESS"
let LOGGED_IN = "LOGGEDIN"
let IS_SALESEXECUTIVE_LOGGEDIN = "IS_SALESEXECUTIVE_LOGGEDIN"
let IS_CLIENT_LOGGEDIN = "IS_CLIENT_LOGGEDIN"
let GST_NO = "GST_NO"
let MOBILE_NO = "MOBILE_NO"
let EMAIL_ID = "EMAIL_ID"

//MARK:-  Global Strings

var isSelectedMenu = Int()

//MARK:-  Global Dictionaries

var BOOKIN_DATA_DICT = [String:Any]()
var BUY_FOR_ME_DATA_DICT = [String:Any]()
var ISFROMBUYFORME = Bool()

struct appColor
{
    static var appOranges = UIColor.init(named: "#FF8906")
    static var appOrange = //UIColor(hex: "#FF8906")
        UIColor(red: 255/255, green: 115/255, blue: 0/255, alpha: 1)
    static var appOrangeBtn = //UIColor(hex: "#FF7300")
        UIColor(red: 255/255, green: 137/255, blue: 6/255, alpha: 1)
    static var appGray = //UIColor(hex: "#707070")
    UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
    static var appGray97 = //UIColor(hex: "#979797")
    UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
}

//Custom Fonts
struct CustomFont {
    static let productSansBold = "ProductSans-Bold"
    static let productSansRegular = "ProductSans-Regular"
}

//Network Connectivity
struct Network {
    static let title = "Network"
    static let message = "Can't connect right now"
}

//MARK:-  Navigation Titles

struct NavTitle {
    static let Notifications = "Notifications"
    static let Success = "Success"
    static let Book_a_New_Package = "Book A New Package"
    static let Consolidate = "Consolidate"
    static let Product_Details = "Product Details"
    static let Return_Form = "Return Form"
    static let Packaging_Shipping = "Packaging & Shipping"
    static let Basic_Profile = "Basic Profile"
    static let Warehouse_Details = "Warehouse Details"
    static let Subscription_Plans = "Subscription Plans"
    static let Tools = "Tools"
    static let Currency_Convertor = "Currency Convertor"
    static let Shipping_Cost_Calculator = "Shipping Cost Calculator"
    static let Your_Address = "Your Address"
    static let New_Address = "New Address"
    static let Settings = "Settings"
    static let Change_Password = "Change Password"
    static let Return_Policy = "Return Policy"
    static let Privacy_Policy = "Privacy Policy"
    static let About = "About"
    static let FAQ = "FAQ's"
    static let Terms_of_Service = "Terms of Service"
    static let Buy_For_Me = "Buy For Me"
    static let Sign_up = "Sign Up"
    static let Forgot_Password = "Forgot Password"
    static let OTP_Verification = "OTP Verification"
    static let New_Password = "New Password"
    
}
