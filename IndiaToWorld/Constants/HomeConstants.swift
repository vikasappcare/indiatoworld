//
//  HomeConstants.swift
//  IndiaToWorld
//
//  Created by Appcare on 13/01/21.
//

import Foundation
import UIKit
//MARK:- collectionList(homeCollectionView)
struct collectionList {
    static let All = "All"
    static let Buyforme = "Buy for me"
    static let Incoming = "Incoming"
    static let InReview = "In Review"
    static let Readytosend = "Ready to send"
    static let InTransit = "In Transit"
}
extension UIViewController{
    func embed(Identifier:String,Basicview:UIView){
        ViewEmbedder.embed(withIdentifier: Identifier,parent: self,container: Basicview){ vc in
            vc.isEditing = true
        }
    }
}
extension UIViewController{
    func designEx(basicview:UIView){
        basicview.layer.cornerRadius = 5
        basicview.layer.borderWidth = 1.5
        basicview.layer.borderColor = #colorLiteral(red: 0.8274509804, green: 0.8588235294, blue: 0.9725490196, alpha: 1)
    }
}

extension HomeVC{
    //MARK:- VC's
    func passingVC(){
        if  isSelectedMenu == 0
        {
            embed(Identifier: Identifiers.AllPackagesVC, Basicview: self.BasicView)
        }
        if  isSelectedMenu == 1
        {
            embed(Identifier: Identifiers.ReadyToBuyVC, Basicview: self.BasicView)
        }
        if  isSelectedMenu == 2
        {
            embed(Identifier: Identifiers.IncomingViewController, Basicview: self.BasicView)
        }
        if  isSelectedMenu == 3
        {
            embed(Identifier: Identifiers.IncomingViewController, Basicview: self.BasicView)
        }
        if  isSelectedMenu ==  4
        {
            embed(Identifier: Identifiers.ReadyToSendVC, Basicview: self.BasicView)
        }
        if  isSelectedMenu == 5
        {
            embed(Identifier: Identifiers.INTransitViewController, Basicview: self.BasicView)
        }
    }
}
//MARK:-
//        if SlotsSelectedCell == nameArr[indexPath.row]
//        {
//            cell.namelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            cell.namelbl.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
//        }
//        else
//        {
//            cell.namelbl.textColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
//            cell.namelbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        }
//

