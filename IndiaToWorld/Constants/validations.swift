//
//  validations.swift
//  IndiaToWorld
//
//  Created by Appcare on 13/01/21.
//

import Foundation
//MARK:- Alerts
struct Alertmessage {
    
    //All fields empty
    static let allFields = "Fields are empty"
    
    //Login
    static let emailID = "Email-ID field is empty"
    static let emailFormat = "Email-ID format is incorrect"
    static let password = "Password field is empty"
    
    //Signup
    static let firstName = "FirstName field is empty"
    static let lastName = "LastName field is empty"
    static let mobileNo = "Mobile number field is empty"
    static let referral = "Referral Code field is empty"
    
    //New Password
    static let newPswd = "New password field is empty"
    static let confirmNewPswd = "Re-enter new password field is empty"
    
    //Otp
    static let enterOtp = "Please enter otp"
    //Change Password
    static let oldPswd = "Old password field is empty"
    static let samePswd = "Passwords do not match"
    
    //Subscription
    static let subPlan = "Please choose any subplan"

    
    //New Address
    static let flatNo = "FlatNo field is empty"
    static let postalCode = "Postal code field is empty"
    static let country = "Country field is empty"
    static let state = "State field is empty"
    static let city = "City field is empty"
    static let landmark = "Landmark field is empty"
    static let addrName = "Address name field is empty"

    //BuyForMe
    static let enterProductName = "Please enter product name"
    static let enterProductDetails = "Please enter product details"
    static let enterProductLink = "Please enter product link"
    static let selectQuantiy = "Please select quantity"
    static let selectReferenceImg = "Please upload reference image"
    static let enterProductDeliveryType = "Please enter product delivery type"
    static let enterEcommerceType = "Please enter ecommerce type"
    static let productTypeTF = "Please enter product type"
    static let enterExpDeliveryDate = "Please enter exepcted delivery date"
    static let uploadInvoiceImg = "Please upload invoice"

}
