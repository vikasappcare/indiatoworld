//
//  SceneDelegate.swift
//  IndiaToWorld
//
//  Created by Vikas on 14/12/20.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//        if UserDefaults.standard.bool(forKey: LOGGED_IN){
//            let tabBarVC = storyBoard.instantiateViewController(withIdentifier: Identifiers.TabBarController)
//
//            // then set your root view controller
//            self.window?.rootViewController = tabBarVC
//
//        }
        updateRootVC()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = self.window

        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func updateRootVC() {
            let status = UserDefaults.standard.bool(forKey: LOGGED_IN)
            print(status)
            if status == true {
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: Identifiers.TabBarController) as! TabBarController
                if let navigationController = self.window?.rootViewController as? UINavigationController
                {
                    navigationController.pushViewController(initialViewControlleripad, animated: true)
                }
                else
                {
                    print("Navigation Controller not Found")
                }
            }
            else
            {
                print(status)
            }
        }
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

